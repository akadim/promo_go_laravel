<?php

return [
    'PROMO CATCH' => 'PROMO CATCH',
    'Dashboard' => 'Dashboard',
    'Gadgets' => 'Gadgets',
    'Add' => 'Add',
    'Update' => 'Update',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'View' => 'View',
    'Title' => 'Title',
    'Description' => 'Description',
    'Image Path' => 'Image Path',
    'Animated Image Path' => 'Animated Image Path',
    'Price' => 'Price',
    'Effect' => 'Effect',
    'Value' => 'Value',
    'SAVE DATA' => 'SAVE DATA',
    'Choose Action' => 'Choose Action',
    'Successfully Added' => 'Successfully Added',
    'Successfully Updated' => 'Successfully Updated',
    'Successfully Deleted' => 'Successfully Deleted',
    'Successfully Restored' => 'Successfully Restored',
    'Actions' => 'Actions',
    'Stores' => 'Stores',
    'Deals' => 'Deals',
    'Catchers' => 'Catchers'
]; 