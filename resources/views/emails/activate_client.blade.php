<p @if(LaravelLocalization::getCurrentLocale() == 'ar') style="text-align: right" @endif>
	{{ t('salutation') }} {{ $client->fullname }}
</p>

<p @if(LaravelLocalization::getCurrentLocale() == 'ar') style="text-align: right" @endif>
	{{ t('activation.account.message.step.closer') }} 
</p>

<p @if(LaravelLocalization::getCurrentLocale() == 'ar') style="text-align: right" @endif>
	<a href="{{ $activate_url }}">{{ t('activate.account.button.message') }}</a>
</p>

<p @if(LaravelLocalization::getCurrentLocale() == 'ar') style="text-align: right" @endif>
	{{ t('activation.account.message.thanks') }} 
</p>