@extends('layouts.admin')

@section('pageTitle', 'Profiles')

@section('additional_function')

<a href="{{ route('admin.profiles.edit', ['id' => -1]) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
              
@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>Profiles</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}} @if(Session::has('cancel_status')) /  <a href="{{ route('admin.profiles.delete.undo', ['id' => Session::get('cancel_status') ]) }}">Undo</a> @endif
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>Title</th>
                       <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($profiles as $profile)
                     <tr>
                        <td>{{ $profile->title }}</td>
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              {{t('Choose Action')}}
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li>
                                <a href="{{ route('admin.profiles.edit', ['id' => $profile->id ]) }}" title="{{t('Edit')}}" class="btn-quick" data-value="{{ $profile->id }}"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
                              </li>
                              <li class="divider"></li>
                              <li>
                                <a href="{{ route('admin.profiles.delete', ['id' => $profile->id ]) }}" title="{{t('Delete')}}" class="btn-quick btn_delete">
                                  <i class="fa fa-trash-o"></i>&nbsp;Delete
                                </a>
                              </li>
                            </ul>
                          </div>
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>

        </div>
   </div>
@stop