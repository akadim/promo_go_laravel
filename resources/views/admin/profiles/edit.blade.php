@extends('layouts.admin')

@section('pageTitle', 'Profile')

@section('body_content')

<div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>EDIT PROFILE</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">
         
           {!! Form::open(['url' => Request::route()->getPrefix().'/profiles/store', 'files' => true, 'id' => 'front-form', 'class' => 'form-with-list', 'novalidate']) !!}

               <fieldset>
                  
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Title
                        </label>
                        {!! Form::input('text', 'title', $profile->title, ['id' => 'txt_title', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Permissions
                        </label>
                        <br><br>
                        <div class="container">
                           <div class="row">
                              @foreach($permissions_array as $permit)
                                <div class="toggle_permit active">
                                    <label for="">
                                       <input type="checkbox" name="permissions[]" value="{{ $permit['id'] }}" class="check_parent" data-id="{{ $permit['id'] }}" @if(isset($permit['checked']) && $permit['checked']) checked="" @endif/> {{ $permit['title'] }}
                                    </label>
                                     <div class="toggle-content">
                                       <div class="row">
                                            @foreach($permit['child_permissions'] as $indexKey => $permission)
                                               {{--*/ ++$indexKey /*--}}
                                               <div class="col-md-4">
                                                   <input type="checkbox" name="permissions[]" value="{{ $permission['id'] }}" style="vertical-align: -5px;" class="check_children_{{ $permit['id'] }}" @if(isset($permission['checked']) && $permission['checked']) checked="" @endif/> {{ $permission['title'] }}
                                               </div>

                                               @if($indexKey % 3 == 0)
                                                </div>
                                                <div class="row">
                                               @endif
                                            @endforeach
                                        </div>
                                     </div>
                                </div>
                              @endforeach
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  

                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::hidden('id', $profile->id, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>

            {!! Form::close() !!}

        </div>

</div>
<script type="text/javascript">
  $(function(){

       $(document).on('change', '.check_parent', function(evt){
            evt.preventDefault();
            var check_id = $(this).attr('data-id');

            if($(this).is(':checked')) {
               $(".check_children_"+check_id).prop('disabled', false);
               //$(".check_children_"+check_id).prop('checked', true);
            }
            else {
              $(".check_children_"+check_id).prop('disabled', true);
              $(".check_children_"+check_id).prop('checked', false);
            }
            
       });

       $('.check_parent').trigger("change");
  });
</script>
@stop