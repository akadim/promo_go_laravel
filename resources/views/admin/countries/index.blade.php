@extends('layouts.admin')

@section('pageTitle', t('Countries'))

@section('additional_function')

<button type="button" class="btn btn-primary btn_modal" data-value="-1" ><i class="fa fa-plus"></i> {{t('Add')}}</button>
              
@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>{{t('COUNTRIES LIST')}}</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>{{ t('Title') }}</th>
                       <th>اﻹسم</th>
                       <th>{{t('Currency')}}</th>
                       <th>{{t('Social Network')}}</th>
                       <th>{{t('Actions')}}</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($countries as $country)
                     <tr>
                        <td><img src="../images/flags/{{ $country->image_path }}" alt="{{ $country->title_en }}" style="width: 30px; height: 30px;">&nbsp;{{ $country->title_en }}</td>
                        <td><img src="../images/flags/{{ $country->image_path }}" alt="{{ $country->title_ar }}" style="width: 30px; height: 30px;">&nbsp;{{ $country->title_ar }}</td>
                        <td>{{ $country->currency }}</td>
                        <td>
                            <a href="https://www.facebook.com/{{ $country->facebook_link }}" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;
                            <a href="https://www.instagram.com/{{ $country->instagram_link }}" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;
                            <a href="https://twitter.com/{{ $country->twitter_link }}" target="_blank"><i class="fa fa-twitter"></i></a> &nbsp;
                            <a href="https://www.pinterest.com/{{ $country->pinterest_link }}" target="_blank"><i class="fa fa-pinterest"></i></a> &nbsp;
                            <a href="https://plus.google.com/{{ $country->google_plus_link }}" target="_blank"><i class="fa fa-google-plus"></i></a> &nbsp;
                        </td>
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              {{t('Choose Action')}}
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li>
                                <a href="{{ route('admin.country.categories', ['country_id' => $country->id ]) }}" title="View Categories" class="btn-quick"><i class="fa fa-loop"></i> Categories</a>
                              </li>
                              <li>
                                <a href="{{ route('admin.countries.edit', ['id' => $country->id ]) }}" title="{{t('Edit')}}" class="btn-quick btn_modal" data-value="{{ $country->id }}"><i class="fa fa-pencil"></i> Edit</a>
                              </li>
                              <li class="divider"></li>
                              <li>
                                <a href="{{ route('admin.countries.delete', ['id' => $country->id ]) }}" title="{{t('Delete')}}" class="btn-quick btn_delete"><i class="fa fa-trash-o"></i> Delete</a>
                              </li>
                            </ul>
                          </div>
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>

        </div>
   </div>

   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <!-- header modal -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">{{t('EDIT COUNTRY')}}</h4>
          </div>

          <!-- body modal -->
          <div class="modal-body">
            
             {!! Form::open(['url' => 'admin/countries/store', 'files' => true, 'id' => 'front-form', 'novalidate']) !!}
                <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Title in English
                        </label>
                        {!! Form::input('text', 'title_en', null, ['id' => 'txt_title_en', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Title in Arabic
                        </label>
                        {!! Form::input('text', 'title_ar', null, ['id' => 'txt_title_ar', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          {{t('Image')}}
                        </label>
                        {!! Form::file('image_path', ['id' => 'image_path', 'class' => 'form-control required', 'data-rule-extension' => 'png|jpe?g']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          {{t('Currency')}}
                        </label>
                        {!! Form::input('text', 'currency', null, ['id' => 'txt_currency', 'class' => 'form-control required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="description">
                          {{t('Social Network')}}
                        </label>
                        <table>
                             <tr>
                               <td>
                                  https://www.facebook.com/
                               </td>
                               <td>
                                 {!! Form::input('text', 'facebook_link', null, ['id' => 'txt_facebook_link', 'class' => 'form-control']) !!}
                               </td>
                              </tr>
                              <tr>
                               <td>
                                 https://www.instagram.com/
                               </td>
                               <td>
                                 {!! Form::input('text', 'instagram_link', null, ['id' => 'txt_instagram_link', 'class' => 'form-control']) !!}
                               </td>
                               </tr>
                              <tr>
                               <td>
                                 https://twitter.com/
                               </td>
                               <td>
                                 {!! Form::input('text', 'twitter_link', null, ['id' => 'txt_twitter_link', 'class' => 'form-control']) !!}
                               </td>
                              </tr>
                              <tr>
                               <td>
                                 https://www.pinterest.com/
                               </td>
                               <td>
                                 {!! Form::input('text', 'pinterest_link', null, ['id' => 'txt_pinterest_link', 'class' => 'form-control']) !!}
                               </td>
                              </tr>
                              <tr>
                               <td>
                                 https://plus.google.com/
                               </td>
                               <td>
                                 {!! Form::input('text', 'google_plus_link', null, ['id' => 'txt_google_plus', 'class' => 'form-control']) !!}
                               </td>
                             </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {!! Form::hidden('id', null, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>
            {!! Form::close() !!}

          </div>

        </div>
      </div>
    </div>  
    <script type="text/javascript">
      $(function(){
          $(document).on('click', '.btn_modal', function(evt){
              evt.preventDefault();
              var id = $(this).attr('data-value');
            
              $.get('countries/'+id+'/edit', function(response){
                   
                   $('#txt_id').val(response.id);
                   $('#txt_title_en').val(response.title_en);
                   $('#txt_title_ar').val(response.title_ar);
                   $('#txt_currency').val(response.currency);
                   $('#txt_facebook_link').val(response.facebook_link);
                   $('#txt_instagram_link').val(response.instagram_link);
                   $('#txt_twitter_link').val(response.twitter_link);
                   $('#txt_pinterest_link').val(response.pinterest_link);
                   $('#txt_google_plus').val(response.google_plus_link);

                   //$('#image_path').val(response.image_path);
                   $('#modal').modal();
              });
              
          });

          $(document).on('click', '.btn_delete', function(evt) {
                 evt.preventDefault();
                 if(confirm("{{ t('Do you really want to delete this row ?') }}")) {
                     window.location.href = $(this).attr('href');
                 }
          });
      });
    </script>
@stop