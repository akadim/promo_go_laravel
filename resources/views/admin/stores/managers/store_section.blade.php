<div class="form-group">
	<div class="col-md-12 col-sm-12">
	  <label for="title">
	    Store 
	  </label>
	  {!! Form::select('store_id', $stores, $store_id, ['id' => 'txt_store', 'class' => 'form-control required']) !!}
	</div>
</div>