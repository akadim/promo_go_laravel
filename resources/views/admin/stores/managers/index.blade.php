@extends('layouts.admin')

@section('pageTitle', 'Stores Managers')

@section('additional_function')

@if(is_user_allowed(auth()->user()->id, 'Add Store Manager'))
<button type="button" class="btn btn-primary btn_modal" data-value="-1" ><i class="fa fa-plus"></i> Add</button>
@endif
              
@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>Stores Managers</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                    @if(Session::has('cancel_status')) 
                      /  <a href="{{ route('admin.stores.managers.delete.undo', ['id' => Session::get('cancel_status') ]) }}">Undo</a> 
                    @endif
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>Full name</th>
                       <th>Email</th>
                       <th>Tel</th>
                       <th>Store</th>
                       <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($stores_managers as $store_manager)
                     <tr>
                        <td>
                            {{ $store_manager['store_manager']->fullname }}
                        </td>
                        <td>
                            {{ $store_manager['store_manager']->email }}
                        </td>
                        <td>
                            {{ $store_manager['store_manager']->phone }}
                        </td>
                        <td>
                            {{ $store_manager['store']->title }}
                        </td>
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              {{t('Choose Action')}}
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              @if(is_user_allowed(auth()->user()->id, 'Change Store Manager Password'))
                              <li>
                                <a href="{{ route('admin.stores.managers.change.password', ['id' => $store_manager['store_manager']->id ]) }}" title="{{t('Edit')}}" class="btn_password_modal" data-value="{{ $store_manager['store_manager']->id }}"><i class="fa fa-key"></i> Change Password </a>
                              </li>
                              @endif
                              @if(is_user_allowed(auth()->user()->id, 'Update Store Manager'))
                              <li>
                                <a href="{{ route('admin.stores.managers.edit', ['id' => $store_manager['store_manager']->id ]) }}" title="{{t('Edit')}}" class="btn_modal" data-value="{{ $store_manager['store_manager']->id }}"><i class="fa fa-pencil"></i> Edit </a>
                              </li>
                              @endif
                              @if(is_user_allowed(auth()->user()->id, 'Delete Store Manager'))
                              <li class="divider"></li>
                              <li>
                                <a href="{{ route('admin.stores.managers.delete', ['id' => $store_manager['store_manager']->id ]) }}" title="{{t('Delete')}}" class="btn_delete"><i class="fa fa-trash-o"></i> Delete </a>
                              </li>
                              @endif
                            </ul>
                          </div>
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>
            <input type="hidden" name="current_email" id="current_email" value=""/>
        </div>
   </div>

   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- header modal -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">EDIT STORE MANAGER</h4>
          </div>

          <!-- body modal -->
          <div class="modal-body">
            
             {!! Form::open(['url' => 'admin/stores-managers/store', 'files' => true, 'id' => 'front-form', 'class' => 'form-with-list', 'novalidate']) !!}

               <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Full name 
                        </label>
                        {!! Form::input('text', 'fullname', null, ['id' => 'txt_fullname', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Phone
                        </label>
                        {!! Form::input('text', 'phone', null, ['id' => 'txt_phone', 'class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Email 
                        </label>
                        {!! Form::input('email', 'email', null, ['id' => 'txt_email', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row" id="password_section">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Password 
                        </label>
                        {!! Form::input('password', 'password', null, ['id' => 'txt_password', 'class' => 'form-control required']) !!}
                      </div>
                    </div>
                  </div>
                  @if(auth()->user()->is_super_admin)
                    <div class="row" id="password_section">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12">
                          <label for="title">
                            Company 
                          </label>
                          {!! Form::select('company_id', $companies, null, ['id' => 'txt_company', 'class' => 'form-control required']) !!}
                        </div>
                      </div>
                    </div>
                    <div class="row" id="store_section">
                      
                    </div>
                  @else
                    <div class="row" id="password_section">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12">
                          <label for="title">
                            Store 
                          </label>
                          {!! Form::select('store_id', $stores, null, ['id' => 'txt_store', 'class' => 'form-control required']) !!}
                        </div>
                      </div>
                    </div>
                  @endif
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {!! Form::hidden('id', null, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>

            {!! Form::close() !!}

          </div>

        </div>
      </div>
    </div> 

    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="change_password_modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- header modal -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">CHANGE PASSWORD</h4>
          </div>

          <!-- body modal -->
          <div class="modal-body">
            
             {!! Form::open(['url' => Request::route()->getPrefix().'/stores-managers/change-password', 'files' => true, 'class' => 'multiple-form', 'novalidate']) !!}

               <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          New Password
                        </label>
                        {!! Form::input('password', 'new_password', null, ['id' => 'txt_new_password', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Retype Password 
                        </label>
                        {!! Form::input('password', 'confirm_password', null, ['id' => 'txt_confirm_password', 'class' => 'form-control required', 'required', 'data-rule-equalto' => '#txt_new_password']) !!}
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::hidden('manager_id', null, ['id' => 'txt_manager_id', 'class' => 'form-control']) !!}
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      Change Password
                    </button>
                  </div>
                </div>

            {!! Form::close() !!}

          </div>

        </div>
      </div>
    </div> 


    <script type="text/javascript">

      function trigger_store_section(company_id, store_id) {
            

            $.get('../{{ Request::route()->getPrefix() }}/stores-managers/store_section/'+company_id+'/'+store_id, function(response){
                 $('#store_section').html(response);
            });

      }

      $(function(){

          $(document).on('change', '#txt_company', function(evt){
                evt.preventDefault();

                var company_id = $(this).val();

                trigger_store_section(company_id, null);
          });

          $(document).on('click', '.btn_modal', function(evt){
              evt.preventDefault();
              var id = parseInt($(this).attr('data-value'));
            
                $.get('../{{ Request::route()->getPrefix() }}/stores-managers/'+id+'/edit', function(response){

                     console.log(response);
                     
                     $('#txt_id').val(response.store_manager.id);
                     $('#txt_fullname').val(response.store_manager.fullname);
                     $('#txt_phone').val(response.store_manager.phone);
                     $("#txt_email").val(response.store_manager.email);
                     $("#current_email").val(response.store_manager.email);

                     if(id != -1) {
                        $("#txt_password").removeClass('required');
                        $("#password_section").hide();

                        if($('#txt_company')) {
                           $('#txt_company').val(response.company_id);
                           trigger_store_section($('#txt_company').val(), response.store_id);
                           //$('#txt_company').trigger('change');
                        }

                        $("#txt_store").val(response.store_id);
                     }
                     else {
                        
                        if(!$("#txt_password").hasClass('required')) {
                           $("#txt_password").addClass('required');
                        }

                        $("#password_section").show();
                     }


                     $('#modal').modal();
                });
              
          });

          $(document).on('click', '.btn_password_modal', function(evt){
               evt.preventDefault();
               var id = $(this).attr('data-value');

               $("#txt_manager_id").val(''+id);
               $("#txt_new_password").val('');
               $("#txt_confirm_password").val('');

               $("#change_password_modal").modal();
          });

          $(document).on('submit', '#front-form', function(evt){
               
               

               if($("#txt_email").hasClass('error')) {
                    $("#txt_email").removeClass('error');
                    $("#txt_email-error").remove();
               }

               var email = $("#txt_email").val();
               var current_email = $("#current_email").val();

               if( (current_email != null || current_email != "") && (current_email != email) ) {
                    
                    $.ajax({
                        url: '../{{ Request::route()->getPrefix() }}/stores-managers/verify-email',
                        method: 'POST',
                        data: {'_token': '{{ csrf_token() }}', 'email': email},
                        async: false,
                        success: function(response){
                             if(response.response == 'Exist') {
                                 evt.preventDefault();
                                 $("#txt_email").addClass('error');
                                 $('<label id="txt_email-error" class="error" for="txt_email">Email already exist.</label>').insertAfter($('#txt_email'));
                             }
                             else {
                                return true;
                             }
                       }
                   });
                    
               }
          });

          /*
          $(document).on('click', '.btn_delete', function(evt) {
                 evt.preventDefault();
                 if(confirm("{{ t('Do you really want to delete this row ?') }}")) {
                     window.location.href = $(this).attr('href');
                 }
          });
          */
      });
    </script>
@stop