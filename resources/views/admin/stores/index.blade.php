@extends('layouts.admin')

@section('pageTitle', 'Stores' )

@section('additional_function')

@if(is_user_allowed(auth()->user()->id, 'Add Store'))
<button type="button" class="btn btn-primary btn_modal" data-value="-1" ><i class="fa fa-plus"></i> {{t('Add')}}</button>
@endif
              
@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>Stores</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                    @if(Session::has('cancel_status')) 
                      /  <a href="{{ route('admin.stores.delete.undo', ['id' => Session::get('cancel_status') ]) }}">Undo</a> 
                    @endif
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>Title</th>
                       <th>Location</th>
                       <th>Phone</th>
                       @if(auth()->user()->is_super_admin)
                       <th>Company</th>
                       @endif
                       <th>{{t('Actions')}}</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($stores as $store)
                     <tr>
                        <td>{{ $store->title }}</td>
                        <td>{{ $store->location }}</td>
                        <td>{{ $store->phone }}</td>
                        @if(auth()->user()->is_super_admin)
                        <td>{{ $store->company->title }}</td>
                        @endif
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              {{t('Choose Action')}}
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                             @if(is_user_allowed(auth()->user()->id, 'Update Store'))
                              <li>
                                <a href="{{ route('admin.stores.edit', ['id' => $store->id ]) }}" title="{{t('Edit')}}" class="btn-quick btn_modal" data-value="{{ $store->id }}"><i class="fa fa-pencil"></i> Edit</a>
                              </li>
                             @endif
                             @if(is_user_allowed(auth()->user()->id, 'Delete Store'))
                              <li class="divider"></li>
                              <li>
                                <a href="{{ route('admin.stores.delete', ['id' => $store->id ]) }}" title="{{t('Delete')}}" class="btn-quick btn_delete"><i class="fa fa-trash-o"></i> Delete</a>
                              </li>
                             @endif
                            </ul>
                          </div>
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>

        </div>
   </div>

   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <!-- header modal -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">EDIT STORE</h4>
          </div>

          <!-- body modal -->
          <div class="modal-body">
            
             {!! Form::open(['url' => Request::route()->getPrefix().'/stores/store', 'files' => true, 'id' => 'front-form', 'novalidate']) !!}
                <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Title
                        </label>
                        {!! Form::input('text', 'title', null, ['id' => 'txt_title', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Location
                        </label>
                        {!! Form::input('text', 'location', null, ['id' => 'txt_location', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>

                  <div class="map_canvas"></div>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Zipcode
                        </label>
                        {!! Form::input('text', 'zipcode', null, ['id' => 'txt_zip_code', 'class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Phone
                        </label>
                        {!! Form::input('text', 'phone', null, ['id' => 'txt_tel', 'class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  @if(auth()->user()->is_super_admin)
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12">
                          <label for="title">
                            Company
                          </label>
                          {!! Form::select('company_id', $companies, null, ['id' => 'txt_company_id', 'class' => 'form-control required', 'required']) !!}
                        </div>
                      </div>
                    </div>
                  @endif
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::hidden('id', null, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>
            {!! Form::close() !!}

          </div>

        </div>
      </div>
    </div>  
    <script type="text/javascript">
      $(function(){
          $(document).on('click', '.btn_modal', function(evt){
              evt.preventDefault();
              var id = $(this).attr('data-value');
            
              $.get('../{{ Request::route()->getPrefix() }}/stores/'+id+'/edit', function(response){

                   $('#txt_id').val(response.id);
                   $('#txt_title').val(response.title);
                   $('#txt_location').val(response.location);
                   $('#txt_zip_code').val(response.gmap_link);
                   $('#txt_city').val(response.city);
                   $('#txt_country').val(response.country);
                   $('#txt_tel').val(response.phone);
                   $('#txt_company_id').val(response.company_id);

                   $('#modal').modal();
              });
              
          });

          var options = {
            map: ".map_canvas"
          };

          $("#txt_location").geocomplete(options)
          .bind("geocode:result", function(event, result){
             $(this).val(result.formatted_address);
          })
          .bind("geocode:error", function(event, status){
           // $.log("ERROR: " + status);
          })
          .bind("geocode:multiple", function(event, results){
            // $.log("Multiple: " + results.length + " results found");
          });
          
          /*
          $(document).on('click', '.btn_delete', function(evt) {
                 evt.preventDefault();
                 if(confirm("{{ t('Do you really want to delete this row ?') }}")) {
                     window.location.href = $(this).attr('href');
                 }
          });
         */ 
      });
    </script>
@stop