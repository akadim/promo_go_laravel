@extends('layouts.admin')

@section('pageTitle', 'Orders')

@section('additional_function')

<button type="button" class="btn btn-primary btn_modal" data-value="-1" ><i class="fa fa-plus"></i> {{t('Add')}}</button>
              
@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>ORDER LIST</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>Deal</th>
                       <th>Voucher</th>
                       <th>Quantity</th>
                       <th>Price</th>
                       <th>{{t('Actions')}}</th>
                    </tr>
                </thead>
                <tbody>  
                   @foreach($order->order_lines as $order_line)
                     <tr>
                        <td>
                          <strong>{{ $order_line->deal->title_en }}</strong>
                          @if( $order_line->deal_option && !is_null($order_line->deal_option))
                            <br>
                            {{ $order_line->deal_option->title_en }}
                          @endif
                        </td>
                        <td>
                          {{ $order_line->voucher }}
                        </td>
                        <td>
                           {{ $order_line->ordered_quantity }}
                        </td>
                        <td>
                          {{ number_format($order_line->price, 2, '.', ' ')  }}
                        </td>
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              {{t('Choose Action')}}
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li>
                                <a href="{{ route('admin.order.order_lines', ['order_id' => $order->id ]) }}" title="View Vouchers" class="btn-quick"><i class="fa fa-loop"></i> Print Vouchers</a>
                              </li>
                            </ul>
                          </div>
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>

        </div>
   </div>

    <script type="text/javascript">
      $(function(){
        
          $(document).on('click', '.btn_delete', function(evt) {
                 evt.preventDefault();
                 if(confirm("{{ t('Do you really want to delete this row ?') }}")) {
                     window.location.href = $(this).attr('href');
                 }
          });
      });
    </script>
@stop