<!doctype html>
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>PROMO CATCH</title>
		<meta name="description" content="" />
		<meta name="Author" content="KADIM Ahmed" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="{{ asset('assets/admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="{{ asset('assets/admin/css/essentials.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/admin/css/layout.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/admin/css/color_scheme/green.css') }}" rel="stylesheet" type="text/css" id="color_scheme" />

		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = "{{ asset('assets/admin/plugins') }}/";</script>
		<script type="text/javascript" src="{{ asset('assets/admin/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/admin/js/jquery.validate.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/admin/assets/js/app.js') }}"></script>
		<script type="text/javascript">
       $(function(){

           if($("#front-form")) { 
              jQuery.extend(jQuery.validator.messages,{
                    required: "{{ t('This field is required') }}.",
                    remote: "Please fix this field.",
                    email: "{{ t('Email invalide') }}.",
                    url: "{{ t('Invalid URL') }}.",
                    date: "{{ t('Invalid Date') }}.",
                    dateISO: "Please enter a valid date (ISO).",
                    number: "{{ t('Invalid Number') }}.",
                    digits: "{{ t('Only Numbers are required') }}.",
                    creditcard: "Please enter a valid credit card number.",
                    equalTo: "Please enter the same value again.",
                    accept: "{{ t('Please enter a value with a valid extension')}}.",
                    maxlength: $.validator.format("Please enter no more than {0} characters."),
                    minlength: $.validator.format("Please enter at least {0} characters."),
                    rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
                    range: $.validator.format("Please enter a value between {0} and {1}."),
                    max: $.validator.format("Please enter a value less than or equal to {0}."),
                    min: $.validator.format("Please enter a value greater than or equal to {0}.")
                });

              $("#front-form").validate(); 

           }
       });
    </script>

	</head>
	<!--
		.boxed = boxed version
	-->
	<body>


		<div class="padding-15">

			<div class="login-box">

				<!-- login form -->
				{!! Form::open(['class' => 'sky-form boxed', 'id' => 'front-form']) !!}
					<header><i class="fa fa-users"></i> Sign In</header>

				  @if(Session::has('fail'))
					<div class="alert alert-danger noborder text-center weight-400 nomargin noradius">
						{{Session::get('fail')}}
					</div>
				  @endif
                   
                   <!--
					<div class="alert alert-warning noborder text-center weight-400 nomargin noradius">
						Account Inactive!
					</div>

					<div class="alert alert-default noborder text-center weight-400 nomargin noradius">
						<strong>Too many failures!</strong> <br />
						Please wait: <span class="inlineCountdown" data-seconds="180"></span>
					</div>
					-->

					<fieldset>	
					
						<section>
							<label class="label">E-mail</label>
							<label class="input">
								<i class="icon-append fa fa-envelope"></i>
							    {!! Form::input('text', 'email', null, ['placeholder' => 'Type your Email', 'class' => 'required email']) !!}
								<span class="tooltip tooltip-top-right">Email Address</span>
							</label>
						</section>
						
						<section>
							<label class="label">Password</label>
							<label class="input">
								<i class="icon-append fa fa-lock"></i>
								{!! Form::input('password', 'password', null, ['placeholder' => 'Type your Password', 'class' => 'required']) !!}
								<b class="tooltip tooltip-top-right">Type your Password</b>
							</label>
						</section>

					</fieldset>

					<footer>
					    <input type="hidden" name="_token" value="{{ Session::token() }}"/>
						<button type="submit" class="btn btn-primary pull-right">Sign In</button>
						<div class="forgot-password pull-left">
							<a href="{{ route('admin.reset.password') }}">Forgot password?</a> 
						</div>
					</footer>
				{!! Form::close() !!}
				<!-- /login form -->

				<hr />

			</div>

		</div>

		
	</body>
</html>