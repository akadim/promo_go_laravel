@extends('layouts.admin')

@section('pageTitle', 'Settings')

@section('body_content')

<div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>SETTINGS</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

           @if(Session::has('status'))
             <div class="alert alert-success margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                {{Session::get('status')}}
             </div>
           @endif
         
           {!! Form::open(['url' => Request::route()->getPrefix().'/settings', 'files' => true, 'class' => 'multiple-form', 'novalidate']) !!}
                <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          About Us in English
                        </label>
                        {!! Form::textarea('about_us_en', $settings[0]['value'] , ['id' => 'txt_about_us_en', 'class' => 'form-control required app_editor']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          About Us in Arabic
                        </label>
                        {!! Form::textarea('about_us_ar', $settings[1]['value'] , ['id' => 'txt_about_us_ar', 'class' => 'form-control required app_editor']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Contact Us in English
                        </label>
                        {!! Form::textarea('contact_us_en', $settings[2]['value'] , ['id' => 'txt_contact_us_en', 'class' => 'form-control required app_editor']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Contact Us in Arabic
                        </label>
                        {!! Form::textarea('contact_us_ar', $settings[3]['value'] , ['id' => 'txt_contact_us_ar', 'class' => 'form-control required app_editor']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Terms and Conditions in English
                        </label>
                        {!! Form::textarea('terms_conditions_en', $settings[4]['value'] , ['id' => 'txt_terms_conditions_en', 'class' => 'form-control required app_editor']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Terms and Conditions in Arabic
                        </label>
                        {!! Form::textarea('terms_conditions_ar', $settings[5]['value'] , ['id' => 'txt_terms_conditions_ar', 'class' => 'form-control required app_editor']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Privacy Policy in English
                        </label>
                        {!! Form::textarea('privacy_policy_en', $settings[6]['value'] , ['id' => 'txt_privacy_policy_en', 'class' => 'form-control required app_editor']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Privacy Policy in Arabic
                        </label>
                        {!! Form::textarea('privacy_policy_ar', $settings[7]['value'] , ['id' => 'txt_privacy_policy_ar', 'class' => 'form-control required app_editor']) !!}
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      UPDATE
                    </button>
                  </div>
                </div>
            {!! Form::close() !!}

        </div>

</div>
<script type="text/javascript">
	$(function(){
       
       $(".bootstrap-tagsinput").addClass('form-control');

       $("#txt_time_limit").datetimepicker({
            format: 'YYYY-MM-DD H:mm:s'
       });

       $(document).on('change', "#txt_category", function(evt){
            evt.preventDefault();
            var category_id = $(this).val();
            $.get('../../deals/show_related_list/'+category_id, function(response){
                $("#category_selects_id").html(response);
            });
       });

       $(document).on('change', '#txt_country_id', function(evt){
            evt.preventDefault();
            var country_id = $(this).val();
            $.get('../../deals/show_related_categories/'+country_id, function(response){
                  $("#categories_section").html(response); 
            });
       });

	});
</script>

@stop