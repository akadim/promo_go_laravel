@extends('layouts.admin')

@section('pageTitle', 'Users')

@section('additional_function')
              
@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>Users</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>Fullname</th>
                       <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($users as $user)
                     <tr>
                        <td>
                            {{ $user->fullname }}
                        </td>
                        <td>
                            {{ $user->email }}
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>

        </div>
   </div>
@stop