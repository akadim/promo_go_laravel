@extends('layouts.admin')

@section('pageTitle', 'Profile')

@section('body_content')

<div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>MY PROFILE</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

           @if(Session::has('status'))
             <div class="alert alert-success margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                {{Session::get('status')}}
             </div>
           @endif
         
           {!! Form::open(['url' => Request::route()->getPrefix().'/profile', 'files' => true, 'class' => 'multiple-form', 'novalidate']) !!}
                <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Full Name
                        </label>
                        {!! Form::input('text', 'fullname', auth()->user()->fullname , ['id' => 'txt_fullname', 'class' => 'form-control required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Email
                        </label>
                        {!! Form::input('text', 'email', auth()->user()->email , ['id' => 'txt_email', 'class' => 'form-control required']) !!}
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {!! Form::hidden('id', auth()->user()->id, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                      UPDATE
                    </button>
                  </div>
                </div>
            {!! Form::close() !!}

        </div>

</div>
<div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>CHANGE PASSWORD</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

           @if(Session::has('password_status'))
             <div class="alert alert-success margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                {{Session::get('password_status')}}
             </div>
           @endif

           @if(Session::has('fail'))
             <div class="alert alert-danger margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                {{Session::get('fail')}}
             </div>
           @endif
         
           {!! Form::open(['url' => Request::route()->getPrefix().'/password', 'files' => true, 'id' => 'front-form', 'novalidate']) !!}
                <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Current Password
                        </label>
                        {!! Form::input('password', 'old_password', null, ['id' => 'txt_old_password', 'class' => 'form-control required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          New Password
                        </label>
                        {!! Form::input('password', 'new_password', null , ['id' => 'txt_new_password', 'class' => 'form-control required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Confirm Password
                        </label>
                        {!! Form::input('password', 'confirm_password', null , ['id' => 'txt_confirm_password', 'class' => 'form-control required', 'data-rule-equalto' => '#txt_new_password']) !!}
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      CHANGE PASSWORD
                    </button>
                  </div>
                </div>
            {!! Form::close() !!}

        </div>

</div>
<script type="text/javascript">
	$(function(){
       
       $(".bootstrap-tagsinput").addClass('form-control');

       $("#txt_time_limit").datetimepicker({
            format: 'YYYY-MM-DD H:mm:s'
       });

       $(document).on('change', "#txt_category", function(evt){
            evt.preventDefault();
            var category_id = $(this).val();
            $.get('../../deals/show_related_list/'+category_id, function(response){
                $("#category_selects_id").html(response);
            });
       });

       $(document).on('change', '#txt_country_id', function(evt){
            evt.preventDefault();
            var country_id = $(this).val();
            $.get('../../deals/show_related_categories/'+country_id, function(response){
                  $("#categories_section").html(response); 
            });
       });

	});
</script>

@stop