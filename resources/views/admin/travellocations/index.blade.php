@extends('layouts.admin')

@section('pageTitle', $category->title.' Locations' )

@section('additional_function')

<button type="button" class="btn btn-primary btn_modal" data-value="-1" ><i class="fa fa-plus"></i> {{t('Add')}}</button>
              
@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>{{ $category->title_en.' '.t('Locations')}}</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>{{ t('Title') }}</th>
                       <th>اﻹسم</th>
                       <th>{{t('Actions')}}</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($travelLocations as $travelLocation)
                     <tr>
                        <td>{{ $travelLocation->title_en }}</td>
                        <td>{{ $travelLocation->title_ar }}</td>
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              {{t('Choose Action')}}
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li>
                                <a href="{{ route('admin.travellocations.edit', ['category_id' => $category->id, 'id' => $travelLocation->id ]) }}" title="{{t('Edit')}}" class="btn-quick btn_modal" data-value="{{ $travelLocation->id }}"><i class="fa fa-pencil"></i> Edit</a>
                              </li>
                              <li class="divider"></li>
                              <li>
                                <a href="{{ route('admin.travellocations.delete', ['category_id' => $category->id, 'id' => $travelLocation->id ]) }}" title="{{t('Delete')}}" class="btn-quick btn_delete"><i class="fa fa-trash-o"></i> Delete</a>
                              </li>
                            </ul>
                          </div>
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>

        </div>
   </div>

   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <!-- header modal -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">{{t('EDIT CATEGORY')}}</h4>
          </div>

          <!-- body modal -->
          <div class="modal-body">
            
             {!! Form::open(['url' => 'admin/travellocations/'.$category->id.'/store', 'files' => true, 'id' => 'front-form', 'novalidate']) !!}
                <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Title in English
                        </label>
                        {!! Form::input('text', 'title_en', null, ['id' => 'txt_title_en', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Title in Arabic
                        </label>
                        {!! Form::input('text', 'title_ar', null, ['id' => 'txt_title_ar', 'class' => 'form-control required', 'style' => 'text-align: right;']) !!}
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::hidden('id', null, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                    {!! Form::hidden('category_id', $category->id, ['id' => 'txt_category_id', 'class' => 'form-control']) !!}
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>
            {!! Form::close() !!}

          </div>

        </div>
      </div>
    </div>  
    <script type="text/javascript">
      $(function(){
          $(document).on('click', '.btn_modal', function(evt){
              evt.preventDefault();
              var id = $(this).attr('data-value');
            
              $.get('../travellocations/{{ $category->id }}/'+id+'/edit', function(response){

                   category_id = response['category_id'];

                   $('#txt_id').val(response.id);
                   $('#txt_title_en').val(response.title_en);
                   $('#txt_title_ar').val(response.title_ar);
                   $('#modal').modal();
              });
              
          });

          $(document).on('click', '.btn_delete', function(evt) {
                 evt.preventDefault();
                 if(confirm("{{ t('Do you really want to delete this row ?') }}")) {
                     window.location.href = $(this).attr('href');
                 }
          });
      });
    </script>
@stop