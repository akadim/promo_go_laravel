@extends('layouts.admin')

@section('pageTitle', 'Set the Positions')

@section('additional_function')
              
@stop

@section('body_content')
   
   <style>
     #map-canvas {
         margin: 0;
         padding: 0;
         height: 400px;
         max-width: none;
      }
      #map-canvas img {
         max-width: none !important;
      }
      
      .editable_section {
           font-size: 18px;
      }

      .editable_section .editable_section_link, .editable_section .editable_section_form {
            display: none;
      }

      .editable_section:hover .editable_section_link {
            display: inline-block;
      }

      .hidden_section {
          display: none;
      }
   </style>
   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>Set The postions of the Deal</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

              <div class="alert alert-success" id="deal_countdown">
                <strong>Remaining Time: <span class="countdown"></span></strong>
              </div>

            {!! Form::open(['url' => route('admin.deals.marking', ['id' => $deal->id]), 'files' => true, 'id' => 'front-form', 'class' => 'form-with-list', 'novalidate']) !!}

                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Store
                        </label>
                        {!! Form::select('store_id', $stores_params, null, ['id' => 'txt_store_id', 'class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Maximum number of deals
                        </label>
                        {!! Form::input('text', 'original_number_of_deals', $deal->number_of_deals, ['id' => 'txt_original_number_of_deals', 'class' => 'form-control', 'disabled']) !!}
                        {!! Form::input('hidden', 'original_number_of_deals', $deal->number_of_deals, ['id' => 'hidden_txt_original_number_of_deals']) !!}
                      </div>
                    </div>
                  </div>

                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Remaining number of deals
                        </label>
                        {!! Form::input('text', 'number_of_deals', $max_number_deals, ['id' => 'txt_number_of_deals', 'class' => 'form-control', 'disabled']) !!}
                        {!! Form::input('hidden', 'number_of_deals', $max_number_deals, ['id' => 'hidden_txt_number_of_deals']) !!}
                      </div>
                    </div>
                  </div>

                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Number of deals
                        </label>
                        {!! Form::input('text', 'number_deals_by_location', $max_number_deals, ['id' => 'txt_number_of_deals_by_location', 'class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>

                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-8 col-sm-8">
                        <label for="title">
                          Consumption Deal's Time 
                        </label>
                        {!! Form::input('text', 'duration_to_goal', null, ['id' => 'txt_duration_to_goal', 'class' => 'form-control required digits', 'required']) !!}
                      </div>
                      <div class="col-md-4 col-sm-4">
                        <label for="title">
                          &nbsp;
                        </label>
                        {!! Form::select('duration_time_type', ['minutes' => 'Minutes', 'hours' => 'Hours', 'days' => 'Days', 'years', 'Years', 'decades' => 'Decades', 'centuries' => 'Centuries'], null, ['id' => 'txt_duration_time_type', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>

                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::hidden('id', $deal->id, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="button" id="btn_generate_markers">
                      Generate Markers
                    </button>
                  </div>
                </div>
                <div id="map-canvas"></div>
                <div class="row">
                  <div class="col-md-12">
                    <input type="hidden" name="selected_store_id" id="selected_store_id" value=""/>
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit" id="btn_save_markers">
                      Save Changes
                    </button>
                  </div>
                </div>

            {!! Form::close() !!}
        </div>
   </div>
   <script>
      var map;
      var marker;
      var arrMarkers=new Array(0);
      var bounds;
 
      
      function initialize_markers() {

          if(arrMarkers) {
             for(var i in arrMarkers) {
                arrMarkers[i].setMap(null);
             }
          }

          arrMarkers = new Array(0);

      }

      function initialize() {
        var mapOptions =  {
                center: new google.maps.LatLng(40.680898,-8.684059),
                zoom: 11,
                mapTypeId: google.maps.MapTypeId.HYBRID
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
      }

      function initialize_countdown(start_date, end_date) {
          
          var start_date = start_date.split(" ");
          var end_date = end_date.split(" ");

          var start_date_parts = start_date[0];
          var start_time_parts = start_date[1];

          var end_date_parts = end_date[0];
          var end_time_parts = end_date[1];

          start_date = start_date_parts.split("-");
          start_time = start_time_parts.split(":");

          end_date = end_date_parts.split("-");
          end_time = end_time_parts.split(":");

          var start_year = start_date[0];
          var start_month = start_date[1];
          var start_day = start_date[2];

          var start_hour = start_time[0];
          var start_minute = start_time[1];
          var start_second = start_time[2];
          
          var end_year = end_date[0];
          var end_month = end_date[1];
          var end_day = end_date[2];

          var end_hour = end_time[0];
          var end_minute = end_time[1];
          var end_second = end_time[2];

          var start_date = new Date(start_year, parseInt(start_month) - 1, start_day, start_hour, start_minute, start_second);
          var end_date = new Date(end_year, parseInt(end_month) -1 , end_day, end_hour, end_minute, end_second);
          
          var end_date_timestamp = end_date.getTime();
          var current_date = new Date();

          if(start_date.getTime() < current_date.getTime() && end_date.getTime() > current_date.getTime()) {
               $("#deal_countdown").show();
               $("#deal_expiration_message").hide();
          }

          $('.countdown').countdown(end_date_timestamp, function(event) {
                $(this).html('<span style="margin: 0px 20px; ">'+event.strftime('%D days %H:%M:%S')+'</span>');
              }).on('finish.countdown', function(evt){
                   $.ajax({
                       url: '../expire_deal/{{ $deal->id }}',
                       async: false,
                       success: function(response) {
                          window.location.href = '{{ Request::route()->getPrefix() }}/deals/results/{{ $deal->id }}';
                       }
                   });
              });

      }

      initialize_countdown('{{ $deal->start_date }}', '{{ $deal->end_date }}');

      function show_info(current_marker, position_id, validated) {
            
            console.log(position_id);
            $.ajax({
                 url: '../show_info/{{ $deal->id }}/'+position_id+'/'+validated,
                 async: false,
                 success: function(response) {
                    var infowindow = new google.maps.InfoWindow({
                       content: response
                    });

                    infowindow.open(map, current_marker);
                 }
             });
      }


      google.maps.event.addDomListener(window, "load", initialize);

      function searchAddress(addressInput, addressInputId) {

         initialize_markers();

         $.get('../{{ $id }}/'+addressInputId+'/markers', function(response){

               var coordinates = response.data;

               if(coordinates.length > 0) {
                    for(i = 0; i < coordinates.length; i++) {
                        var latLongs = { lat: coordinates[i][0], lng: coordinates[i][1] }
                        var marker = createMarker(latLongs, 'some text for test');
                        marker['position_id'] = coordinates[i][4];

                        /*
                        var latitude = coordinates[i][0];
                        var longitude = coordinates[i][1];
                        var time_to_goal = coordinates[i][2];
                        var position_id = coordinates[i][4];
                        
                        console.log(coordinates[i][4]);
                        */

                        marker.addListener('click', function(){
                             
                             console.log(this.position_id);
                             show_info(this, this.position_id, 'yes');  
                        });

                        //google.maps.event.trigger(marker, 'click');

                        arrMarkers.push(marker);
                    }
               } 

               var geocoder = new google.maps.Geocoder();

               geocoder.geocode({ address: addressInput}, function(results, status){

                   if(status == google.maps.GeocoderStatus.OK) {
                      
                       var myResult = results[0].geometry.location;

                       map.setCenter(myResult);

                       map.setZoom(17);
                   }
                   else {
                      alert("The Geocode was not successful for the following reason: "+status);
                   }
               });

         });

      }

      function createMarker(latlng, text) {

         marker = new google.maps.Marker({
             map: map,
             position: latlng,
             title: text.toString(),
             draggable: true,
             icon: '{{ asset("images/markers/default.png") }}'
         });

         return marker;

      }

      function plotrandom(number) {
           bounds = map.getBounds();

           var southWest = bounds.getSouthWest();
           var northEast = bounds.getNorthEast();
           var lngSpan = northEast.lng() - southWest.lng();
           var latSpan = northEast.lat() - southWest.lat();

           pointsrand=[];

           for(var i=0; i< number; ++i) {
              var point = new google.maps.LatLng(southWest.lat() + latSpan * Math.random(), southWest.lng() + lngSpan * Math.random());

              pointsrand.push(point);
           }

           for(var i=0; i<number; ++i) {
              var str_text = i+" : "+pointsrand[i];

              var marker = createMarker(pointsrand[i], str_text);

             
              marker.addListener('click', function(){
                  show_info(this, -1, 'not_yet');

              });

              arrMarkers.push(marker);

              marker.setMap(map);

           }

      }

      $(document).on('change', '#txt_store_id', function(evt){
           evt.preventDefault();

           var addressInput = $('#txt_store_id option:selected').text();
           var addressInputId = $('#txt_store_id').val();

           searchAddress(addressInput, addressInputId);
      });

      $(document).on('click', '#btn_generate_markers', function(evt){
            evt.preventDefault();

           initialize_markers();

            if(arrMarkers) {
               for(var i in arrMarkers) {
                  arrMarkers[i].setMap(null);
               }
            }

            var number_of_deals = parseInt($("#hidden_txt_number_of_deals").val());
            var number_of_deals_by_location  = parseInt($("#txt_number_of_deals_by_location").val());

            if(number_of_deals_by_location <= number_of_deals) {
                plotrandom(number_of_deals_by_location);
            }
            else {
              toastr.error("Can you please specify a number lower or equal to the Max");
            }
      });

      $(document).on('submit', '#front-form', function(evt){

           evt.preventDefault();

           var latLngs = [];

           for(var i=0; i < arrMarkers.length; i++) {

               latLngs.push({'lat': arrMarkers[i].getPosition().lat(), 'lng': arrMarkers[i].getPosition().lng()});
           }

           if(latLngs.length > 0) {

              $("#btn_save_markers").prop("disabled", true);

              $.post(''+window.location.href,
                      {
                        'deal_id': '{{ $id }}', 
                        'store_id': $("#txt_store_id").val(), 
                        'duration_to_goal': $("#txt_duration_to_goal").val(),
                        'duration_time_type': $("#txt_duration_time_type").val(),
                        'positions': latLngs,
                        '_token': '{{ csrf_token() }}' 
                      }, 
                      function(response){

                         $("#btn_save_markers").prop("disabled", false);

                         var remaining_number_of_deals = parseInt(response.remaining_deals_positions);

                         var database = firebase.database();

                         var api_deal_positions = response.positions;
                         var positions = [];

                        /*
                         for(var j=0; j < api_deal_positions.length; j++) {

                              database.ref('deals/'+snapshot.key+'/deal_positions').push({
                                  id:  api_deal_positions[j].id,
                                  lat: parseFloat(api_deal_positions[j].latitude),
                                  lg: parseFloat(api_deal_positions[j].longitude),
                                  duration_to_goal: parseInt(api_deal_positions[j].duration_to_goal),
                                  duration_time_type: api_deal_positions[j].duration_time_type,
                                  status: api_deal_positions[j].status
                              });
  
                         }
                         */
                         
                        
                         database.ref('deals').orderByChild('id').equalTo({{ $deal->id }}).on('child_added', function(snapshot){
                              
                             database.ref('deals/'+snapshot.key+'/deal_positions').remove();

                             for(var j=0; j < api_deal_positions.length; j++) {

                                  database.ref('deals/'+snapshot.key+'/deal_positions').push({
                                      id:  api_deal_positions[j].id,
                                      name: "{{ $deal->company->title }}",
                                      img: "{{ $deal->company->imagePath }}",
                                      discount: "{{ ($deal->is_condition) ? $deal->condition : $deal->discount }}",
                                      lat: parseFloat(api_deal_positions[j].latitude),
                                      lg: parseFloat(api_deal_positions[j].longitude),
                                      duration_to_goal: parseInt(api_deal_positions[j].duration_to_goal),
                                      duration_time_type: api_deal_positions[j].duration_time_type,
                                      status: api_deal_positions[j].status
                                  });
      
                             }
                         });


                         $("#txt_number_of_deals").val('' +  remaining_number_of_deals);
                         $("#hidden_txt_number_of_deals").val('' +  remaining_number_of_deals);

                         searchAddress(response.store_address, response.store_id);

                         toastr.success("The deals has been marked successfully in the map");
               });
           }
      });
   </script>

   <script type="text/javascript">
  $(function(){
       
       $(document).on("click", ".editable_section_link", function(evt) {
          
          evt.preventDefault();

          var position_id = $(this).data('postion');

          $(this).parent().hide();
          $(this).parent().next().show();
      }); 

      $(document).on("click", ".editable_section_btn", function(evt) {

          var textfield_id = $(this).data('textfield');

          var position_id = $("#"+textfield_id).data('position');
          var time_to_goal = $("#"+textfield_id).val();
          
          if(time_to_goal != null && time_to_goal != '') {
             $.get('../time_to_goal/'+position_id+'/'+time_to_goal, function(response){
                  $("#editable_section_form_"+response.position_id).hide();
                  $("#editable_section_text_"+response.position_id).children('.editable_text_content').html(response.time_to_goal);
                  $("#editable_section_text_"+response.position_id).show();
                  
                  /*
                  var database = firebase.database();
                  database.ref('deals').orderByChild('discount_code').equalTo('{{ $deal->discount_code }}').on('child_added', function(snapshot){
                             database.ref('deals/'+snapshot.key).update({deal_positions: positions});
                  });
                  */
            });
          }
          else {
            $("#editable_section_form_"+position_id).hide();
            $("#editable_section_text_"+position_id).show();
          }
      });

      $(document).on("click", ".editable_section_cancel_link", function(evt){
          evt.preventDefault();

          var position_id = $(this).data('position');
          $("#editable_section_form_"+position_id).hide();
          $("#editable_section_text_"+position_id).show(); 
      });

  });
</script>
@stop