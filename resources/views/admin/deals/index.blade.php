@extends('layouts.admin')

@section('pageTitle', 'Deals')

@section('additional_function')

@if(is_user_allowed(auth()->user()->id, 'Add Deal'))
<a href="{{ route('admin.deals.edit', ['id' => -1]) }}" class="btn btn-primary btn_modal" data-value="-1"><i class="fa fa-plus"></i> Add</a>
@endif
              
@stop

@section('body_content')
   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>Deals</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          @if(auth()->user()->is_super_admin)
          <div class="pull-right">
            <form method="get" action="#" class="fancy-form form-inline">
                <label for="title">
                  Company
                </label>
                <select class="form-control" name="company_select" id="company_select" style="margin-top: -5px;">
                   <option value="">------ Choose a Company ------</option>
                   @foreach($filter_companies as $company)
                    <option value="{{ $company->id }}">{{ $company->title }}</option>
                   @endforeach
                </select>
            </form>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <form method="get" action="#" class="fancy-form form-inline">
                <label for="title">
                  Sector
                </label>
                <select class="form-control" name="sector_select" id="sector_select" style="margin-top: -5px;">
                   <option value="">------ Choose a Sector ------</option>
                   @foreach($sectors as $sector)
                    <option value="{{ $sector->id }}">{{ $sector->title }}</option>
                   @endforeach
                </select>
            </form>
          </div>
          @endif
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                    @if(Session::has('cancel_status')) 
                      /  <a href="{{ route('admin.deals.delete.undo', ['id' => Session::get('cancel_status') ]) }}">Undo</a> 
                    @endif
                 </div>
               @endif

               <div id="deals_section">
                 <div class="row">
                      <input type="hidden" name="deals_limit" id="deals_limit" value="12"/>
                      <input type="hidden" name="deals_count" id="deals_count" value="{{ $deals_count }}" />
                      <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                       <?php $index = 0; ?>
                       @foreach($deals as $deal)
                           <?php $index = $index + 1; ?>
                           <div class="col-md-3">
                                <div class="box {{ ($deal->status == 'expired') ? 'expired' : 'ongoing' }}" id="deal_box_{{ $deal->id }}" style="position: static;"><!-- default, danger, warning, info, success -->
                                  <div class="box-title"><!-- add .noborder class if box-body is removed -->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="{{ asset('images/companies/'.$deal->company->imagePath) }}" alt="{{ $deal->company->title }}"  style="width: 100px; height: 100px;border-radius: 50%;" class="img-responsive"/>
                                        </div>
                                        <div class="col-md-8">
                                            <h2 style="font-weight: bolder; text-shadow: 2px 2px gray;margin-top: 10px;"><a href="#">{{ $deal->company->title }}</a></h2><br>
                                            <small class="block">Deal: @if($deal->is_condition == 1) {!! $deal->description !!} @else {{ $deal->discount.'%' }} @endif</small>
                                            <small class="block">Number of deals:  {{ $deal->number_of_deals }}</small>
                                            <small class="block">Start Date:  {{ $deal->start_date }}</small>
                                            <small class="block">End Date:  {{ $deal->end_date }}</small>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="box-body text-center">
                                    <div data-countdown="{{ $deal->end_date }}" data-start-date="{{ $deal->start_date }}"  data-id="{{ $deal->id }}" class="deal_countdown"></div>
                                  </div>
                                  <div class="dropdown" style="position: absolute; top: 0px; right: 15px;">
                                    <button class="btn {{ ($deal->status == 'expired') ? 'expired' : 'ongoing' }} dropdown-toggle btn_deal_{{ $deal->id }}" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                      <i class="fa fa-cog" style="font-size: 20px;"></i>
                                      <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                      @if(is_user_allowed(auth()->user()->id, 'Show Results'))
                                        
                                           <li id="list_operation_{{ $deal->id }}">
                                             <a href="{{ route('admin.deal.results', ['id' => $deal->id ]) }}" title="Show Results" class="btn-quick" data-value="{{ $deal->id }}">
                                                 <i class="fa fa-tags"></i>&nbsp;Show Results
                                              </a>
                                           </li>   
                                      @endif
                                      @if(is_user_allowed(auth()->user()->id, 'Update Deal'))
                                      <li>
                                        <a href="{{ route('admin.deals.edit', ['id' => $deal->id ]) }}" title="{{t('Edit')}}" class="btn-quick btn_modal" data-value="{{ $deal->id }}"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
                                      </li>
                                      @endif
                                      @if(is_user_allowed(auth()->user()->id, 'Delete Deal'))
                                      <li class="divider"></li>
                                      <li>
                                        <a href="{{ route('admin.deals.delete', ['id' => $deal->id ]) }}" title="{{t('Delete')}}" class="btn-quick btn_delete"><i class="fa fa-trash-o"></i>&nbsp;Delete</a>
                                      </li>
                                      @endif
                                    </ul>
                                  </div>
                                </div>
                           </div>  
                           @if($index % 4 == 0)
                              </div>
                              <div class="row">
                           @endif
                       @endforeach
                   </div>
               </div>

               <a href="{{ route('admin.deals.more') }}" class="text-center btn btn-primary btn-lg" id="show_more" style="width: 100%">Show More Deals</a>

        </div>
   </div>

   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- header modal -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">EDIT DEAL</h4>
          </div>

          <!-- body modal -->
          <div class="modal-body">
            
             {!! Form::open(['url' => Request::route()->getPrefix().'/deals/store', 'files' => true, 'id' => 'front-form', 'class' => 'form-with-list', 'novalidate']) !!}

               <fieldset>


                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Deal's Type
                        </label>
                        <div class="input-group input-group-lg">
                           <label class="radio">
                              <input type="radio" name="is_condition" id="discount_type" value="0" checked="checked">
                              <i></i> Discount
                            </label>

                            <label class="radio">
                              <input type="radio" name="is_condition" id="promotional_text_type" value="1">
                              <i></i> Promotional Text
                            </label>

                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row" id="discount_section_type">
                    <div class="form-group">
                      <div class="col-md-2 col-sm-2">
                        <label for="title" style="white-space: nowrap;">
                          Percentage Discount
                        </label>
                        <div class="input-group input-group-lg">
                          {!! Form::input('text', 'discount', null, ['id' => 'txt_discount', 'class' => 'form-control required', 'required']) !!}
                          <span class="input-group-addon" id="sizing-addon1">%</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Promotional Text 
                        </label>
                        {!! Form::textarea('description', null, ['id' => 'txt_description', 'class' => 'form-control app_editor required', 'required']) !!}
                      </div>
                    </div>
                  </div>

                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Maximum Deals
                        </label>
                        {!! Form::input('text', 'number_of_deals', null, ['id' => 'txt_number_of_deals', 'class' => 'form-control required digits', 'required']) !!}
                      </div>
                    </div>
                  </div>

                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Deal Start Date 
                        </label>
                        {!! Form::input('text', 'start_date', null, ['id' => 'txt_start_date', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>

                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Deal End Date 
                        </label>
                        {!! Form::input('text', 'end_date', null, ['id' => 'txt_end_date', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>

                  @if(auth()->user()->is_super_admin)
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                           Company
                        </label>
                        {!! Form::select('company_id', $companies, null, ['id' => 'txt_company_id', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  @endif
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::hidden('id', null, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>

            {!! Form::close() !!}

          </div>

        </div>
      </div>
    </div>  

    @if(Session::has('api_deal'))
    <script>
        var database = firebase.database();

        var deal_id = {{ Session::get('api_deal') }};

        var deal_url = "{{ route('admin.deals.show') }}";

        $.ajax({
            method: 'POST',
            url: deal_url,
            data: { id: deal_id, "_token": "{{ csrf_token() }}" },
            async: false,
            success: function(response) {

                 var deal = response.deal;

                 var firebase_deal = {
                    id: deal.id,
                    discount_code: parseInt(deal.discount_code),
                    company_image: deal.company.imagePath,
                    company_name: deal.company.title,
                    number_of_deals: deal.number_of_deals,
                    start_date: deal.start_date,
                    end_date: deal.end_date,
                    deleted: deal.deleted,
                    sector_id: deal.company.sector_id
                }
                
                if(deal.discount != null && deal.discount != '') {
                    firebase_deal.discount = deal.discount;
                }

                if(deal.description != null && deal.description != '') {
                    firebase_deal.description = deal.description;
                }

                @if(Session::has('updated_deal'))
                   database.ref('deals').orderByChild('discount_code').equalTo({{ Session::get("updated_deal") }}).on('child_added', function(snapshot){
                        database.ref('deals/'+snapshot.key).update(firebase_deal);
                    });
                  
                    database.ref('deals').orderByChild('discount_code').equalTo({{ Session::get("updated_deal") }}).on('child_changed', function(snapshot){
                        database.ref('deals/'+snapshot.key).update(firebase_deal);
                    });
                @else 
                   database.ref('deals').push(firebase_deal);
                  //  database.ref('deals').on('child_added', function(snapshot){
                  //       database.ref('deals').push(snapshot.val());
                  //  });
                @endif

            }
        });


    </script> 
    @endif

    <script type="text/javascript">

      $('#txt_start_date').datetimepicker({
           format: 'YYYY-MM-DD HH:mm:ss'
      });

      $('#txt_end_date').datetimepicker({
           format: 'YYYY-MM-DD HH:mm:ss'
      });

      function change_deal_type() {
          if($('#promotional_text_type').is(':checked')) {
              $("#discount_section_type").hide();
              $("#conditions_type").show();
              $('#deal_type_switcher').val('1');
          }
          else if($("#discount_type").is(":checked")) {
              $("#discount_section_type").show();
              $("#conditions_type").hide();
              $('#deal_type_switcher').val('0');
          }
      }

      $(function(){

          if(parseInt($("#deals_count").val()) < parseInt($("#deals_limit").val()) ) {
               $("#show_more").hide();
          }
          
          $(document).on("click", "#show_more", function(evt){
                evt.preventDefault();

                $("#show_more").prop("disabled", true);

                $.ajax({
                     method: "POST",
                     url: $(this).attr('href'),
                     data: { deals_limit: $("#deals_limit").val(), "_token": $("#_token").val() },
                     success: function(response){
                          var number_of_deals = parseInt($("#deals_limit").val()) + 12;
                          $("#deals_limit").val( number_of_deals );
                          if(number_of_deals >= parseInt($("#deals_count").val())) {
                               $("#show_more").hide();
                          }
                          $("#deals_section").append(response);
                          $("#show_more").prop("disabled", false);
                     }
                });
          });

          $('[data-countdown]').each(function() {

                var $this = $(this), end_date = $(this).data('countdown'), start_date = $(this).data('start-date');

                var start_date = start_date.split(" ");
                var end_date = end_date.split(" ");

                var start_date_parts = start_date[0];
                var start_time_parts = start_date[1];

                var end_date_parts = end_date[0];
                var end_time_parts = end_date[1];

                start_date = start_date_parts.split("-");
                start_time = start_time_parts.split(":");

                end_date = end_date_parts.split("-");
                end_time = end_time_parts.split(":");

                var start_year = start_date[0];
                var start_month = start_date[1];
                var start_day = start_date[2];

                var start_hour = start_time[0];
                var start_minute = start_time[1];
                var start_second = start_time[2];
                
                var end_year = end_date[0];
                var end_month = end_date[1];
                var end_day = end_date[2];

                var end_hour = end_time[0];
                var end_minute = end_time[1];
                var end_second = end_time[2];

                var current_date = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), new Date().getHours(), new Date().getMinutes(), new Date().getSeconds());

                var start_date = new Date(start_year, parseInt(start_month) - 1, start_day, start_hour, start_minute, start_second);
                var end_date = new Date(end_year, parseInt(end_month) -1 , end_day, end_hour, end_minute, end_second);


 

                if( start_date.getTime() > current_date.getTime() ) {
                     $(this).html("Not yet started");
                }
                else if( end_date.getTime() < current_date.getTime()) {
                     $(this).html("Expired");
                }
                else {

                    var deal_id = $(this).data('id');
                    $(this).countdown(end_date, function(event) {
                      $(this).html(event.strftime('%D days %H:%M:%S'));
                    })
                    .on('finish.countdown', function(evt){
                          
                          $.ajax({
                               url: '../{{ Request::route()->getPrefix() }}/deals/expire_deal/'+deal_id,
                               async: false,
                               success: function(response) {
                                  $("#deal_box_"+deal_id).attr('class', 'box expired');
                                  $(".btn_deal_"+deal_id).removeClass('ongoing');
                                  $(".btn_deal_"+deal_id).addClass('expired');
                                  $this.html('Expired');
                               }
                           });

                    });

                }


          });

          $(document).on('click', '.btn_modal', function(evt){
              evt.preventDefault();
              var id = $(this).attr('data-value');
            
              $.get('../{{ Request::route()->getPrefix() }}/deals/'+id+'/edit', function(response){

                   
                   $('#txt_id').val(response.id);


                   if(response.is_condition == null || response.is_condition == 0) {
                       $('#discount_type').prop('checked', true);
                   }
                   else {
                       $('#promotional_text_type').prop('checked', true);
                   }


                   $('#txt_discount').val(response.discount);
                   tinyMCE.get('txt_description').setContent( (response.description != null) ? response.description : '');
                   $('#txt_number_of_deals').val(response.number_of_deals);
                   $('#txt_duration').val(response.duration);
                   $('#txt_duration_to_goal').val(response.duration_to_goal);
                   $('#txt_start_date').val(response.start_date);
                   $('#txt_end_date').val(response.end_date);
                   $("#txt_company_id").val(response.company_id);

                   change_deal_type();

                   $('#modal').modal();
              });
              
          });

          $(document).on('change', 'input[name=is_condition]', function(evt) {
              evt.preventDefault();
              change_deal_type();
         });

          $(document).on('change', '#sector_select', function(evt) {
              evt.preventDefault();

              if($(this).val() == null && $(this).val() == "") {
                  location.reload();
              }
              
              var sector_id = $(this).val();

              $.ajax({
                   url: '{{ route("admin.sector.deals") }}',
                   type: 'POST',
                   data: { sector_id: sector_id, "_token": "{{ csrf_token() }}" },
                   success: function(response){
                      $("#show_more").hide();
                      $("#deals_section").html(response);
                  }
              });

         });

         $(document).on('change', '#company_select', function(evt) {
              evt.preventDefault();

              if($(this).val() == null && $(this).val() == "") {
                  location.reload();
              }
              
              var company_id = $(this).val();

              $.ajax({
                   url: '{{ route("admin.company.deals") }}',
                   type: 'POST',
                   data: { company_id: company_id, "_token": "{{ csrf_token() }}" },
                   success: function(response){
                      $("#show_more").hide();
                      $("#deals_section").html(response);
                  }
              });

         });

          /*
          $(document).on('click', '.btn_delete', function(evt) {
                 evt.preventDefault();
                 if(confirm("{{ t('Do you really want to delete this row ?') }}")) {
                     window.location.href = $(this).attr('href');
                 }
          });
          */
      });
    </script>
@stop