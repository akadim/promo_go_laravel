@extends('layouts.admin')

@section('pageTitle', $deal->title.' Options' )

@section('additional_function')

<button type="button" class="btn btn-primary btn_modal" data-value="-1" ><i class="fa fa-plus"></i> {{t('Add')}}</button>
              
@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>{{ $deal->title_en.' '.t('Options')}}</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>Title</th>
                       <th>اﻹسم</th>
                       <th>Price</th>
                       <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($dealOptions as $dealOption)
                     <tr>
                        <td>{{ $dealOption->title_en }}</td>
                        <td>{{ $dealOption->title_ar }}</td>
                        <td>{{ $dealOption->price }}</td>
                        <td>
                          <a href="{{ route('admin.deals.options.edit', ['deal_id' => $deal->id, 'id' => $dealOption->id ]) }}" title="{{t('Edit')}}" class="btn btn-xs btn-default btn-quick btn_modal" data-value="{{ $dealOption->id }}"><i class="fa fa-pencil"></i></a>
                          <a href="{{ route('admin.deals.options.delete', ['deal_id' => $deal->id, 'id' => $dealOption->id ]) }}" title="{{t('Delete')}}" class="btn btn-xs btn-default btn-quick btn_delete"><i class="fa fa-times"></i></a>
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>

        </div>
   </div>

   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <!-- header modal -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">{{t('EDIT DEAL')}}</h4>
          </div>

          <!-- body modal -->
          <div class="modal-body">
            
             {!! Form::open(['url' => 'admin/deals/options/'.$deal->id.'/store', 'files' => true, 'id' => 'front-form', 'novalidate']) !!}
                <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="txt_title_en">
                          Title in English
                        </label>
                        {!! Form::input('text', 'title_en', null, ['id' => 'txt_title_en', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="txt_title_ar">
                          Title in Arabic
                        </label>
                        {!! Form::input('text', 'title_ar', null, ['id' => 'txt_title_ar', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          {{t('Price')}}
                        </label>
                        {!! Form::input('text', 'price', null, ['id' => 'txt_price', 'class' => 'form-control required', 'required number']) !!}
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::hidden('id', null, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                    {!! Form::hidden('deal_id', $deal->id, ['id' => 'txt_deal_id', 'class' => 'form-control']) !!}
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>
            {!! Form::close() !!}

          </div>

        </div>
      </div>
    </div>  
    <script type="text/javascript">
      $(function(){
          $(document).on('click', '.btn_modal', function(evt){
              evt.preventDefault();
              var id = $(this).attr('data-value');
            
              $.get('../../deals/options/{{ $deal->id }}/'+id+'/edit', function(response){

                   deal_id = response['deal_id'];

                   $('#txt_id').val(response.id);
                   $('#txt_title_en').val(response.title_en);
                   $('#txt_title_ar').val(response.title_ar);
                   $('#txt_price').val(response.price);
                   $('#modal').modal();
              });
              
          });

          $(document).on('click', '.btn_delete', function(evt) {
                 evt.preventDefault();
                 if(confirm("{{ t('Do you really want to delete this row ?') }}")) {
                     window.location.href = $(this).attr('href');
                 }
          });
      });
    </script>
@stop