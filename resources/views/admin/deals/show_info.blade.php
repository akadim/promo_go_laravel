
<div id="content">
   <div id="siteNotice">
   </div>
   <h1 id="firstHeading" class="firstHeading">{{ $deal->company->title }}</h1>
   <div id="bodyContent" style="width: 310px;">
     <p style="font-size: 16px;">
       {!! ($deal->is_condition) ? $deal->description : "Discount: -".$deal->discount."%" !!} <br>
       @if($validated == 'yes')
         <span class="editable_section">
           Time to goal: 
           <span id="editable_section_text_{{ $deal_position->id }}">
              <span data-value="txt_{{ $deal_position->id }}" class="editable_text_content">{{ $deal_position->duration_to_goal }}</span> {{ ($deal_position->duration_time_type != null) ? $deal_position->duration_time_type : "" }} &nbsp;
              <a class="editable_section_link" href="" data-position="{{ $deal_position->id }}"><i class="fa fa-pencil"></i></a>
           </span>
            <span id="editable_section_form_{{ $deal_position->id }}" class="hidden_section"> 
               <input type="text" id="txt_{{ $deal_position->id }}" name="txt_editabl_section" data-position="{{ $deal_position->id }}" value="{{ $deal_position->duration_to_goal }}" style="width: 80px;" required class="txt_editable_form"/>  min 
               <button class="editable_section_btn  btn btn-primary" data-textfield="txt_{{ $deal_position->id }}">OK</button>
               <a class="editable_section_cancel_link btn btn-danger" href="" data-position="{{ $deal_position->id }}">X</a>
            </span>
         </span>
       @endif
     </p>
   </div>
</div>