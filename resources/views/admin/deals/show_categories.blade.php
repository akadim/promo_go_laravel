<div class="form-group">
  <div class="col-md-12 col-sm-12">
    <label for="description">
      {{t('Category')}}
    </label>
    {!! Form::select('category_id', $categories, null, ['id' => 'txt_category', 'class' => 'form-control required']) !!}
  </div>
</div>