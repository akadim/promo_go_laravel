@extends('layouts.admin')

@section('pageTitle', 'Deal Results')

@section('additional_function')
   @if(is_user_allowed(auth()->user()->id, 'Set Deal Position') && ( $deal->status != 'expired' ))
     <a href="{{ route('admin.deals.marking', ['id' => $deal->id ]) }}" title="Marking the positions" class="btn-quick btn btn-primary">
       <i class="fa fa-map-marker"></i>&nbsp;Set the positions
     </a>  
   @endif       
@stop

@section('body_content')
   
   <style>
     #map-canvas {
         margin: 0;
         padding: 0;
         height: 400px;
         max-width: none;
      }
      #map-canvas img {
         max-width: none !important;
      }
   </style>
   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>Deal Results</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

              <div class="alert alert-success" id="deal_countdown" style="display: none;">
                <strong>Remaining Time: <span class="countdown"></span></strong>
              </div>

               <div class="alert alert-danger" id="deal_expiration_message" style="display: none;">
                <strong>This Deal has long been Expired!</strong>
              </div>

            {!! Form::open(['url' => 'something', 'files' => true, 'id' => 'front-form', 'class' => 'form-with-list', 'novalidate']) !!}
                <fieldset> 

                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Store
                        </label>
                        {!! Form::hidden('id', $deal->id, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                        {!! Form::select('store_id', $stores_params, null, ['id' => 'txt_store_id', 'class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>

                </fieldset>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="alert alert-info">
                        <span id="deal_total_section"></span> Total Deals: <span id="deal_available_section">{{ $deal_positions_number }}</span>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img src="{{ asset('images/markers/default.png') }}" alt="Available"> <span id="deal_ongoing_section">{{ $ongoing_deals }}</span>  Available Deals  &nbsp;&nbsp;&nbsp;
                        <img src="{{ asset('images/markers/catched.png') }}" alt="Taken"> <span id="deal_taken_section">{{ $taken_deals }}</span>  Deals Taken  &nbsp;&nbsp;&nbsp;
                        <img src="{{ asset('images/markers/expired.png') }}" alt="Expired"> <span id="deal_expired_section">{{ $expired_deals }}</span>  Deals Expired &nbsp;&nbsp;&nbsp;
                        <img src="{{ asset('images/markers/consumed.png') }}" alt="Expired" style="width: 22px; height: 40px;"> <span id="deal_consumed_section">{{ $consumed_deals }}</span>  Deals Consumed
                    </div>
                  </div>
                </div>

            {!! Form::close() !!}
            <div id="map-canvas"></div>
        </div>
   </div>
   <script type="text/javascript">
      var map;
      var marker;
      var arrMarkers=new Array(0);
      var bounds;

      function initialize_countdown(start_date, end_date) {

          var start_date = start_date.split(" ");
          var end_date = end_date.split(" ");

          var start_date_parts = start_date[0];
          var start_time_parts = start_date[1];

          var end_date_parts = end_date[0];
          var end_time_parts = end_date[1];

          start_date = start_date_parts.split("-");
          start_time = start_time_parts.split(":");

          end_date = end_date_parts.split("-");
          end_time = end_time_parts.split(":");

          var start_year = start_date[0];
          var start_month = start_date[1];
          var start_day = start_date[2];

          var start_hour = start_time[0];
          var start_minute = start_time[1];
          var start_second = start_time[2];
          
          var end_year = end_date[0];
          var end_month = end_date[1];
          var end_day = end_date[2];

          var end_hour = end_time[0];
          var end_minute = end_time[1];
          var end_second = end_time[2];

          var start_date = new Date(start_year, parseInt(start_month) - 1, start_day, start_hour, start_minute, start_second);
          var end_date = new Date(end_year, parseInt(end_month) -1 , end_day, end_hour, end_minute, end_second);

          var end_date_timestamp = end_date.getTime();

          var current_date = new Date();

          if(start_date.getTime() < current_date.getTime() && end_date.getTime() > current_date.getTime()) {
               $("#deal_countdown").show();
               $("#deal_expiration_message").hide();
          }
          else if(end_date.getTime() < current_date.getTime()) {
               $("#deal_countdown").hide();
               $("#deal_expiration_message").show();
          }

          $('.countdown').countdown(end_date_timestamp, function(event) {
                $(this).html('<span style="margin: 0px 20px; ">'+event.strftime('%D days %H:%M:%S')+'</span>');
              }).on('finish.countdown', function(evt){
                   $.ajax({
                       url: '../expire_deal/{{ $deal->id }}',
                       async: false,
                       success: function(response) {
                          window.location.reload();
                       }
                   });
              });

      }

      initialize_countdown('{{ $deal->start_date }}', '{{ $deal->end_date }}');

      
      function initialize_markers() {

          if(arrMarkers) {
             for(var i in arrMarkers) {
                arrMarkers[i].setMap(null);
             }
          }

          arrMarkers = new Array(0);

      }

      function initialize() {
        var mapOptions =  {
                center: new google.maps.LatLng(40.680898,-8.684059),
                zoom: 11,
                mapTypeId: google.maps.MapTypeId.HYBRID
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
      }

      function show_info(current_marker) {
            var contentString = '<div id="content">'+
                                  '<div id="siteNotice">'+
                                  '</div>'+
                                  '<h1 id="firstHeading" class="firstHeading">{{ $deal->company->title }}</h1>'+
                                  '<div id="bodyContent">'+
                                      '<p style="font-size: 16px;">'+
                                         '{!! ($deal->is_condition) ? str_replace($deal->description, "\'", "\\'") : "Discount: -".$deal->discount."%" !!}'+
                                      '</p>'+
                                  '</div>'+
                                '</div>';

            var infowindow = new google.maps.InfoWindow({
               content: contentString
            });

            infowindow.open(map, current_marker);
      }


      google.maps.event.addDomListener(window, "load", initialize);

      function get_marker_image(status) {

           var marker_image = '{{ asset("images/markers/consumed.png") }}';

           switch(status) {
              case "ongoing": marker_image = '{{ asset("images/markers/default.png") }}';break;
              case "catched": marker_image = '{{ asset("images/markers/catched.png") }}';break;
              case "expired": marker_image = '{{ asset("images/markers/expired.png") }}';break;
              case "used": marker_image = '{{ asset("images/markers/consumed.png") }}';break;
           }

           return marker_image;
      }

      function searchAddress() {
         var addressInput = $('#txt_store_id option:selected').text();
         var addressInputId = $('#txt_store_id').val();

         initialize_markers();

         $.get('../{{ $deal_id }}/'+addressInputId+'/markers', function(response){
               
               var coordinates = response.data;
               
               var deal_positions_catched = response.deal_positions_catched;
               var deal_positions_expired = response.deal_positions_expired;
               var deal_positions_available = response.deal_positions_available;
               var deal_positions_used = response.deal_positions_used;
               var deal_positions_ongoing = response.deal_positions_ongoing;
               
               $('#deal_ongoing_section').html(deal_positions_ongoing);
               $('#deal_taken_section').html(deal_positions_catched);
               $('#deal_expired_section').html(deal_positions_expired);
               $('#deal_consumed_section').html(deal_positions_used);
               $('#deal_available_section').html(deal_positions_available);

               if(coordinates.length > 0) {
                    for(var i = 0; i < coordinates.length; i++) {
                        var latLongs = { lat: coordinates[i][0], lng: coordinates[i][1] }
                        var image_path = get_marker_image(coordinates[i][3]);
                        var marker = createMarker(latLongs, image_path);
                        /*
                        var start_date = coordinates[i][2];
                        var end_date = coordinates[i][3];
                        */

                        marker.addListener('click', function(){
                             show_info(this);  
                        });

                        arrMarkers.push(marker);
                    }
               } 

               var geocoder = new google.maps.Geocoder();

               geocoder.geocode({ address: addressInput}, function(results, status){

                   if(status == google.maps.GeocoderStatus.OK) {
                      
                       var myResult = results[0].geometry.location;

                       map.setCenter(myResult);

                       map.setZoom(17);
                   }
                   else {
                      alert("The Geocode was not successful for the following reason: "+status);
                   }
               });

         });

      }

      function createMarker(latlng, image_path) {

         marker = new google.maps.Marker({
             map: map,
             position: latlng,
             draggable: false,
             icon: image_path
         });

         return marker;

      }


      $(document).on('change', '#txt_store_id', function(evt){
           evt.preventDefault();

           searchAddress();
      });

   </script>
@stop