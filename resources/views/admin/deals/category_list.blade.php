 @if($subcategories_number > 0)
 <div class="row">
    <div class="form-group">
      <div class="col-md-12 col-sm-12">
        <label for="description">
          {{t('SubCategory')}}
        </label>
        {!! Form::select('sub_category_id', $subcategories, null, ['id' => 'txt_sub_category', 'class' => 'form-control required', 'default' => t('Choose an Option')  ]) !!}
      </div>
    </div>
  </div>
 @endif

@if($travelLocations_number > 0)
  <div class="row">
    <div class="form-group">
      <div class="col-md-12 col-sm-12">
        <label for="description">
          {{t('Location')}}
        </label>
        {!! Form::select('travel_location_id', $travelLocations, null, ['id' => 'txt_travel_location', 'class' => 'form-control required', 'empty' => t('Choose an Option')]) !!}
      </div>
    </div>
  </div>
@endif