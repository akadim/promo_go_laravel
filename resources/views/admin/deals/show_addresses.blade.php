<div class="form-group">
  <div class="col-md-12 col-sm-12">
    <label for="description">
      Company Address
    </label>
    {!! Form::select('company_addresses[]', $company_addresses, null, ['id' => 'txt_company_address', 'class' => 'form-control required', 'size' => '3']) !!}
  </div>
</div>