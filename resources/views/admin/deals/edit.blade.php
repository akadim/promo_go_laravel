@extends('layouts.admin')

@section('pageTitle', t('Deals'))

@section('body_content')

<div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>{{t('EDIT DEAL')}}</strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">
         
           {!! Form::open(['url' => Request::route()->getPrefix().'/deals/store', 'files' => true, 'id' => 'front-form', 'class' => 'form-with-list', 'novalidate']) !!}

               <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label class="switch switch switch-round">
                          <input type="checkbox" name="is_condition" id="deal_type_switcher" @if($deal->is_condition == 1) checked="" @endif>
                          <span class="switch-label" data-on="YES" data-off="NO"></span>
                          <span> is it a condition ?</span>
                        </label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row" id="discount_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Discount
                        </label>
                        {!! Form::input('text', 'discount', $deal->discount, ['id' => 'txt_number_of_deals', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  
                  
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Conditions 
                        </label>
                        {!! Form::textarea('description', $deal->description, ['id' => 'txt_description', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Number of Deals 
                        </label>
                        {!! Form::input('text', 'number_of_deals', $deal->number_of_deals, ['id' => 'txt_number_of_deals', 'class' => 'form-control required digits', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Duration 
                        </label>
                        {!! Form::input('text', 'duration', $deal->duration, ['id' => 'txt_duration', 'class' => 'form-control required digits', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Duration to the Goal 
                        </label>
                        {!! Form::input('text', 'duration_to_goal', $deal->duration_to_goal, ['id' => 'txt_duration_to_goal', 'class' => 'form-control required digits', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  @if(auth()->user()->role == 'super_admin')
                  <div class="row" id="conditions_type">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                           Company
                        </label>
                        {!! Form::select('brand_id', $brands, $deal->brand_id, ['id' => 'txt_company_id', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  @endif
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {!! Form::hidden('id', $deal->id, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>

            {!! Form::close() !!}

        </div>

</div>
<script type="text/javascript">
	$(function(){

       $(document).on('change', '#deal_type_switcher', function(evt) {
            evt.preventDefault();
            if($(this).is(':checked')) {
                $("#discount_type").hide();
                $("#conditions_type").show();
            }
            else {
                $("#discount_type").show();
                $("#conditions_type").hide();
            }
       });
      
       $("#deal_type_switcher").trigger("change");

	});
</script>

@stop