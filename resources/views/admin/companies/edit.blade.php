{!! Form::open(['url' => 'admin/categories/store', 'files' => true, 'id' => 'front-form', 'class' => 'form-with-list', 'novalidate']) !!}
<div class="tabs nomargin">
  <!-- tabs -->
  <ul class="nav nav-tabs nav-justified">
    <li class="active">
      <a href="#jtab1_nobg" data-toggle="tab">
        <i class="fa fa-plus"></i> New Category
      </a>
    </li>
    <li class="">
      <a href="#jtab2_nobg" data-toggle="tab">
        <i class="fa fa-cogs"></i> Copy Category
      </a>
    </li>
  </ul>

  <!-- tabs content -->
  <div class="tab-content transparent">
    <div id="jtab1_nobg" class="tab-pane active">
       
       <fieldset>
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 col-sm-12">
                <label for="title">
                  Title in English
                </label>
                {!! Form::input('text', 'title_en', null, ['id' => 'txt_title_en', 'class' => 'form-control required', 'required']) !!}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 col-sm-12">
                <label for="title">
                  Title in Arabic
                </label>
                {!! Form::input('text', 'title_ar', null, ['id' => 'txt_title_ar', 'class' => 'form-control required', 'style' => 'text-align: right;']) !!}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 col-sm-12">
                <label for="title">
                  {{t('Icon')}}
                </label>
                {!! Form::file('icon_path', ['id' => 'icon_path', 'class' => 'form-control required', 'data-rule-extension' => 'png|jpe?g']) !!}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 col-sm-12">
                <label for="description">
                  Description in English
                </label>
                {!! Form::textarea('description_en', null, ['id' => 'txt_description_en', 'class' => 'form-control app_editor']) !!}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 col-sm-12">
                <label for="description">
                  Description in Arabic
                </label>
                {!! Form::textarea('description_ar', null, ['id' => 'txt_description_ar', 'class' => 'form-control app_editor']) !!}
              </div>
            </div>
          </div>
        </fieldset>

    </div>

    <div id="jtab2_nobg" class="tab-pane">
      <p>Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et, iaculis ac massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
  </div>

</div>
<fieldset>
  <div class="row">
    <div class="col-md-12">
      <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
        {!! Form::hidden('id', null, ['id' => 'txt_id', 'class' => 'form-control']) !!}
        {{t('SAVE DATA')}}
      </button>
    </div>
  </div>
</fieldset>
{!! Form::close() !!}