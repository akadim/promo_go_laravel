@extends('layouts.admin')

@section('pageTitle', 'Promo Clients')

@section('additional_function')

@if(is_user_allowed(auth()->user()->id, 'Add Company'))
<button type="button" class="btn btn-primary btn_modal" data-value="-1" ><i class="fa fa-plus"></i> Add</button>
@endif

@stop

@section('body_content')

   <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
          <span class="title elipsis">
            <strong>Promo Clients </strong> <!-- panel title -->
          </span>

          <!-- right options -->
          <!-- /right options -->

        </div>

        <!-- panel content -->
        <div class="panel-body">

            
               @if(Session::has('status'))
                 <div class="alert alert-success margin-bottom-30">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{Session::get('status')}}
                    @if(Session::has('cancel_status')) 
                      /  <a href="{{ route('admin.companies.delete.undo', ['id' => Session::get('cancel_status') ]) }}">Undo</a> 
                    @endif
                 </div>
               @endif
            

            
            
            <table class="table table-striped table-bordered table-hover" id="bee_datatable">
                <thead>
                    <tr>
                       <th>Title</th>
                       <th>Parent</th>
                       <th>Sector</th>
                       <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($companies as $company)
                     <tr>
                        <td>
                            <img src="{{ asset('images/companies/'.$company->imagePath) }}" style="width: 150px; height: 70px;"/>
                            {{ $company->title }}
                        </td>
                        <td>
                          {{ $company->parent_company->title }}
                        </td>
                        <td>
                          {{ ($company->sector == null) ? "uncategorized" : $company->sector->title }}
                        </td>
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              {{t('Choose Action')}}
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              @if(is_user_allowed(auth()->user()->id, 'Update Company'))
                              <li>
                                <a href="{{ route('admin.companies.edit', ['id' => $company->id ]) }}" title="{{t('Edit')}}" class="btn_modal" data-value="{{ $company->id }}"><i class="fa fa-pencil"></i> Edit </a>
                              </li>
                              @endif
                              @if(is_user_allowed(auth()->user()->id, 'Delete Company'))
                              <li class="divider"></li>
                              <li>
                                <a href="{{ route('admin.companies.delete', ['id' => $company->id ]) }}" title="{{t('Delete')}}" class="btn_delete"><i class="fa fa-trash-o"></i> Delete </a>
                              </li>
                              @endif
                            </ul>
                          </div>
                        </td>
                     </tr>
                   @endforeach
                </tbody>
            </table>

        </div>
   </div>

   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- header modal -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">EDIT COMPANY</h4>
          </div>

          <!-- body modal -->
          <div class="modal-body">
            
             {!! Form::open(['url' => Request::route()->getPrefix().'/companies/store', 'files' => true, 'id' => 'front-form', 'class' => 'form-with-list', 'novalidate']) !!}

               <fieldset>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Title 
                        </label>
                        {!! Form::input('text', 'title', null, ['id' => 'txt_title', 'class' => 'form-control required', 'required']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Image Path
                        </label>
                        {!! Form::file('imagePath', ['id' => 'imagePath', 'class' => 'form-control', 'data-rule-extension' => 'png|jpe?g']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Website 
                        </label>
                        {!! Form::input('url', 'website', null, ['id' => 'txt_website', 'class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Parent 
                        </label>
                        {!! Form::select('parent_id', $companies_list, null, ['id' => 'txt_parent_id', 'class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12">
                        <label for="title">
                          Sector 
                        </label>
                        {!! Form::select('sector_id', $sectors_list, null, ['id' => 'txt_sector_id', 'class' => 'form-control required']) !!}
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30" type="submit">
                      {!! Form::hidden('id', null, ['id' => 'txt_id', 'class' => 'form-control']) !!}
                      {{t('SAVE DATA')}}
                    </button>
                  </div>
                </div>

            {!! Form::close() !!}

          </div>

        </div>
      </div>
    </div>  
    <script type="text/javascript">
      $(function(){

          $(document).on('click', '.btn_modal', function(evt){
              evt.preventDefault();
              var id = $(this).attr('data-value');
            
              $.get('../{{ Request::route()->getPrefix() }}/companies/'+id+'/edit', function(response){
                   
                   $('#txt_id').val(response.id);
                   $('#txt_website').val(response.website);
                   $('#txt_title').val(response.title);
                   $("#txt_user_id").val(response.user_id);
                   $("#txt_parent_id").val(response.parent_id);
                   $("#txt_sector_id").val(response.sector_id);


                   $('#modal').modal();
              });
              
          });

          /*
          $(document).on('click', '.btn_delete', function(evt) {
                 evt.preventDefault();
                 if(confirm("{{ t('Do you really want to delete this row ?') }}")) {
                     window.location.href = $(this).attr('href');
                 }
          });
          */
      });
    </script>
    @if(Session::has('api_company'))
    <script>
        var database = firebase.database();

        var company_id = {{ Session::get('api_company') }}

        $.ajax({
            method: 'POST',
            url: '{{ route("admin.company") }}',
            data: { id: company_id, "_token": "{{ csrf_token() }}" },
            async: false,
            success: function(response) {

                 var company = response.company;

                 var firebase_company = {
                    id: company.id,
                    title: company.title,
                    imagePath: company.imagePath,
                    website: company.website,
                    parent_id: company.parent_id,
                    sector_id: company.sector_id,
                    deleted: company.deleted
                };
                
                @if(Session::has('updated_company'))
                   database.ref('companies').orderByChild('id').equalTo({{ Session::get("updated_company") }}).on('child_added', function(snapshot){
                        database.ref('companies/'+snapshot.key).update(firebase_company);
                    });
                  
                    database.ref('companies').orderByChild('id').equalTo({{ Session::get("updated_company") }}).on('child_changed', function(snapshot){
                        database.ref('companies/'+snapshot.key).update(firebase_company);
                    });
                @else 
                   database.ref('companies').push(firebase_company);
                @endif

            }
        });


    </script> 
    @endif
@stop