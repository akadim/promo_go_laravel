<!doctype html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title>{{ t('PROMO CATCH') }} | @yield('pageTitle')</title>
    <meta name="description" content="" />
    <meta name="Author" content="KADIM Ahmed" />

    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

    <!-- WEB FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

    <!-- CORE CSS -->
    <link href="{{ asset('assets/admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- THEME CSS -->
    <link href="{{ asset('assets/admin/css/essentials.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/css/layout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/css/color_scheme/green.css') }}" rel="stylesheet" type="text/css" id="color_scheme" />
    <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-tagsinput.css') }}">
    <link href="{{ asset('assets/admin/css/layout-datatables.css') }}" rel="stylesheet" type="text/css" />

    <!-- JAVASCRIPT FILES -->
    <script type="text/javascript">var plugin_path = "{{ asset('assets/admin/plugins') }}/";</script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQJWnmep3QkI-hAOh3G0D2rQBLs70-5Qg&amp;libraries=places"
   type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/jquery/jquery-2.1.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/jquery.numeric.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/additional-methods.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/moment.js')}}"></script>
    <script src="{{ asset('assets/front/js/jquery.countdown.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/plugins/toastr/toastr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/bootstrap-tagsinput.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/jquery.geocomplete.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/app.js') }}"></script>
    <script type="text/javascript">
       $(function(){

           if($("#bee_datatable")) {
               $("#bee_datatable").DataTable();
           }

           if($("#front-form")) { 
              jQuery.extend(jQuery.validator.messages,{
                    required: "{{ t('This field is required') }}.",
                    remote: "Please fix this field.",
                    email: "{{ t('Email invalide') }}.",
                    url: "{{ t('Invalid URL') }}.",
                    date: "{{ t('Invalid Date') }}.",
                    dateISO: "Please enter a valid date (ISO).",
                    number: "{{ t('Invalid Number') }}.",
                    digits: "{{ t('Only Numbers are required') }}.",
                    creditcard: "Please enter a valid credit card number.",
                    equalTo: "Please enter the same value again.",
                    accept: "{{ t('Please enter a value with a valid extension')}}.",
                    maxlength: $.validator.format("Please enter no more than {0} characters."),
                    minlength: $.validator.format("Please enter at least {0} characters."),
                    rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
                    range: $.validator.format("Please enter a value between {0} and {1}."),
                    max: $.validator.format("Please enter a value less than or equal to {0}."),
                    min: $.validator.format("Please enter a value greater than or equal to {0}.")
                });

              $(".multiple-form").validate();

              $("#front-form").validate(); 

              tinymce.init({
                selector: ".app_editor",  // change this value according to your HTML
                  plugins: [
                    "advlist autolink lists link  charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste jbimages"
                ],
              });

           }
       });
    </script>
    
    
    <script src="https://www.gstatic.com/firebasejs/4.1.2/firebase.js"></script>
    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyCtHHHyCzpVj9WgIr4NoEfmqFvf_Lz6Wbg",
        authDomain: "promogo-a39f4.firebaseapp.com",
        databaseURL: "https://promogo-a39f4.firebaseio.com",
        projectId: "promogo-a39f4",
        storageBucket: "promogo-a39f4.appspot.com",
        messagingSenderId: "755641311305"
      };
      firebase.initializeApp(config);
    </script>

    <style>
      #map {
        width: 100%;
        height: 400px;
        background-color: grey;
      }
    </style>

  </head>
  <!--
    .boxed = boxed version
  -->
  <body>


    <!-- WRAPPER -->
    <div id="wrapper">

      <!-- 
        ASIDE 
        Keep it outside of #wrapper (responsive purpose)
      -->
      <aside id="aside">
        <!--
          Always open:
          <li class="active alays-open">

          LABELS:
            <span class="label label-danger pull-right">1</span>
            <span class="label label-default pull-right">1</span>
            <span class="label label-warning pull-right">1</span>
            <span class="label label-success pull-right">1</span>
            <span class="label label-info pull-right">1</span>
        -->
        @if(!auth()->user()->is_super_admin)
         {{--*/ $current_company = current_company(auth()->user()->id) /*--}}  
          <img src="{{ asset('images/companies/'.$current_company->imagePath) }}" alt="" style="width: 100%; height: 25%; margin-bottom: 10px;"/>
        @endif
        <nav id="sideNav"><!-- MAIN MENU -->
          <ul class="nav nav-list">
            <li><!-- dashboard -->
              <a class="dashboard" href="{{ route('admin.dashboard') }}"><!-- warning - url used by default by ajax (if eneabled) -->
                <i class="main-icon fa fa-dashboard"></i> <span>{{t('Dashboard')}}</span>
              </a>
            </li>
            @foreach($sidebar as $sidebar_item)
               <li><!-- dashboard -->
                  <a class="dashboard" @if(!is_null($sidebar_item['url']) && $sidebar_item['url'] != '') href="{{ $sidebar_item['url'] }}" @endif>
                    @if(!empty($sidebar_item['child_permissions']))
                     <i class="fa fa-menu-arrow pull-right"></i>
                    @endif
                    <i class="main-icon fa fa-users"></i> <span>{{ $sidebar_item['title'] }}</span>
                  </a>
                  @if(!empty($sidebar_item['child_permissions']))
                    <ul>
                      @foreach($sidebar_item['child_permissions'] as $sidebar_child_item)
                         <li><a href="{{ url(Request::route()->getPrefix().''.$sidebar_child_item->path_link) }}">{{ $sidebar_child_item->title }}</a></li>
                      @endforeach
                    </ul>
                  @endif
            @endforeach
            @if(auth()->user()->is_super_admin)
             <li><!-- dashboard -->
              <a class="dashboard">
                <i class="fa fa-menu-arrow pull-right"></i>
                <i class="main-icon fa fa-cogs"></i> <span>Settings</span>
              </a>
              <ul><!-- submenus -->
                <li><a href="{{ route('admin.profiles') }}">Profiles</a></li>
                @if(is_user_allowed(auth()->user()->id, 'Sectors'))
                <li><a href="{{ route('admin.sectors') }}">Sectors</a></li>
                @endif
              </ul>
            </li>
            @endif
          </ul>

        </nav>

        <span id="asidebg"><!-- aside fixed background --></span>
      </aside>
      <!-- /ASIDE -->


      <!-- HEADER -->
      <header id="header">

        <!-- Mobile Button -->
        <button id="mobileMenuBtn"></button>

        <!-- Logo -->
        <span class="logo pull-left">
           <img src="{{ asset('images/promo_catch.png') }}" alt="Promo Catch" style="width: 200px; height: 50px; "/>
        </span>

        <nav>

          <!-- OPTIONS LIST -->
          <ul class="nav pull-right">

            <!-- USER OPTIONS -->
            <li class="dropdown pull-left">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img class="user-avatar" alt="" src="assets/images/noavatar.jpg" height="34" /> 
                <span class="user-name">
                  <span class="hidden-xs">
                    {{ auth()->user()->fullname }} <i class="fa fa-angle-down"></i>
                  </span>
                </span>
              </a>
              <ul class="dropdown-menu hold-on-click">
                <li><!-- my calendar -->
                  <a href="{{ route('admin.profile') }}"><i class="fa fa-user"></i> Profile</a>
                </li>
                <li class="divider"></li>

                <li><!-- logout -->
                  <a href="{{ route('admin.logout') }}"><i class="fa fa-power-off"></i> Log Out</a>
                </li>

              </ul>
            </li>
            <!-- /USER OPTIONS -->

          </ul>
          <!-- /OPTIONS LIST -->

        </nav>

      </header>
      <!-- /HEADER -->


      <!-- 
        MIDDLE 
      -->
      <section id="middle">


        <!-- page title -->
        <header id="page-header">
          <h1>@yield('pageTitle')</h1>
          <ol class="breadcrumb">
            @foreach($breadcrumbs as $breadcrumb)
                 <li>
                    @if(!$breadcrumb['active'])
                       <a href="{{ $breadcrumb['url']}}"  class="active">{{ $breadcrumb['title']}}</a>
                    @else
                       {{ $breadcrumb['title'] }}
                    @endif
                  </li>
            @endforeach
          </ol>
          <div style="position: absolute;right: 25px;top: 15px;">
              @yield('additional_function')
          </div>
        </header>
        <!-- /page title -->


        <div id="content" class="padding-20">

            @yield('body_content')

        </div>
      </section>
      <!-- /MIDDLE -->

    </div>



  
    

  </body>
</html>