<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">   
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Searching for the best deals in Dubai, Try Bee Dealz">
    <meta property="fb:app_id" content="178733612156994">
    <meta name="layout" content="main">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bee Dealz | @yield('pageTitle') </title>
    <link rel="shortcut icon" href="https://static-cdn.cobone.com/assets/favicon-b07d7371e13c21dc473c8db493a730e8.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="https://static-cdn.cobone.com/assets/apple-touch-icon-f296754f157e7bb565ca77facf1230ba.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://static-cdn.cobone.com/assets/apple-touch-icon-retina-41a8ee9a7c9930346a22e2c8a442d621.png">

    <link href="{{asset('assets/front/css/fonts.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">

    @if(LaravelLocalization::getCurrentLocale() == 'ar')
      <link rel="stylesheet" href="{{asset('assets/front/css/style_rtl.css')}}">
    @endif

    <link rel="stylesheet" href="{{asset('assets/front/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/toastr.css')}}">

    <link rel="canonical" href="https://www.beedealz.com">
    
    <script src="{{asset('assets/front/js/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/jquery.countdown.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/toastr.min.js')}}"></script>
    <script src="{{ asset('assets/front/js/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/JavaScript">
      emsSetEnv("www1");
      emsTracking("145648222","beedealz.com");
    </script>

    
  </head>
  <body class="js-DealSearch js-LazyImages js-SwappingBanners ">

  <script>
      megamenu.docinit({
        menuid:'solidmenu',
        dur:300
      })
    </script>


    
    
    

    <div id="wrapper" class="">
      <div id="page-content-wrapper">

<!-- Header -->
<header>
  <script type="text/javascript" src="{{ asset('assets/front/js/ouibounce.js') }}"></script>
  <div class="container">
    <div class="logorow">
      <div class="col logocolleft">
        <a href="{{ route('home') }}" title="Cobone">
          <img src="{{ asset('bee_dealz_logo.png') }}" alt="Bee Dealz" style="width: 196px; height: 64px;">
        </a>
      </div>
      <div class="col logocolright">

      </div>
    </div>
  </div>
  <div class="container-fluid topshad"></div>
  
</header>

 <section>
  <div class="container-fluid pagebg">
    <div class="container">

      <div class="panelwhite">
        <div class="checkoutrow">
          <div class="checkoutcoltimeline">
            <span class="ico-checkout {{ ($checkout_title == 'order_summary') ? 'ico-checkout-1-blue' : 'ico-checkout-1-gray'}} "></span>
            <span class="checkouttimelinehead {{ ($checkout_title == 'order_summary') ? 'blue' : 'gray'}}">{{ t('Order Summary')}}</span>
          </div>
          <div class="checkoutcoltimeline checkoutcolactive">
            <span class="ico-checkout {{ ($checkout_title == 'payment_type') ? 'ico-checkout-2-blue' : 'ico-checkout-2-gray'}}"></span>
            <span class="checkouttimelinehead {{ ($checkout_title == 'payment_type') ? 'blue' : 'gray'}}">{{ t('Payment Type')}}</span>
          </div>
          <div class="checkoutcoltimeline">
            <span class="ico-checkout {{ ($checkout_title == 'success_title') ? 'ico-checkout-3-blue' : 'ico-checkout-3-gray'}}"></span>
            <span class="checkouttimelinehead {{ ($checkout_title == 'success_title') ? 'blue' : 'gray'}}">{{ t('Success')}}</span>
          </div>
        </div>
        <div class="checkoutrow br">
          <div class="checkoutcoltimeline checkoutcolactive"><span class="checkouttimeline {{ ($checkout_title == 'order_summary') ? 'bluebg' : 'graylightbg'}}"></span></div>
          <div class="checkoutcoltimeline"><span class="checkouttimeline {{ ($checkout_title == 'payment_type') ? 'bluebg' : 'graylightbg'}}"></span></div>
          <div class="checkoutcoltimeline"><span class="checkouttimeline {{ ($checkout_title == 'success_title') ? 'bluebg' : 'graylightbg'}}"></span></div>
        </div>
        @yield('body_content')
      </div>
    </div>
  </div>
</section>

  


<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel" xmlns="http://www.w3.org/1999/html">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modalrow">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <div class="modaltitle" id="cartModalLabel"><span class="ico16 ico16-basket"></span>Your Shopping Cart</div>
        </div>
      </div>
    </div>
  </div>
</div>

        
        

<!-- Banner & Contact Us for Mobile -->
<div class="footercontact"></div>
<script>
  /*****  Banner & Contact Box shifted here for Mobile Only  *****/
  $( document ).ready(function() {
    // Mobile & Tablet only
    if ($(window).width()<700) {
      var varBannerText = $('.spotlightcolright').html();
      $('.spotlightcolright').html('');
      $('.footercontact').html(varBannerText);
    }
  });
</script>







<!-- Footer -->
<footer>


  <!-- Navigation -->
  <div class="holder footerholder">

    <!-- Col #1 -->
    <div class="col footercol3">
      <h4>{{ t('company')}}</h4>
      <ul class="footernav">
        <li><a href="{{ route('about-us') }}">{{ t('about.us')}}</a></li>
        <li><a href="{{ route('contact-us') }}">{{ t('contact.us')}}</a></li>
        <li><a href="https://www.cobone.com/account/register">{{ t('join.our.site')}}</a></li>
        <li><a href="{{ route('terms-conditions') }}">{{ t('terms.conditions')}}</a></li>
        <li><a href="{{ route('privacy-policy') }}">{{ t('privacy.policy')}}</a></li>
      </ul>
    </div>


    <!-- Col #2 -->
    <div class="col footercol3">
      <h4>{{ t('learn.more') }}</h4>
      <ul class="footernav">
        <li><a href="https://www.cobone.com/en/static/howitworks">{{ t('how.it.works')}}</a></li>
        <li><a href="https://www.cobone.com/en/static/faq-ae">{{ t('faq')}}</a></li>
        
        <li><a href="https://www.cobone.com/en/static/forbusiness-ae">{{ t('partner.with')}}</a></li>
      </ul>
    </div>


    <!-- Col #3 -->
    <div class="col footercol3">
  <h4>{{ t('statistics')}}</h4>
  <h5>3,661,551</h5>
  <p>{{ t('total.coupons.sold')}}</p>
  <h6>AED 827,598,655.27</h6>
  <p>{{ t('total.money.saved')}}</p>
</div>

  </div>



  <!-- Payment Icons -->
  <div class="container bottomcontainer">
    <!-- Col #1 -->
    <div class="col footercol3">
      <p>{{ t('cash.on.delivery')}}</p>
      <span class="ico-footer-cc ico-footer-cc-cod"></span>
      <span class="ico-footer-cc ico-footer-cc-payhome"></span>
    </div>
    <div class="col footercol3">
      <p>{{ t('online.accounts')}}</p>
      
      <span class="ico-footer-cc ico-footer-cc-paypal"></span>
      
      <span class="ico-footer-cc ico-footer-cc-onecard"></span>
    </div>
    <div class="col footercol3">
      <p>{{ t('credit.debit.card')}}</p>
      <span class="ico-footer-cc ico-footer-cc-visa"></span>
      <span class="ico-footer-cc ico-footer-cc-mastercard"></span>
      <span class="ico-footer-cc ico-footer-cc-visa-electron"></span>
    </div>
  </div>




  <!-- Cities -->
  <div class="container footersocialcontainer text-center">
  
    <div class="bottomx20">
      {{ t('message.expiration.coupons')}}: <a href="https://s3-eu-west-1.amazonaws.com/cdn.cobone.com/license/DED-NOC-letter-exp-7-Sept-16.jpg" target="_blank">12336431</a> {{ t('valid.until') }} 07/09/2016
    </div>
  
    <div class="footercitylogo">
      <a href="{{ route('home') }}" title="Bee Dealz">
        <img src="{{ asset('bee_dealz_logo.png') }}" alt="Bee Dealz" style="width: 110px; height: 46px;">
      </a>
    </div>
  </div>




  <!-- Social Media -->
  <div class="container footersocialcontainer">
    <p>{{ t('follow.us') }}</p>
    <div class="text-center">
        <a href="http://www.facebook.com/133323990035820" title="Facebook" target="_blank"><span class="ico33 ico33-facebook"></span></a>
        <a href="http://www.instagram.com/cobonedubai" title="Instagram" target="_blank"><span class="ico33 ico33-instagram"></span></a>
        <a href="http://www.twitter.com/cobonedubai" title="Twitter" target="_blank"><span class="ico33 ico33-twitter"></span></a>
        <a href="http://www.pinterest.com/cobone" title="Pinterest" target="_blank"><span class="ico33 ico33-pinterest"></span></a>
        <a href="https://plus.google.com/103770203060970696536" title="Google+" target="_blank"><span class="ico33 ico33-googleplus"></span></a>
    </div>
    
    <small>&copy; {{ date('Y')}} {{ t('copyright') }}</small>
  </div>
</footer>

  

</div>
</div>
 </body>
</html>