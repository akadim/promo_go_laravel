<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">   
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Searching for the best deals in Dubai, Try Bee Dealz">
    <meta property="fb:app_id" content="178733612156994">
    <meta name="layout" content="main">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bee Dealz | @yield('pageTitle') </title>
    <link rel="shortcut icon" href="https://static-cdn.cobone.com/assets/favicon-b07d7371e13c21dc473c8db493a730e8.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="https://static-cdn.cobone.com/assets/apple-touch-icon-f296754f157e7bb565ca77facf1230ba.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://static-cdn.cobone.com/assets/apple-touch-icon-retina-41a8ee9a7c9930346a22e2c8a442d621.png">

    <link href="{{asset('assets/front/css/fonts.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">

    @if(LaravelLocalization::getCurrentLocale() == 'ar')
      <link rel="stylesheet" href="{{asset('assets/front/css/style_rtl.css')}}">
    @endif

    <link rel="stylesheet" href="{{asset('assets/front/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/toastr.css')}}">

    <link rel="canonical" href="https://www.beedealz.com">
    
    <script src="{{asset('assets/front/js/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/jquery.countdown.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/toastr.min.js')}}"></script>
    <script src="{{ asset('assets/front/js/jquery.validate.js') }}"></script>
    <script type="text/javascript">
       $(function(){

           if($(".form-validation")) { 
              jQuery.extend(jQuery.validator.messages,{
                    required: "{{ t('field.required') }}.",
                    remote: "Please fix this field.",
                    email: "{{ t('email.invalid') }}.",
                    url: "{{ t('Invalid URL') }}.",
                    date: "{{ t('Invalid Date') }}.",
                    dateISO: "Please enter a valid date (ISO).",
                    number: "{{ t('Invalid Number') }}.",
                    digits: "{{ t('Only Numbers are required') }}.",
                    creditcard: "Please enter a valid credit card number.",
                    equalTo: "Please enter the same value again.",
                    accept: "{{ t('Please enter a value with a valid extension')}}.",
                    maxlength: $.validator.format("Please enter no more than {0} characters."),
                    minlength: $.validator.format("Please enter at least {0} characters."),
                    rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
                    range: $.validator.format("Please enter a value between {0} and {1}."),
                    max: $.validator.format("Please enter a value less than or equal to {0}."),
                    min: $.validator.format("Please enter a value greater than or equal to {0}.")
                });

              $("#form-register").validate();
              $("#form-login").validate();

           }
       });
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/JavaScript">
      emsSetEnv("www1");
      emsTracking("145648222","beedealz.com");
    </script>

    
  </head>
  <body class="js-DealSearch js-LazyImages js-SwappingBanners ">

  <script>
      megamenu.docinit({
        menuid:'solidmenu',
        dur:300
      })
    </script>


    
    
    

    <div id="wrapper" class="">
      <div id="sidebar-wrapper">
        
<!-- Navbar / Nav Bar -->


    <div id="sidebar-wrapper">
        <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
            
                <li>
                    <a href="https://www.cobone.com/en/deals/dubai/all">
                      <span class="ico-vertical ico-vertical-all"></span>{{ t('all.deals')}}
                    </a>
                </li>
                
                @foreach($current_country->categories as $menu_category) 
                    <li>
                        <a href="javascript:void();" onclick="javascript:toggleMobileMenu('id-{{ $menu_category->title_en }}-{{ $menu_category->id }}');" title="{{ $menu_category->title_en }}">
                            <span class="ico-vertical ico-vertical-food-dining"></span>{{ (LaravelLocalization::getCurrentLocale() == 'en') ? $menu_category->title_en : $menu_category->title_ar }}
                        </a>

                    <!-- Dropdown :: Other Categories -->
                    <ul id="id-{{ $menu_category->title_en }}-{{ $menu_category->id }}">
                        <li>
                            <a href="https://www.cobone.com/en/deals/food-dining-dubai" title="Food">
                                <span class="ico16 ico16-buright"></span>{{ t('all.deals')}}
                            </a>
                        </li>

                        @foreach($menu_category->subcategories as $mobile_subcategory)
                          <li>
                             <a href="https://www.cobone.com/en/deals/food-dining-dubai/filter/food-beverage/5-star" title="5 Star">
                                <span class="ico16 ico16-buright"></span>{{ (LaravelLocalization::getCurrentLocale() == 'en') ? $mobile_subcategory->title_en : $mobile_subcategory->title_ar }}
                            </a> 
                          </li>
                        @endforeach
                        
                        @if($menu_category->travel_locations != null && $menu_category->travel_locations->count() > 0)
                            <li class="subtitle">
                                <span class="ico16 ico16-arrowdown"></span>{{ t('location')}}
                            </li>
                            @foreach($menu_category->travel_locations as $mobile_travel_location)
                               <li>
                                    <a href="https://www.cobone.com/en/deals/destinations-dubai/filter/location/dubai" title="Dubai">
                                        <span class="ico16 ico16-buright"></span>{{ (LaravelLocalization::getCurrentLocale() == 'en') ? $mobile_travel_location->title_en : $mobile_travel_location->title_ar }}
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                 </li>
                @endforeach
                
            
        </ul>
    </div>


<script>
    function toggleMobileMenu (id) {
        var varID = '#'+id;
        jQuery(varID).slideToggle(200);
    }
</script>

      </div>
      <div id="page-content-wrapper">
        




<!-- Topbar / Top Bar -->
<div class="row topbar">

  <div class="col topbarcolleft">
    <!-- Nav -->
    <a href="javascript:void(0);" id="menu-toggle">
      <span class="ico24 ico24-menu"></span>
    </a>
    <script>
      $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
      });
    </script>
  </div>

  <div class="col topbarcolright">
    <!-- Sites -->
    <div class="dropdown">
      <div class="dropdown">
      
  <div class="dropdown">
    <a href="" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <span class="ico24 ico24-arrowdown hidemobile"></span>
      <span class="topbarhead">{{ (LaravelLocalization::getCurrentLocale() == 'en') ? $current_country->title_en : $current_country->title_ar }}</span>
      <span class="ico24 ico24-globe"></span>
    </a>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

       @foreach($countries as $country)
        <li>
            <a href="{{ route('country.deals', ['country_name' => $country->title_en]) }}">
              <img src="{{ asset('assets/images/flag'.$country->image_path) }}" alt="{{ $country->title_en }}"> {{ (LaravelLocalization::getCurrentLocale() == 'en') ? $country->title_en : $country->title_ar }}
            </a>
        </li>
       @endforeach

    </ul>
  </div>


      </div>
    </div>

    <a href="https://www.cobone.com/ar/deals/eid-deals/filter/eid/getaways">
      <span class="ico24 ico24-arrowdown hidemobile"></span>
      <span class="topbarhead">{{ (LaravelLocalization::getCurrentLocale() == 'en') ? 'العربية' : 'English' }}</span>
    </a>
    <!-- Language -->


    <!-- Search -->
    <a href="#idSearchBar" data-toggle="collapse">
      <span class="ico24 ico24-search"></span>
    </a>
  </div>
</div>




<!-- Searchbar / Search Bar -->
<div class="row topbarsearch collapse" id="idSearchBar">
  <div class="topbarsearchbar">
    <form action="/en/deals/dubai/search" method="get" class="deal-search-form">
      <div class="col topbarsearchbarcolleft">
        <input name="query" class="subnavsearchtxt form-control lowercase required" placeholder="{{ t('search.field') }}" required="required" type="text">
      </div>
      <div class="col topbarsearchbarcolright">
        <input class="topbarsearchbarbtn" value="Search" type="submit">
      </div>
    </form>
  </div>
</div>






<!-- Header -->
<header>
  <script type="text/javascript" src="{{ asset('assets/front/js/ouibounce.js') }}"></script>
  <div class="container">
    <div class="logorow">
      <div class="col logocolleft">
        <a href="{{ url('/deals/'.$current_country->title_en) }}" title="Cobone">
          <img src="{{ asset('bee_dealz_logo.png') }}" alt="Bee Dealz" style="width: 196px; height: 64px;">
        </a>
      </div>
      <div class="col logocolright">
          
          <div id="top-banner">
               {{--*/ $banner_index = 0 /*--}}
               {{--*/ $banner_company_addresses = [] /*--}}
               @foreach($banner_company_addresses as $banner_company_address)
                  {{---*/ $banner_index += 1 /*---}}
                  <a wait_time="3" href="" class="{{ ($banner_index == 1) ? 'active' : '' }}">
                    <img src="{{ asset('images/companies/'.$banner_company_address->image_banner_path) }}" alt="BeeDealz" class="top-banner-image">
                  </a>
               @endforeach
            
          </div>

      </div>
    </div>
  </div>
  <div class="container">
    <div class="subnavrow">

      <div class="col subnavcolhome">
        <a href="{{ url('/deals/'.$current_country->title_en) }}" title=""><span class="ico16 ico16-home"></span></a>
      </div>

      <!-- City -->
      <div class="col subnavcolcity">
        
          <a href="#" class="dropdown-toggle topnavtxt" data-toggle="dropdown" role="button" aria-expanded="false">
            <span class="sp-flag-dubai"></span>{{ (LaravelLocalization::getCurrentLocale() == 'en') ? $current_country->title_en : $current_country->title_ar }}<span class="ico16 ico16-dropdown"></span>
          </a>
          <ul class="dropdown-menu" role="menu">

              @foreach($countries as $country)
                <li>
                    <a href="{{ route('change.country', ['country_id' => $country->id]) }}">
                      <img src="{{ asset('/images/flags/'.$country->image_path) }}" alt="{{ $country->title_en }}" style="width: 20px; height: 17px;">&nbsp;{{ (LaravelLocalization::getCurrentLocale() == 'en') ? $country->title_en : $country->title_ar }}
                    </a>
                </li>
               @endforeach
            
          </ul>


      </div>

      <!-- Language -->
      <div class="col subnavcollanguage">
        <a rel="alternate" hreflang="{{ (LaravelLocalization::getCurrentLocale() == 'en') ? 'ar' : 'en' }}" href="{{LaravelLocalization::getLocalizedURL((LaravelLocalization::getCurrentLocale() == 'en') ? 'ar' : 'en') }}">{{ (LaravelLocalization::getCurrentLocale() == 'en') ? 'العربية' : 'English' }}<span class="ico16 ico16-dropdown"></span></a>
      </div>

      <!-- Search -->
      <div class="col subnavcolsearch subnavsearchbox">
        <form action="/en/deals/dubai/search" method="get" class="deal-search-form">
          <input name="query" class="subnavsearchtxt lowercase" placeholder="{{ t('search.field') }}" required="required" type="text">
        </form>
      </div>

      <!-- Logo -->
      <div class="col subnavcollogo">
        <a href="https://www.cobone.com/en/deals/dubai" title="Cobone">
          <img src="Value%20Eid%20Deals%20for%20hotels,%20spa%20+%20food%20&amp;%20dining%20%7C%20Cobone_files/logo-en-d3f494b0ebb664033579a79ee1844313.png" alt="Cobone">
        </a>
      </div>

      <!-- Login/Register -->
      <div class="col subnavcollogin">
        
          <!-- Before Login -->
          <span class="ico16 ico16-user"></span>
          @if(auth()->guard('clients')->user())
          <a href="{{ url('/logout') }}" class="topnavtxt">
             {{ t('logout')}}
          </a>
          @else
          <a href="{{ url('/login') }}" class="topnavtxt">
             {{ t('login')}}
          </a>
          @endif
          <span class="subnavsep">|</span>

          @if(auth()->guard('clients')->user())
          <span class="subnavmenuholder">
            <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle topnavtxt rightx8" href="#">
              {{ t('my.account')}}
              <span class="ico16 ico16-dropdown"></span>
            </a>
            <ul role="menu" class="dropdown-menu">
                <li><a href="{{ route('client.orders') }}"><span aria-hidden="true" class="ico16 ico16-order"></span>{{ t('my.orders')}}</a></li>
                <li><a href="/account/credit"><span aria-hidden="true" class="ico16 ico16-credit"></span>{{ t('my.credits')}}</a></li>
                <li><a href="{{ route('client.account') }}"><span aria-hidden="true" class="ico16 ico16-setting"></span>{{ t('my.details')}}</a></li>
            </ul>
          </span>
          @else
          <a href="{{ url('/register') }}" class="topnavtxt">
            {{ t('register')}}
          </a>
          @endif
        
        
        <span class="subnavsep">|</span>
        <a href="#" id="openCart"><span class="ico16 ico16-cart"></span></a>
      </div>

    </div>
  </div>
  <div class="container-fluid topshad"></div>
  

    <!-- Ouibounce Modal -->

    <link rel="stylesheet" href="css/overlay.css">
    <!-- Overlay #Starts -->
<div style="display: none;" id="ouibounce-modal">
  <div class="underlay"></div>
  <div class="modal overlayholder overlaycontainer">
      

    <!-- Overlay ### Head -->
    <div class="overlayrow">
      <div class="col overlayrowcolico">
        <img src="Value%20Eid%20Deals%20for%20hotels,%20spa%20+%20food%20&amp;%20dining%20%7C%20Cobone_files/overlay-ico-c8988c9cc3f3856e3584da6f5985a3bb.png" alt="Cobone">
      </div>
      <div class="col overlayrowcoltitle">
        <img src="Value%20Eid%20Deals%20for%20hotels,%20spa%20+%20food%20&amp;%20dining%20%7C%20Cobone_files/logo-en-d3f494b0ebb664033579a79ee1844313.png" alt="Cobone">
        <h2>Don't miss a Deal!</h2>
      </div>
      <div class="col overlayrowcolhide">
        <a href="javascript:void(0)" onclick="$('#ouibounce-modal').hide();">
          <img src="Value%20Eid%20Deals%20for%20hotels,%20spa%20+%20food%20&amp;%20dining%20%7C%20Cobone_files/overlay_hide.png" alt="Close">
        </a>
      </div>
    </div>


    <!-- Overlay ### Body -->
    <div id="overlay_row_container">
      <div class="overlayrow">
        <div class="overlaybody">
          <div class="overlay_row overlay_textcenter">
            <div class="overlayh3">
              Free Signup for the best Daily Deals in your City!
            </div>
          </div>

          <div class="overlayfbconnect">
            <form action="/account/facebook" method="post" id="fbForm">
  <div class="registerfbpanel">
    <div class="registerfb">
      <fb:login-button fb-iframe-plugin-query="app_id=178733612156994&amp;container_width=0&amp;locale=en_US&amp;login_text=%0A%20%20%20%20%20%20%20%20Connect%20with%20Facebook%0A%20%20%20%20%20%20&amp;scope=public_profile%2Cemail&amp;sdk=joey&amp;size=large" fb-xfbml-state="rendered" class=" fb_iframe_widget" login_text="
        Connect with Facebook
      " size="large" scope="public_profile,email" onlogin="Cobone.facebook.checkLoginState();"><span style="vertical-align: bottom; width: 0px; height: 0px;"><iframe class="" src="Value%20Eid%20Deals%20for%20hotels,%20spa%20+%20food%20&amp;%20dining%20%7C%20Cobone_files/login_button.html" style="border: medium none; visibility: visible; width: 0px; height: 0px;" title="fb:login_button Facebook Social Plugin" scrolling="no" allowfullscreen="true" allowtransparency="true" name="fd711e612f8baa" frameborder="0" height="1000px" width="1000px"></iframe></span></fb:login-button>
      <input name="accessToken" id="accessToken" type="hidden">
    </div>
    <div class="registerlabel">
      
      
    </div>
  </div>
</form>

          </div>

          <div class="overlayor">
            <div class="col overlayorcolline"></div>
            <div class="col overlayorcoltitle">
              OR
            </div>
            <div class="col overlayorcolline"></div>
          </div>
          
          <form novalidate="novalidate" action="/subscription/processWelcome/exitOverlayForm" method="post" name="exitOverlayForm" id="exitOverlayForm">
            <div class="overlayformrow">
              <div class="col overlayformcolleft">
                <input aria-required="true" class="form-control required email" name="email" onclick="if(this.value=='Email:'){this.value=''; }" onfocus="this.select()" onblur="if(this.value==''){this.value='Email:';}" value="Email:" type="text">
              </div>
              <div class="col overlayformcolright">
                  <select name="city" class="form-control" title="City" id="city">
                    
                        
                            <option value="dubai" selected="selected">Dubai</option>
                        
                    
                        
                            <option value="abudhabi">Abu Dhabi</option>
                        
                    
                        
                            <option value="jeddah">Jeddah</option>
                        
                    
                        
                            <option value="riyadh">Riyadh </option>
                        
                    
                        
                            <option value="dammam">Dammam</option>
                        
                    
                  </select>
              </div>
            </div>
            <div class="overlayformrow">
              <div class="col overlayformcolleft">
                <div class="overlayregisterlabel">
                  Already registered?
                  <a href="https://www.cobone.com/account/login" onclick="_gaq.push(['_trackEvent', 'welcome-overlay', 'click', 'login']);">
                    Sign in
                  </a>
                </div>
              </div>
              <div class="col overlayformcolright">
                <input value="Sign Up" class="overlaybtngreen" id="welcome-submit-button" onclick="_gaq.push(['_trackEvent', 'welcome-overlay', 'click', 'subscribe', 'Sign Up']);" data-submitbutton="true" name="discover" type="submit">
                <img src="Value%20Eid%20Deals%20for%20hotels,%20spa%20+%20food%20&amp;%20dining%20%7C%20Cobone_files/spinner-c7b3cbb3ec8249a7121b722cdd76b870.gif" alt="Loading..." class="leftx5 hide exit-overlay-spinner">
              </div>
            </div>
          </form>

          <div id="exitOverlayForm-error" class="overlayerror" role="alert" style="display:none;"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Overlay #End -->


    <script>
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
            aggressive: false //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script>
  
</header>




  <nav class="navbar navbar-inverse container">
    <div class="container-fluid navbarholder">
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav" id="solidmenu">
          
          
          
            
            {{--*/ $index = 0 /*--}}
             @foreach($current_country->categories as $menu_category) 
              {{--*/ $index = $index + 1 /*--}}
              <li style="width:{{ ($index == sizeof($current_country->categories)) ? '13.7%' : '12.3%'  }};" class="dropdown">
                <a class="" href="{{ route('country.category.deals', ['country_name' => $current_country->title_en, 'category_id' => $menu_category->id ]) }}" title="{{ $menu_category->title_en }}" rel="{{ $menu_category->title_en.'-'.$menu_category->id }}">
                  <img src="{{ asset('images/categories/icons/'.$menu_category->icon_path) }}" alt="" style="width: 30px; height: 30px;">
                  {{ (LaravelLocalization::getCurrentLocale() == 'en') ? $menu_category->title_en : $menu_category->title_ar }}
                </a>

                

                <!-- Code removed from here -->
              </li>
            @endforeach
            
          
        </ul>
      </div>
    </div>
  </nav>

  @yield('body_content')


<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel" xmlns="http://www.w3.org/1999/html">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modalrow">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <div class="modaltitle" id="cartModalLabel"><span class="ico16 ico16-basket"></span>Your Shopping Cart</div>
        </div>
      </div>
      <div id="cart_section">
        <form method="get" action="{{ url('/checkout') }}">
          <div class="modal-body">
            <div id="cart-items">
              {{--*/ $cart = (LaravelLocalization::getCurrentLocale() == 'en') ? ( isset($_SESSION['cart_'.$current_country->id.'_en']) ? $_SESSION['cart_'.$current_country->id.'_en'] : null) : (isset($_SESSION['cart_'.$current_country->id.'_ar']) ? $_SESSION['cart_'.$current_country->id.'_ar'] : null) /*--}}
              {{--*/ $cart_session = (LaravelLocalization::getCurrentLocale() == 'en') ? 'cart_'.$current_country->id.'_en' : 'cart_'.$current_country->id.'_ar' /*--}}
              {{--*/ $cart_total = 0.0 /*--}}
              @if($cart != null)
              @foreach($cart as $cart_item)
                 <div class="modalcartrow" id="row-{{ $cart_item['type'].'-'.$cart_item['id'] }}">
                  <div class="modalcartcol1">{{ $cart_item['title'] }}</div>
                  <div class="modalcartcol2">{{ $cart_item['currency'] }} {{ number_format($cart_item['price'], 2, '.', ' ') }}</div>
                  <div class="modalcartcol3">
                    <a class="remove-from-cart" data-id="{{ $cart_item['type'].'-'.$cart_item['id'] }}" href="{{ route('delete.from.cart', ['cart_id' => ($cart_item['type'].'-'.$cart_item['id']), 'cart_session' => $cart_session  ]) }}">
                      <span class="ico16 ico16-trash"></span>
                    </a>
                  </div>
                </div>
                {{--*/ $cart_total = $cart_total + floatval($cart_item['price']) /*--}}
              @endforeach
              @else
                 {{ t('empty.cart') }}
              @endif
            </div>
            <div class="modalrow">
              <div class="text-right">
                <span class="modeltotallabel">{{ t('cart.total')}}</span> 
                <span class="modeltotalprice leftx9 rightx6" id="stotal">{{ $current_country->currency }} 
                   <span id="sub_total">{{ number_format($cart_total, 2, '.', ' ')  }}</span>
                </span>
              </div>
              <div class="modalnotes">
                <p>{{ t('Please note the following:')}}</p>
                <ul>
                  <li>{{ t('* Deal might expire before checkout and no longer be available')}}</li>
                  <li>{{ t('* Deal price may change due to market fluctuations')}}</li>
                  <li>{{ t('* Final checkout price may vary depending on your chosen payment method and available store credit')}}</li>
                </ul>
              </div>
              <div class="modalbottom text-right">
                <a class="modelcontinueshopping rightx6" data-dismiss="modal" href="#">{{ t('continue.shopping')}}</a>
                <input type="hidden" name="cart_session" value="{{ $cart_session }}" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if($cart)
                  <button class="btn btn-primary checkout-btn" type="submit">{{ t('checkout') }}</button>
                @endif
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

        
        

<!-- Banner & Contact Us for Mobile -->
<div class="footercontact"></div>
<script>
  /*****  Banner & Contact Box shifted here for Mobile Only  *****/
  $( document ).ready(function() {
    // Mobile & Tablet only
    if ($(window).width()<700) {
      var varBannerText = $('.spotlightcolright').html();
      $('.spotlightcolright').html('');
      $('.footercontact').html(varBannerText);
    }
  });
</script>







<!-- Footer -->
<footer>


  <!-- Navigation -->
  <div class="holder footerholder">

    <!-- Col #1 -->
    <div class="col footercol3">
      <h4>{{ t('company')}}</h4>
      <ul class="footernav">
        <li><a href="{{ route('about-us') }}">{{ t('about.us')}}</a></li>
        <li><a href="{{ route('contact-us') }}">{{ t('contact.us')}}</a></li>
        <li><a href="https://www.cobone.com/account/register">{{ t('join.our.site')}}</a></li>
        <li><a href="{{ route('terms-conditions') }}">{{ t('terms.conditions')}}</a></li>
        <li><a href="{{ route('privacy-policy') }}">{{ t('privacy.policy')}}</a></li>
      </ul>
    </div>

<!--
    <div class="col footercol3">
      <h4>{{ t('learn.more') }}</h4>
      <ul class="footernav">
        <li><a href="https://www.cobone.com/en/static/howitworks">{{ t('how.it.works')}}</a></li>
        <li><a href="https://www.cobone.com/en/static/faq-ae">{{ t('faq')}}</a></li>
        
        <li><a href="https://www.cobone.com/en/static/forbusiness-ae">{{ t('partner.with')}}</a></li>
      </ul>
    </div>
-->

    <!-- Col #3 -->
    <div class="col footercol3">
  <h4>{{ t('statistics')}}</h4>
  <h5>{{ $number_orderLines }}</h5>
  <p>{{ t('total.coupons.sold')}}</p>
  <h6>{{ $current_country->currency }} {{ number_format($total_bee_deals, 2, '.', ' ') }}</h6>
  <p>{{ t('total.money.saved')}}</p>
</div>

  </div>



  <!-- Payment Icons -->
  <div class="container bottomcontainer">
    <!-- Col #1 -->
    <div class="col footercol3">
      <p>{{ t('cash.on.delivery')}}</p>
      <span class="ico-footer-cc ico-footer-cc-cod"></span>
      <span class="ico-footer-cc ico-footer-cc-payhome"></span>
    </div>
    <div class="col footercol3">
      <p>{{ t('online.accounts')}}</p>
      
      <span class="ico-footer-cc ico-footer-cc-paypal"></span>
      
      <span class="ico-footer-cc ico-footer-cc-onecard"></span>
    </div>
    <div class="col footercol3">
      <p>{{ t('credit.debit.card')}}</p>
      <span class="ico-footer-cc ico-footer-cc-visa"></span>
      <span class="ico-footer-cc ico-footer-cc-mastercard"></span>
      <span class="ico-footer-cc ico-footer-cc-visa-electron"></span>
    </div>
  </div>




  <!-- Cities -->
  <div class="container footersocialcontainer text-center">
  
    <div class="bottomx20">
      {{ t('message.expiration.coupons')}}: <a href="https://s3-eu-west-1.amazonaws.com/cdn.cobone.com/license/DED-NOC-letter-exp-7-Sept-16.jpg" target="_blank">12336431</a> {{ t('valid.until') }} 07/09/2016
    </div>
  
    <div class="footercitylogo">
      <a href="{{ url('/deals/'.$current_country->title_en) }}" title="Bee Dealz">
        <img src="{{ asset('bee_dealz_logo.png') }}" alt="Bee Dealz" style="width: 110px; height: 46px;">
      </a>
    </div>
    <div class="footercityholder">
      <ul>
        @foreach($countries as $country)
          
          <li>
            <a href="{{ route('change.country', ['country_id' => $country->id]) }}">
              <img src="{{ asset('/images/flags/'.$country->image_path) }}" alt="{{ $country->title_en }}" style="width: 30px; height: 20px;"> {{ (LaravelLocalization::getCurrentLocale() == 'en') ? $country->title_en : $country->title_ar }}
            </a>
          </li>

        @endforeach
      
      </ul>
    </div>
  </div>




  <!-- Social Media -->
  <div class="container footersocialcontainer">
    <p>{{ t('follow.us') }}</p>
    <div class="text-center">
        <a href="http://www.facebook.com/{{ $current_country->facebook_link }}" title="Facebook" target="_blank"><span class="ico33 ico33-facebook"></span></a>
        <a href="http://www.instagram.com/{{ $current_country->instagram_link }}" title="Instagram" target="_blank"><span class="ico33 ico33-instagram"></span></a>
        <a href="http://www.twitter.com/{{ $current_country->twitter_link }}" title="Twitter" target="_blank"><span class="ico33 ico33-twitter"></span></a>
        <a href="http://www.pinterest.com/{{ $current_country->pinterest_link }}" title="Pinterest" target="_blank"><span class="ico33 ico33-pinterest"></span></a>
        <a href="https://plus.google.com/{{ $current_country->google_plus_link }}" title="Google+" target="_blank"><span class="ico33 ico33-googleplus"></span></a>
    </div>
    
    <small>&copy; {{ date('Y')}} {{ t('copyright') }}</small>
  </div>
</footer>

  

</div>
</div>
 @foreach($current_country->categories as $menu_category) 
  <div class="megawrapper" style="z-index: 115; position: absolute; top: 192px; left: 420.5px; visibility: hidden; width: 582px; height: 471px;">
           <div style="position:absolute;overflow:hidden;left:0;top:0;width:100%;height:100%;">
             <div style="visibility: inherit; top: -446px;" id="{{ $menu_category->title_en.'-'.$menu_category->id }}" class="mega bgwhite">
                <div class="meganavholder">
                  <div class="meganavcontainer">
                    
                      <div class="col meganavcolcat">
                        <h3>{{ $menu_category->title_en }}</h3>
                        <ul class="meganavlist">
                          @foreach($menu_category->subcategories as $menu_subcategory)
                            <li><a href="https://www.cobone.com/en/deals/food-dining-dubai/filter/food-beverage/5-star" title="{{ $menu_subcategory->title_en }}">{{ (LaravelLocalization::getCurrentLocale() == 'en') ? $menu_subcategory->title_en : $menu_subcategory->title_ar }}</a></li>
                          @endforeach
                        </ul>
                      </div>
                    
                    <div class="col meganavcoldeal">
                      {{--*/ $deal_for_show = null /*--}}
                      @foreach($menu_category->deals as $cat_deal)
                          {{--*/ $deal_for_show = $cat_deal /*--}}
                      @endforeach
                      <h4>
                        <a href="{{ ($deal_for_show != null) ?  url('/deals/detail/'.$deal_for_show->id) : '' }}" title="{{ ($deal_for_show != null) ?  $deal_for_show->title_en : ''}}">
                          {{ ($deal_for_show != null) ? ( (LaravelLocalization::getCurrentLocale() == 'en') ?  $deal_for_show->title_en : $deal_for_show->title_ar ) : '' }}
                        </a>
                      </h4>

                      <div class="meganavimg">
                        <a title="{{ ($deal_for_show != null) ? $deal_for_show->title_en : '' }}" href="{{ ($deal_for_show != null) ?  url('/deals/detail/'.$deal_for_show->id) : '' }}">
                          <img alt="" src="{{ asset('images/deals/'.(($deal_for_show != null) ? $deal_for_show->image_path : '') ) }}">
                        </a>
                      </div>

                      <div class="col meganavcolprice">
                        <div class="meganavcurrency">{{ ($deal_for_show != null) ? $deal_for_show->category->country->currency : '' }}</div>
                        <div class="meganavprice">{{ ($deal_for_show != null) ? ceil(( floatval($deal_for_show->price) - ( floatval($deal_for_show->price) * ( intval($deal_for_show->discount)/100 ) ) )) : '' }}</div>
                        <div class="meganavpriceold">{{ ($deal_for_show != null) ? $deal_for_show->price : '' }}</div>
                      </div>
                      
                      <div class="col meganavcolbtn">
                        <a href="{{ ($deal_for_show != null) ?  url('/deals/detail/'.$deal_for_show->id) : '' }}" class="btn btn-primary">{{ t('view.deal')}}</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <a href="{{ route('country.category.deals', ['country_name' => $current_country->title_en, 'category_id' => $menu_category->id ]) }}" title="{{ $menu_category->title_en }}" class="btn btn-primary rightx2 topx15">
                    {{ t('all.deals') }}
                  </a>
                </div>
              </div>
            </div>
          </div>
@endforeach
<script type="text/javascript">
    $(function(){
        $(document).on('click', ".remove-from-cart", function(evt){
               evt.preventDefault();
               var remove_url = $(this).attr('href');
               var data_id = $(this).attr('data-id');
               $.get(remove_url, function(response){
                    $("#row-"+response.success).fadeOut();
                    var total_price = parseFloat(response.total_price);
                    $("#sub_total").html(total_price.toFixed(2));
               });
        });
    });
</script>
 </body>
</html>