<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

    public function catcher_status() {
        return $this->hasMany('App\CatcherStatus');
    }

    public function coins_purchases() {
        return $this->hasMany('App\CoinsPurchase');
    }

    public function catcher_gadgets() {
        return $this->hasMany('App\CatcherGadget');
    }

    public function deal_positions() {
        return $this->hasMany('App\DealPosition');
    }

}
