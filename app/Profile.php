<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function users() {
    	return $this->hasMany('App\User');
    }

    public function permissions() {
    	return $this->belongsToMany('App\Permission');
    }
}
