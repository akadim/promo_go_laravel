<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    /**
     * The attributes that are mass assignable.,
     *
     * @var array
     */
    protected $fillable = [
        'discount', 'description', 'number_of_deals', 'duration', 'duration_to_goal', 'is_condition', 'discount_code'
    ];

    public function company()  {
        return $this->belongsTo('App\Company');
    }

    public function deal_positions() {
        return $this->hasMany('App\DealPosition');
    }

}
