<?php
namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryUser;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

use Mail;

class UserController extends Controller {

    public $breadcrumbs = [];
    

	public function profile(Request $request){

		 $breadcrumbs = [
		      [
		          'title' => 'Dashboard',
			        'url' => ''.route('admin.dashboard'),
			        'active' => false
		      ],
		      [
		          'title' => 'Profile',
			        'url' => ''.route('admin.profile'),
			        'active' => true
		      ]
		 ];

		 if($request->isMethod('post')) {
              
              $user = Auth::user();
              $user->fullname = $request->fullname;
              $user->email = $request->email;

              $user->save();

              $request->session()->flash('status', 'Profile successfully updated');

              return redirect()->route('admin.profile');

		 }

		 return view('admin.users.profile', compact(['breadcrumbs']));
	}

	public function password(Request $request) {

		if($request->isMethod('post')) {

			  $user = Auth::user();

			  if (!Hash::check($request->old_password, $user->password)) {
				     $request->session()->flash('fail', 'Current Password incorrect');
				     return redirect()->route('admin.profile');
			  }
				              
              $user->password = bcrypt($request->new_password);

              $user->save();

              $request->session()->flash('password_status', 'Password successfully changed');

              return redirect()->route('admin.profile');

		}
		return redirect()->route('admin.profile');
	}

  public function login(Request $request) {
      
      if ($request->isMethod('post')) {

          //die(bcrypt('123'));
          if(Auth::attempt(['email' => $request->email, 'password' => $request->password ])) {
               return redirect($request->route()->getPrefix().'/');
          }
          else {
            return redirect($request->route()->getPrefix().'/login')->with(['fail' => 'Email or password are invalid']);
          }
      }

      return view('admin.users.login');
  }

  public function logout(Request $request) {

  	    Auth::logout();
  	    return redirect($request->route()->getPrefix().'/login');
  }

  public function index(){

		 $breadcrumbs = [
		      [
		          'title' => t('Dashboard'),
			      'url' => ''.route('admin.dashboard'),
			      'active' => false
		      ],
		      [
		          'title' => t('Users'),
			      'url' => ''.route('admin.users'),
			      'active' => true
		      ]
		 ];

		 $users = User::where('role', '!=', 'super_admin')->with('brand')->get();

		 return view('admin.users.index', compact(['users', 'breadcrumbs']));
	}

	public function edit($id = null) {

		if($id == -1) {
			$user = new User;
			return json_encode($user);
		}

		return User::where('id', '=', $id)->first();
	}
    
    public function store(Request $request) {

        $user = new User();

        if($request->id != '' || $request->id != null) {
             $user = User::where('id', '=', $request->id)->first();
        }

        //die($request->title);

        $user->fullname = $request->fullname;
        $user->email = $request->email;
        $user->role = $request->role;

        if($request->id != '' || $request->id != null) {
             $user->password = $request->password;
        }

        $user->save();

        $request->session()->flash('status', t('User Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect($request->route()->getPrefix().'/users');
    }

    public function delete(Request $request, $id = null)  {
         
         $user = User::where('id', '=', $id)->first();

         $user->delete();

         $request->session()->flash('status', t('User Successfully Deleted') );

         return redirect($request->route()->getPrefix().'/users');
    }

    private function createRandomPassword() { 
    
        $random_number = "";

        for($i=1; $i <= 13; $i++) {
            $random_number = $random_number."".rand(0, 9);
        }

        return intval($random_number); 

   } 

    public function reset_password(Request $request) {
         
         if($request->isMethod('post')) {
             
             $user_count = User::where('email', '=', $request->email)->count();

             if($user_count <= 0) {
                $request->session()->flash('fail', 'The user with this email is not found, try again!');
                return redirect()->route('admin.reset.password');
             }
             else {
                
                $user = User::where('email', '=', $request->email)->first();

                $random_password = str_random(8);

                $user->password = bcrypt( $random_password );

                $user->save();

                Mail::send('emails.reset_password', ['user' => $user, 'random_password' => $random_password], function ($m) use ($user) {
                    $m->from('noreply@promo-catch.com');
                    $m->to($user->email, $user->fullname)->subject('PROMOCATCH: RESET PASSWORD');
                });

                $request->session()->flash('success', 'Your password has been successfully changed!');

             }
         }

         return view('admin.users.reset_password');
    }


}