<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Profile;
use App\Company;
use App\CompanyAssignment;
use App\Http\Controllers\Controller;
use Mail;

use Illuminate\Http\Request;

class CompaniesManagersController extends Controller {

    public $breadcrumbs = [];
    

	public function index(){

		 $breadcrumbs = [
		      [
		        'title' => 'Dashboard',
			      'url' => ''.route('admin.dashboard'),
			      'active' => false
		      ],
		      [
		          'title' => 'Companies Managers',
			        'url' => ''.route('admin.companies.managers'),
			        'active' => true
		      ]
		 ];

         $company_assignments = CompanyAssignment::all();

         $companies_managers = [];

         foreach($company_assignments as $company_assignment) {
               $user = User::find($company_assignment->user_id);
               $company = Company::find($company_assignment->company_id);
               if(!$user->deleted) {
                  $companies_managers[] = ['company_manager' => $user, 'company' => $company];
               }
         }

         /*

		 $companies_managers = User::with('profile')->where('deleted', '=', 0)->whereHas('profile', function($query) {
              return $query->where('title', '=', 'Company Manager');
         })->get();

         */

         $companies = ['' => ' ---- Choose a Company ---- '] + Company::where('deleted', '=', 0)->lists('title', 'id')->all();

		 return view('admin.companies.managers.index', compact([ 'companies_managers','companies', 'breadcrumbs']));
	}

	public function edit($id = null) {
        

        $user = User::where('deleted', '=', 0)->where('id', '=', $id)->with('profile')->first();
        $company_id = 0;
        $company_name = '';

        if(intval($id) == -1) {
            $user = new User;
        }



        $company_assignment_count = CompanyAssignment::where('user_id', '=', $user->id)->count();

        if($company_assignment_count > 0) {
            $company_assignment = CompanyAssignment::where('user_id', '=', $user->id)->first();
            $company_id = $company_assignment->company_id;
            $company = Company::find($company_id);
            $company_name = $company->title;
        }

        return response()->json([
                   'company_manager' => $user,
                   'company_id' => $company_id,
                   'company_name' => $company_name
            ]);
	}
    
    public function store(Request $request) {
         

        $company_manager = new User();
        $company_manager_profile = Profile::where('title', '=', 'Company Manager')->first();

        if($request->id != '' || $request->id != null) {
             $company_manager = User::where('id', '=', $request->id)->first();
        }

        //die($request->title);

        $company_manager->fullname = $request->fullname;
        $company_manager->phone = $request->phone;
        $company_manager->email = $request->email;
        $company_manager->profile_id = $company_manager_profile->id;

        if(isset($request->id) && (is_null($request->id) || $request->id == '')) {
             $company_manager->password = bcrypt($request->password);
        }
        
        $company_manager->id = $request->id;
        $company_manager->active = 1;
        $company_manager->deleted = 0;

        $company_manager->save();

        $company_assignment_count = CompanyAssignment::where('user_id', '=', $company_manager->id)->count();

        $company_assignment = CompanyAssignment::where('user_id', '=', $company_manager->id)->first();

        if($company_assignment_count <= 0) {
             $company_assignment = new CompanyAssignment;
        }
       
       $company_assignment->user_id = $company_manager->id;
       $company_assignment->company_id = $request->company_id;
       $company_assignment->save();

        $request->session()->flash('status', t('Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect($request->route()->getPrefix().'/companies-managers');
    }

    public function delete(Request $request, $id = null)  {
         
         $company_manager = User::where('id', '=', $id)->first();
                 
         /*
         if(file_exists(base_path() . '/public/images/companies/'.$company->image_path)) {
           unlink(base_path() . '/public/images/companies/'.$company->image_path);
         }
         */
         
         $company_manager->deleted = 1;

         $company_manager->save();

         $request->session()->flash('status', t('Successfully Deleted') );
         $request->session()->flash('cancel_status', $id );

         return redirect($request->route()->getPrefix().'/companies-managers');
    }

    public function undo_delete(Request $request, $id=null) {

         $company = User::where('id', '=', $id)->first();
         
         $company->deleted = 0;

         $company->save();

         $request->session()->flash('status', 'Successfully Restored' );

         return redirect($request->route()->getPrefix().'/companies-managers');
    }

    public function change_password(Request $request) {
        if($request->isMethod('post')) {
            $company_manager = User::find($request->manager_id);
            $company_manager->password = bcrypt($request->new_password);
            $company_manager->save();

            $request->session()->flash('status', 'Password Successfully Changed' );

            return redirect($request->route()->getPrefix().'/companies-managers');
        }
    }
	
    public function verify_email(Request $request) {

        $email = $request->email;
        
        $company_managers_count = User::where('email', '=', $email)->count();

        $response = ($company_managers_count > 0) ? 'Exist' : 'Not Exist';

        return response()->json(['response' => $response]);
    }

    public function activate_account(Request $request) {

          $company_manager = User::find($request->id);
          $company_manager->active = $request->show_active;
          $random_password = str_random(8);
          $company_manager->password = bcrypt($random_password);
          $company_manager->save();

          if($company_manager->active == 1) {
              Mail::send('emails.company_manager_activated', ['user' => $company_manager, 'random_password' => $random_password], function ($m) use ($company_manager) {
                   $m->from('noreply@promo-catch.com');
                   $m->to($company_manager->email, $company_manager->fullname)->subject('PROMOCATCH: Your Account is Active');
              });
          }

          return redirect($request->route()->getPrefix().'/companies-managers');
    }

}