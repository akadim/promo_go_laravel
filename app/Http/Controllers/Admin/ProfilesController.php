<?php
namespace App\Http\Controllers\Admin;

use App\Profile;
use App\Permission;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class ProfilesController extends Controller {

    public $breadcrumbs = [];
    

	public function index(){

		 $breadcrumbs = [
		      [
		        'title' => 'Dashboard',
			      'url' => ''.route('admin.dashboard'),
			      'active' => false
		      ],
		      [
		        'title' => 'Profiles',
			      'url' => ''.route('admin.profiles'),
			      'active' => true
		      ]
		 ];



		 $profiles = Profile::where('deleted', '=', 0)->get();

		 return view('admin.profiles.index', compact(['profiles', 'breadcrumbs']));
	}

	public function edit($id = null) {
            

            $breadcrumbs = [
              [
                  'title' => 'Dashboard',
                  'url' => ''.route('admin.dashboard'),
                  'active' => false
              ],
              [
                  'title' => 'Profiles',
                  'url' => ''.route('admin.profiles'),
                  'active' => false
              ],
              [
                  'title' => 'EDIT PROFILE',
                  'url' => ''.route('admin.profiles.edit', ['id' => $id]),
                  'active' => false
              ]
          ];

        $profile = new Profile;

        $permissions_array = [];

        $parent_ids = [];

        $permissions = Permission::all();

        foreach($permissions as $current_permission) {
          
          if(!is_null($current_permission->parent_id) && !in_array($current_permission->parent_id, $parent_ids)) {
                $parent_ids[] = $current_permission->parent_id;
          }
          
          if(is_null($current_permission->parent_id) && $current_permission->path_link != null) {
                $parent_ids[] = $current_permission->id;
          }
       }

        if(!empty($parent_ids)) {
            foreach($parent_ids as $parent_id) {
                $permit = Permission::where('id', '=', $parent_id)->first();
                $permit = ['id' => $permit->id, 'title' => $permit->title];
                $child_permissions = Permission::where('parent_id', '=', $parent_id)->get();
                $child_permissions = $child_permissions->toArray();
                $child_permissions_count = Permission::where('parent_id', '=', $parent_id)->count();
                $permit['child_permissions'] = ( $child_permissions_count > 0 ) ? $child_permissions : [];
                $permissions_array[] = $permit;
            }
        }

    		if($id != -1) {
    			$profile =  Profile::with('permissions')->find($id);

          foreach($permissions_array as $key => $permission) {

              foreach($profile->permissions as $checked_permission) {

                  if($permission['id'] == $checked_permission->id) {
                      $permissions_array[$key]['checked'] = true; 
                  }

                  if(!empty($permission['child_permissions'])) {
                      foreach($permission['child_permissions'] as $childKey => $child_permission) {
                        if($child_permission['id'] == $checked_permission->id) {
                            $permissions_array[$key]['child_permissions'][$childKey]['checked'] = true;
                        }
                      }
                  }
              }
          }
    		}

       //die(print_r($permissions_array));

        return view('admin.profiles.edit', compact(['profile', 'permissions_array', 'breadcrumbs']));
	}
    
    public function store(Request $request) {
         
        $current_user = Auth::user();
        $profile = new Profile();

        if($request->id != '' || $request->id != null) {
             $profile = Profile::where('id', '=', $request->id)->with('permissions')->first();

             if(!empty($profile->permissions)) {
                 foreach($profile->permissions as $permission_to_delete) {
                     $profile->permissions()->detach($permission_to_delete);
                 }
             }
        }

        $profile->title = $request->title;
        $profile->id = $request->id;
        $profile->deleted = 0;

        $profile->save();

        if(!empty($request->permissions)) {

            foreach($request->permissions as $permission_id) {
                $permission_to_add = Permission::find($permission_id);
                $profile->permissions()->save($permission_to_add);
            }
        }


        $request->session()->flash('status', 'Profile Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated' ) );
        
        return redirect($request->route()->getPrefix().'/profiles');
    }

    public function delete(Request $request, $id = null)  {
         
         $profile = Profile::where('id', '=', $id)->with('permissions')->first();
         
         
         if(!empty($profile->permissions)) {
             foreach($profile->permissions as $permission_to_delete) {
                 $profile->permissions()->detach($permission_to_delete);
             }
         }
         
         $profile->deleted = 1;
         $profile->save();

         $request->session()->flash('status', 'Profile Successfully Deleted' );
         $request->session()->flash('cancel_status', $id );

         return redirect($request->route()->getPrefix().'/profiles');
    }

    public function undo_delete(Request $request, $id = null) {

        $profile = Profile::where('id', '=', $id)->with('permissions')->first();

        $profile->deleted = 0;
        $profile->save();

        $request->session()->flash('status', 'Profile Successfully Restored' );
        return redirect($request->route()->getPrefix().'/profiles');
    }
	

}