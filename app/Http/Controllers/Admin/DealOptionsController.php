<?php
namespace App\Http\Controllers\Admin;

use App\DealOption;
use App\Deal;
use App\Country;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DealOptionsController extends Controller {

    public $breadcrumbs = [];
    

	public function index($deal_id = null){
         
         $deal = Deal::find($deal_id);

         if($deal == null) {
              return $this->redirect()->back();
         }

		 $breadcrumbs = [
		      [
		          'title' => t('Dashboard'),
			        'url' => ''.route('admin.dashboard'),
              'active' => false
		      ],
          [
              'title' => 'Deals',
              'url' => ''.route('admin.deals'),
              'active' => false
          ],
		      [
		          'title' => $deal->title_en.' Options',
			        'url' => ''.route('admin.deals.options', ['deal_id' => $deal_id]),
			        'active' => true
		      ]
		 ];



		 $dealOptions = DealOption::with('deal')->where('deal_id', '=', $deal_id)->get();
		 //die(print_r($deals));
		 $deals = Deal::lists('title_en', 'id');

		 return view('admin.deals.options.index', compact(['deal', 'dealOptions', 'breadcrumbs', 'deals']));
	}

	public function edit($deal_id = null, $id = null) {
        
		if($id == -1) {
			$dealOption = new DealOption;
            $dealOption->deal_id = $deal_id;
			return json_encode($dealOption);
		}

        return DealOption::where('id', '=', $id)->with('deal')->first();
	}
    
    public function store(Request $request) {
         

        $dealOption = new DealOption();

        if($request->id != '' || $request->id != null) {
             $dealOption = DealOption::where('id', '=', $request->id)->with('deal')->first();
        }

        //die($request->title);
        $dealOption->price = $request->price;
        $dealOption->title_en = $request->title_en;
        $dealOption->title_ar = $request->title_ar;
        $dealOption->id = $request->id;

        $dealOption->deal_id = $request->deal_id;

        $dealOption->save();

        $request->session()->flash('status', t('Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect('admin/deals/options/'.$request->deal_id);
    } 

    public function delete(Request $request, $id = null)  {

         
         $dealOption = DealOption::where('id', '=', $request->id)->first();

         $deal_id = $dealOption->deal_id;
         
         $dealOption->delete();

         $request->session()->flash('status', t('Successfully Deleted') );

         return redirect('admin/deals/options/'.$deal_id);
    }
	

}