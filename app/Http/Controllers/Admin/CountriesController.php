<?php
namespace App\Http\Controllers\Admin;

use App\Country;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CountriesController extends Controller {

    public $breadcrumbs = [];
    

	public function index(){

		 $breadcrumbs = [
		      [
		          'title' => t('Dashboard'),
			      'url' => ''.route('admin.dashboard'),
			      'active' => true
		      ],
		      [
		          'title' => t('Countries'),
			      'url' => ''.route('admin.countries'),
			      'active' => true
		      ]
		 ];

		 $countries = Country::all();

		 return view('admin.countries.index', compact(['countries', 'breadcrumbs']));
	}

	public function edit($id = null) {

		if($id == -1) {
			$country = new Country;
			return json_encode($country);
		}

		return Country::where('id', '=', $id)->first();
	}
    
    public function store(Request $request) {

        $country = new Country();

        if($request->id != '' || $request->id != null) {
             $country = Country::where('id', '=', $request->id)->first();
        }

        //die($request->title);

        $country->title_en = $request->title_en;
        $country->title_ar = $request->title_ar;
        $country->id = $request->id;
        $imageName = $country->title_en . '.' . $request->file('image_path')->getClientOriginalExtension();
        $country->image_path = $imageName;
        $country->currency = $request->currency;
        $country->facebook_link = $request->facebook_link;
        $country->instagram_link = $request->instagram_link;
        $country->twitter_link = $request->twitter_link;
        $country->pinterest_link = $request->pinterest_link;
        $country->google_plus_link = $request->google_plus_link;

        $request->file('image_path')->move(base_path() . '/public/images/flags/', $imageName);

        $country->save();

        $request->session()->flash('status', t('Country Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect('admin/countries');
    }

    public function delete(Request $request, $id = null)  {
         
         $country = Country::where('id', '=', $id)->first();
         
         if(!is_null($country->image_path) && file_exists(base_path() . '/public/images/flags/'.$country->image_path)) {
         	 unlink(base_path() . '/public/images/flags/'.$country->image_path);
         }

         $country->delete();

         $request->session()->flash('status', t('Country Successfully Deleted') );

         return redirect('admin/countries');
    }
	

}