<?php
namespace App\Http\Controllers\Admin;

use App\TravelLocation;
use App\Category;
use App\Country;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TravelLocationsController extends Controller {

    public $breadcrumbs = [];
    

	public function index($category_id = null){
         
         $category = Category::find($category_id);

         $country = Country::where('id', '=', $category->country_id)->first();

         if($category == null) {
              return $this->redirect()->back();
         }

		 $breadcrumbs = [
          [
               'title' => 'Dashboard',
               'url' => ''.route('admin.dashboard'),
               'active' => false
          ],
          [
              'title' =>  'Countries',
              'url' => ''.route('admin.countries'),
              'active' => false
          ],
          [
              'title' => $country->title_en.' Categories',
              'url' => ''.route('admin.country.categories', ['country_id' => $category->country_id ]),
              'active' => false
          ],
          [
            'title' => $category->title_en.' Locations',
            'url' => ''.route('admin.travellocations', ['category_id' => $category_id]),
            'active' => true
          ]
     ];



		 $travelLocations = TravelLocation::with('category')->where('category_id', '=', $category_id)->get();
		 //die(print_r($categories));
		 $categories = Category::lists('title_en', 'id');

		 return view('admin.travellocations.index', compact(['category', 'categories', 'breadcrumbs', 'travelLocations']));
	}

	public function edit($category_id = null, $id = null) {
        
		if($id == -1) {
			$travelLocation = new TravelLocation;
            $travelLocation->category_id = $category_id;
			return json_encode($travelLocation);
		}

        return TravelLocation::where('id', '=', $id)->with('category')->first();
	}
    
    public function store(Request $request) {
         

        $travelLocation = new TravelLocation();

        if($request->id != '' || $request->id != null) {
             $travelLocation = TravelLocation::where('id', '=', $request->id)->with('category')->first();
        }

        //die($request->title);

        $travelLocation->title_en = $request->title_en;
        $travelLocation->title_ar = $request->title_ar;
        $travelLocation->id = $request->id;

        $travelLocation->category_id = $request->category_id;

        $travelLocation->save();

        $request->session()->flash('status', t('Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect('admin/travellocations/'.$request->category_id);
    } 

    public function delete(Request $request, $id = null)  {

         
         $travelLocation = TravelLocation::where('id', '=', $request->id)->first();

         $category_id = $travelLocation->category_id;
         
         $travelLocation->delete();

         $request->session()->flash('status', t('Successfully Deleted') );

         return redirect('admin/travellocations/'.$category_id);
    }
	

}