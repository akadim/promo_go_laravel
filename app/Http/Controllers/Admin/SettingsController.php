<?php
namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryCountry;
use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Sitesetting;

use Illuminate\Http\Request;

class SettingsController extends Controller {

    public $breadcrumbs = [];

    public function siteSettings($request = null) {
    	return DB::table('sitesettings')->whereName($request)->first();
    }
    

	public function index(Request $request){

		 $breadcrumbs = [
		      [
		          'title' => 'Dashboard',
			        'url' => ''.route('admin.dashboard'),
			        'active' => false
		      ],
		      [
		          'title' => 'Settings',
			        'url' => ''.route('admin.settings'),
			        'active' => true
		      ]
		 ];

		 if($request->isMethod('post')) {
              
              //$setting = Sitesetting::first();

              $setting = $this->siteSettings('about_us_en');
              DB::table('sitesettings')->where('name', $setting->name)->update(['value' => $request->about_us_en]);

              $setting = $this->siteSettings('about_us_ar');
              DB::table('sitesettings')->where('name', $setting->name)->update(['value' => $request->about_us_ar]);

              $setting = $this->siteSettings('contact_us_en');
              DB::table('sitesettings')->where('name', $setting->name)->update(['value' => $request->contact_us_en]);

              $setting = $this->siteSettings('contact_us_ar');
              DB::table('sitesettings')->where('name', $setting->name)->update(['value' => $request->contact_us_ar]);

              $setting = $this->siteSettings('terms_conditions_en');
              DB::table('sitesettings')->where('name', $setting->name)->update(['value' => $request->terms_conditions_en]);

              $setting = $this->siteSettings('terms_conditions_ar');
              DB::table('sitesettings')->where('name', $setting->name)->update(['value' => $request->terms_conditions_ar]);

              $setting = $this->siteSettings('privacy_policy_en');
              DB::table('sitesettings')->where('name', $setting->name)->update(['value' => $request->privacy_policy_env]);

              $setting = $this->siteSettings('privacy_policy_ar');
              DB::table('sitesettings')->where('name', $setting->name)->update(['value' => $request->privacy_policy_ar]);

              $request->session()->flash('status', 'Settings successfully updated');

              return redirect()->route('admin.settings');

		 }

		 $settings = Sitesetting::all();
		 $settings = $settings->toArray();

		 return view('admin.users.settings', compact(['breadcrumbs', ['settings']]));
	}


}