<?php
namespace App\Http\Controllers\Admin;

use App\Store;
use App\Company;
use App\CompanyAssignment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class StoresController extends Controller {

    public $breadcrumbs = [];
    

	public function index(){

		 $breadcrumbs = [
		      [
		           'title' => t('Dashboard'),
			         'url' => ''.route('admin.dashboard'),
               'active' => false
		      ],
          [
              'title' => 'Stores',
              'url' => ''.route('admin.stores'),
              'active' => true
          ]
		 ];

         $current_user = Auth::user();

         $stores = [];

         if($current_user->is_super_admin) {
             $stores = Store::with('company')->where('deleted', '=', 0)->get();
         }
         else {
             $company_assignment_count = CompanyAssignment::where('user_id', '=', $current_user->id)->count();

             if($company_assignment_count > 0) {
                $company_assignment = CompanyAssignment::where('user_id', '=', $current_user->id)->first();
                $stores = Store::with('company')->where('company_id', '=', $company_assignment->company_id)->where('deleted', '=', 0)->get();
             }

         }

		 $companies = ['' => ' ---- Choose a Company ---- '] + Company::where('deleted', '=', 0)->lists('title', 'id')->all();

		 return view('admin.stores.index', compact(['companies', 'breadcrumbs', 'stores']));
	}

	public function edit($id = null) {
        
		if($id == -1) {
			$store = new Store;
			return json_encode($store);
		}
 
    return Store::where('id', '=', $id)->with('company')->first();
	}
    
    public function store(Request $request) {
         
        $current_user = Auth::user();
        
        $store = new Store();

        if($request->id != '' || $request->id != null) {
             $store = Store::where('id', '=', $request->id)->with('company')->first();
        }

        //die($request->title);

        $store->title = $request->title;
        $store->location = $request->location;
        $store->zipcode = $request->zipcode;
        $store->city = $request->city;
        $store->country = $request->country;
        $store->phone = $request->phone;
        
        if($current_user->is_super_admin) {
             $store->company_id = $request->company_id;
        }
        else {
            $company_assignment = CompanyAssignment::where('user_id', '=', $current_user->id)->first();
            $store->company_id = $company_assignment->company_id;
        }

        
        $store->id = $request->id;
        $store->deleted = 0;

        $store->save();

        $request->session()->flash('status', t('Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect($request->route()->getPrefix().'/stores');
    } 

    public function delete(Request $request, $id = null)  {

         
         $store = Store::where('id', '=', $request->id)->first();
         
         $store->deleted = 1;

         $store->save();

         $request->session()->flash('status', t('Successfully Deleted') );
         $request->session()->flash('cancel_status', $id );

         return redirect($request->route()->getPrefix().'/stores');
    }

    public function undo_delete(Request $request, $id = null) {

         $store = Store::where('id', '=', $request->id)->first();
         
         $store->deleted = 0;

         $store->save();

         $request->session()->flash('status', 'Successfully Restored' );

         return redirect($request->route()->getPrefix().'/stores');
    }
	

}