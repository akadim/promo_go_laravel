<?php
namespace App\Http\Controllers\Admin;

use App\Profile;
use App\Store;
use App\Company;
use App\CompanyAssignment;
use App\StoreAssignment;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class StoresManagersController extends Controller {

    public $breadcrumbs = [];
    

	public function index(){

		 $breadcrumbs = [
		      [
		          'title' => 'Dashboard',
			      'url' => ''.route('admin.dashboard'),
			      'active' => true
		      ],
		      [
		          'title' => 'Stores Managers',
			      'url' => ''.route('admin.stores.managers'),
			      'active' => true
		      ]
		 ];

         $current_user = Auth::user();

         $store_assignments = StoreAssignment::all();

         $stores_managers = [];

         $companies = ['' => ' ---- Choose a Company ---- '] + Company::where('deleted', '=', 0)->lists('title', 'id')->all();

         $current_company_assignment = ($current_user->is_super_admin) ? [] : CompanyAssignment::where('user_id', '=', $current_user->id)->first();

         $stores = [];

         if($current_user->is_super_admin) {
            $stores = Store::where('deleted', '=', 0)->lists('title', 'id')->all();
         }
         else {
            $stores = Store::where('deleted', '=', 0)->where('company_id', '=', $current_company_assignment->company_id)->lists('title', 'id')->all();
         }

         $stores = ['' => ' ---- Choose a Store ---- '] + $stores;

         foreach($store_assignments as $store_assignment) {

               $user = User::find($store_assignment->user_id);
               $store = Store::where('id', '=', $store_assignment->store_id)->with('company')->first();

               if(!$user->deleted && ( $current_user->is_super_admin || (!$current_user->is_super_admin && $store->company_id == $current_company_assignment->company_id ) ) ) {
                  $stores_managers[] = ['store_manager' => $user, 'store' => $store];
               }
         }

		 return view('admin.stores.managers.index', compact([ 'stores_managers','stores', 'breadcrumbs', 'companies']));
	}

	public function edit($id = null) {
        

        $user = User::where('deleted', '=', 0)->where('id', '=', $id)->with('profile')->first();
        $store_id = 0;
        $company_id = 0;

        if(intval($id) == -1) {
            $user = new User;
        }



        $store_assignment_count = StoreAssignment::where('user_id', '=', $user->id)->count();

        if($store_assignment_count > 0) {
            $store_assignment = StoreAssignment::where('user_id', '=', $user->id)->first();
            $store_id = $store_assignment->store_id;
            $store = Store::where('id', '=', $store_id)->first();
            $company_id = $store->company_id;
        }

        return response()->json([
                   'store_manager' => $user,
                   'store_id' => $store_id,
                   'company_id' => $company_id
            ]);
	}

    public function store_section($company_id=null, $store_id = null) {
        $stores = ['' => ' ---- Choose a Store ----- '] + Store::where('company_id', '=', $company_id)->lists('title', 'id')->all();

        return view('admin.stores.managers.store_section', compact(['stores', 'store_id']));
    }
    
    public function store(Request $request) {
         

        $store_manager = new User();
        $store_manager_profile = Profile::where('title', '=', 'Store Manager')->first();

        if($request->id != '' || $request->id != null) {
             $store_manager = User::where('id', '=', $request->id)->first();
        }

        //die($request->title);

        $store_manager->fullname = $request->fullname;
        $store_manager->phone = $request->phone;
        $store_manager->email = $request->email;
        $store_manager->profile_id = $store_manager_profile->id;

        if(isset($request->id) && (is_null($request->id) || $request->id == '')) {
             $store_manager->password = bcrypt($request->password);
        }
        
        $store_manager->id = $request->id;
        $store_manager->active = 1;
        $store_manager->deleted = 0;

        $store_manager->save();

        $store_assignment_count = StoreAssignment::where('user_id', '=', $store_manager->id)->count();

        $store_assignment = StoreAssignment::where('user_id', '=', $store_manager->id)->first();

        if($store_assignment_count <= 0) {
             $store_assignment = new StoreAssignment;
        }
       
       $store_assignment->user_id = $store_manager->id;
       $store_assignment->store_id = $request->store_id;
       $store_assignment->save();

        $request->session()->flash('status', t('Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect($request->route()->getPrefix().'/stores-managers');
    }

    public function delete(Request $request, $id = null)  {
         
         $store_manager = User::where('id', '=', $id)->first();
                 
         /*
         if(file_exists(base_path() . '/public/images/stores/'.$store->image_path)) {
           unlink(base_path() . '/public/images/stores/'.$store->image_path);
         }
         */
         
         $store_manager->deleted = 1;

         $store_manager->save();

         $request->session()->flash('status', t('Successfully Deleted') );
         $request->session()->flash('cancel_status', $id );

         return redirect($request->route()->getPrefix().'/stores-managers');
    }

    public function undo_delete(Request $request, $id=null) {

         $store = User::where('id', '=', $id)->first();
         
         $store->deleted = 0;

         $store->save();

         $request->session()->flash('status', 'Successfully Restored' );

         return redirect($request->route()->getPrefix().'/stores-managers');
    }

    public function change_password(Request $request) {
        if($request->isMethod('post')) {
            $store_manager = User::find($request->manager_id);
            $store_manager->password = bcrypt($request->new_password);
            $store_manager->save();

            $request->session()->flash('status', 'Password Successfully Changed' );

            return redirect($request->route()->getPrefix().'/stores-managers');
        }
    }
	
    public function verify_email(Request $request) {

        $email = $request->email;
        
        $store_managers_count = User::where('email', '=', $email)->count();

        $response = ($store_managers_count > 0) ? 'Exist' : 'Not Exist';

        return response()->json(['response' => $response]);
    }

}