<?php
namespace App\Http\Controllers\Admin;

use App\Deal;
use App\Category;
use App\Company;
use App\DealPosition;
use App\CompanyAssignment;
use App\StoreAssignment;
use App\DealAssignment;
use App\Store;
use App\Sector;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

use Carbon\Carbon;

use Illuminate\Http\Request;

class DealsController extends Controller {

    public $breadcrumbs = [];

    public function list_deals($limit=null, $skip=null, $company_id=null, $sector_id=null) {

        $deals = Deal::with('company')->where('deleted', '=', 0)->get();
        
        if($limit != null) {
            $deals = Deal::with('company')->take($limit)->where('deleted', '=', 0)->get();
        }

        if($skip != null) {
            $deals = Deal::with('company')->take($limit)->skip($skip)->where('deleted', '=', 0)->get();
        }

        if($sector_id != null) {
            $all_deals = Deal::where('deleted', '=', 0)->with('company')->get();
            $deals = [];

            foreach ($all_deals as $one_deal) {
                if(intval($one_deal->company->sector_id) == intval($sector_id)) {
                    $deals [] = $one_deal;
                }
            }
        }

        if($company_id != null) {
            $deals = Deal::where('deleted', '=', 0)->where('company_id', '=', $company_id)->get();
        }

        $current_user = Auth::user();

        if(!$current_user->is_super_admin) {

            $current_company_id = 0;

            $company_assignment_count = CompanyAssignment::where('user_id', '=', $current_user->id)->count();
            $store_assignment_count = StoreAssignment::where('user_id', '=', $current_user->id)->count();

            if($company_assignment_count > 0) {
                $company_assignment = CompanyAssignment::where('user_id', '=', $current_user->id)->first();
                $current_company_id = $company_assignment->company_id;
            }
            else if($store_assignment_count > 0)  {
                $store_assignment = StoreAssignment::where('user_id', '=', $current_user->id)->first();
                $current_store = Store::where('id', '=', $store_assignment->store_id)->first();
                $current_company_id = $current_store->company_id;
            } 

            $deals = Deal::with('company')->where('company_id', '=', $current_company_id)->where('deleted', '=', 0)->get();

            if($limit != null) {
              $deals = Deal::with('company')->where('company_id', '=', $current_company_id)->take($limit)->where('deleted', '=', 0)->get();
            }

            if($skip != null) {
              $deals = Deal::with('company')->where('company_id', '=', $current_company_id)->take($limit)->where('deleted', '=', 0)->skip($skip)->get();
            }


        }

        $organized_deals = $deals;

        $deals = [];

        foreach($organized_deals as $organized_deal) {
            if($organized_deal->status == "ongoing") {
                $deals[] = $organized_deal;
            }
        }

        foreach($organized_deals as $organized_deal) {
            if($organized_deal->status == "expired") {
                $deals[] = $organized_deal;
            }
        }

        return $deals;
    }

  private function get_deals_number_by_company($company_id = null) {
           return Deal::where('company_id', '=', $company_id)->count();
  }
    

	public function index(){

		 $breadcrumbs = [
		      [
		          'title' => 'Dashboard',
			        'url' => ''.route('admin.dashboard'),
			        'active' => false
		      ],
		      [
		          'title' => 'Deals',
			        'url' => ''.route('admin.deals'),
			        'active' => true
		      ]
		 ];

        $deals = $this->list_deals(12);

        $available_sectors = Sector::where('deleted', '=', 0)->with('companies')->get();

        $sectors = [];

        foreach($available_sectors as $current_sector) {
            
            foreach($current_sector->companies as $current_company) {
                if($this->get_deals_number_by_company($current_company->id) > 0) {
                     $sectors [] = $current_sector;
                }
            }

        }

        $company_deals = Deal::where('deleted', '=', 0)->get();

        $company_ids = [];

        foreach($company_deals as $company_deal) {
          $company_ids[] = $company_deal->company_id;
        }

        $company_ids = array_unique($company_ids);

        $filter_companies = Company::whereIn('id', $company_ids)->get(); 


        /* Just for getting the entire Deals */
        $full_deals = $this->list_deals();

        $deals_count = sizeof($full_deals);
        /* The end part of getting the number of the entire deals*/

        //die(print_r($deals));

        $current_time = Carbon::now()->timestamp;

        $companies = ['' => ' ------ Choose a Company ------ '] + Company::lists('title', 'id')->all();

		 return view('admin.deals.index', compact(['deals', 'breadcrumbs', 'companies', 'current_time', 'deals_count', 'sectors', 'filter_companies']));
	}

  public function sector_deals(Request $request) {

       $sector_id = $request->sector_id;

       $deals = $this->list_deals(null, null, null, $sector_id);

       $current_time = Carbon::now()->timestamp;

       return view('admin.deals.sector_deals', compact(['deals', 'current_time']));
  }

  public function company_deals(Request $request) {

       $company_id = $request->company_id;

       $deals = $this->list_deals(null, null, $company_id);

       $current_time = Carbon::now()->timestamp;

       return view('admin.deals.sector_deals', compact(['deals', 'current_time']));
  }

  public function show_more(Request $request) {

        $start_point = $request->deals_limit;

        $current_time = Carbon::now()->timestamp;

        $deals = $this->list_deals(12, $start_point);

        return view('admin.deals.more_deals', compact(['deals', 'current_time']));
  }

	public function edit($id = null) {
        
        $deal = new Deal;

		    if($id != -1) {
            $deal = Deal::where('id', '=', $id)->with('company')->first();
		    }  

        return $deal;
	}

  private function createRandomPassword() { 
    
    $random_number = "";

    for($i=1; $i <= 13; $i++) {
        $random_number = $random_number."".rand(0, 9);
    }

    return intval($random_number); 

   } 
    
    public function store(Request $request) {
         

        $deal = new Deal();
        $current_user = Auth::user();
        $current_user = User::where('id', '=', $current_user->id)->with('profile')->first();
        $deal_assignment = new DealAssignment;

        if($request->id != '' || $request->id != null) {
             $deal = Deal::where('id', '=', $request->id)->first();

             $deal_assignments_count = DealAssignment::where('user_id', '=', $current_user->id)->count();
             if($deal_assignments_count > 0) {
                 $deal_assignments = DealAssignment::where('user_id', '=', $current_user->id)->get();

                 foreach($deal_assignments as $deal_assignment) {
                    $deal_assignment->delete();
                 }
             }

             $request->session()->flash('updated_deal', $deal->discount_code);
        }

        //die($request->title);
        
        if($request->is_condition == true || $request->is_condition == 1) {
            $deal->description = $request->description;
            $deal->discount = 0;
        }
        else if($request->is_condition == false || $request->is_condition == 0) {
            $deal->discount = $request->discount;
            $deal->description = '';
        }
        
        if($request->id == null) {
            $deal->discount_code = $this->createRandomPassword();
        }

        $deal->number_of_deals = $request->number_of_deals;

        $deal->start_date = $request->start_date;
        $deal->end_date = $request->end_date;
        $current_time = Carbon::now()->timestamp;

        if(strtotime($deal->end_date) > $current_time) {
            $deal->status = 'ongoing';
        }
        else {
            $deal->status = 'expired';
        }
        
        $deal->deleted = 0;

        if(!is_null($request->file('imagePath'))) {

            $iconName  = $company->title . '_icon.' . $request->file('imagePath')->getClientOriginalExtension();

            $company->imagePath = $iconName;

            $request->file('imagePath')->move(base_path() . '/public/images/companies/', $iconName);
        }

        if($current_user->is_super_admin) {
             $deal->company_id = $request->company_id;
        }
        else {
            
             $store_assignments_count = StoreAssignment::where('user_id', '=', $current_user->id)->count();
             $company_assignments_count = CompanyAssignment::where('user_id', '=', $current_user->id)->count();

             if($store_assignments_count > 0) {
                $store_assignment = StoreAssignment::where('user_id', '=', $current_user->id)->first();
                $store = Store::where('id', '=', $store_assignment->store_id)->first();
                $deal->company_id = $store->company_id;
             }
             else if($company_assignments_count > 0) {
                $company_assignment = CompanyAssignment::where('user_id', '=', $current_user->id)->first();
                $deal->company_id = $company_assignment->company_id;
             }
        }

        $deal->is_condition = $request->is_condition;

        $deal->id = $request->id;



        //die(var_dump($deal));
        
        //die();

        $deal->save();

        $deal_assignment->deal_id = $deal->id;
        $deal_assignment->user_id = $current_user->id;
        $deal_assignment->save();

        $request->session()->flash('status', t('Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        $request->session()->flash('api_deal', $deal->id);
        
        return redirect($request->route()->getPrefix().'/deals');
    }

    public function get_deal(Request $request) {

          $id = $request->id;
          
          $deal = Deal::where('id','=', $id)->with('company')->with('deal_positions')->first();

          return response()->json(['deal' => $deal], 200);
    }

    public function delete(Request $request, $id = null)  {
         
         $deal = Deal::where('id', '=', $id)->first();
         
         $deal->deleted = 1;
         $deal->save();

         $request->session()->flash('status', t('Successfully Deleted') );
         $request->session()->flash('cancel_status', $id );
         $request->session()->flash('api_deal', $deal->id);
         $request->session()->flash('updated_deal', $deal->discount_code);

         return redirect($request->route()->getPrefix().'/deals');
    }

    public function undo_delete(Request $request, $id = null) {
         $deal = Deal::where('id', '=', $id)->first();
         
         $deal->deleted = 0;
         $deal->save();

         $request->session()->flash('status', 'Successfully Restored' );
         $request->session()->flash('api_deal', $deal->id);
         $request->session()->flash('updated_deal', $deal->discount_code);

         return redirect($request->route()->getPrefix().'/deals');
    }

    public function markers( $id = null, $store_id = null) {

        $deal_positions = DealPosition::where('deal_id', '=', $id)->where('store_id', '=', $store_id)->get();
        $deal_coordinates = [];
        $deal_positions_available = DealPosition::where('deal_id', '=', $id)->where('store_id', '=', $store_id)->count();;
        $deal_positions_catched = 0;
        $deal_positions_expired = 0;
        $deal_positions_ongoing = 0;
        $deal_positions_used = 0;

        foreach($deal_positions as $deal_position) {

          if($deal_position->status == 'catched') {
              $deal_positions_catched += 1;
          }
          else if($deal_position->status == 'expired') {
              $deal_positions_expired += 1;
          }
          else if($deal_position->status == 'ongoing') {
              $deal_positions_ongoing += 1;
          }
          else if($deal_position->status == 'used') {
              $deal_positions_used += 1;
          }

          $deal_coordinates[] = [$deal_position->latitude, $deal_position->longitude, $deal_position->duration_to_goal, $deal_position->status, $deal_position->id];
        }

        return response()->json([ 
              'data' => $deal_coordinates,
              'deal_positions_available' => $deal_positions_available,
              'deal_positions_catched' => $deal_positions_catched,
              'deal_positions_expired' => $deal_positions_expired,
              'deal_positions_ongoing' => $deal_positions_ongoing,
              'deal_positions_used' => $deal_positions_used
          ]);
    }

    private function expire_the_deal($id=null) {

        $deal_postions_count = DealPosition::where('deal_id', '=', $id)->count();
        
        if($deal_postions_count > 0) {
            $deal_positions = DealPosition::where('deal_id', '=', $id)->get();

            foreach($deal_positions as $deal_position){
                $deal_position->status = 'expired';
                $deal_position->save();
            }
        }
    }

    public function marking(Request $request, $id = null) {

          $deal = Deal::where('id', '=', $id)->with('company')->first();
          $deal_pos_count = DealPosition::where('deal_id', '=', $id)->count();
          $company = Company::where('id', '=', $deal->company_id)->with('stores')->first();
          $stores = $company->stores;


          if(strtotime($deal->end_date) <= strtotime(date('Y-m-d H:i:s'))) {
              $this->expire_the_deal($id);
              return redirect()->route('admin.deal.results', ['id' => $id]);
          }

          $max_number_deals = intval($deal->number_of_deals) - intval($deal_pos_count);

          $stores_params = [];


          foreach($stores as $store) {
              $stores_params[$store->id] = $store->location;
          }

          $stores_params = ['' => ' ----- Select a Location ----- ']+$stores_params;

          $breadcrumbs = [
              [
                  'title' => 'Dashboard',
                  'url' => ''.route('admin.dashboard'),
                  'active' => false
              ],
              [
                  'title' => 'Deals',
                  'url' => ''.route('admin.deals'),
                  'active' => false
              ],
              [
                  'title' => 'DEAL CODE ('.$deal->discount_code.') POSITIONS',
                  'url' => ''.route('admin.deals.marking', ['id' => $id]),
                  'active' => true
              ]
         ];

         if($request->isMethod('post')) {


              $dealpositions_count = DealPosition::where('store_id', '=', $request->store_id)->count();
              $dealpositions = DealPosition::where('store_id', '=', $request->store_id)->get();

              $api_deal = Deal::where('id', '=', $request->deal_id)->first();
              $discount_code = $api_deal->discount_code;

              $store = Store::find($request->store_id);

              if($dealpositions_count > 0) {
                  foreach ($dealpositions as $dealposition) {
                      $dealposition->delete();
                  }
              }

              $positions = $request->positions;
              $firebase_positions = [];
              $number_of_positions = sizeof($positions);

              //die("store id = ".$request->store_id);

              foreach($positions as $position) {                  
                    $dealposition = new DealPosition;
                    $dealposition->store_id = $request->store_id;
                    $dealposition->latitude = $position['lat'];
                    $dealposition->longitude = $position['lng'];
                    $dealposition->deal_id = $request->deal_id;
                    $dealposition->status = 'ongoing';
                    $dealposition->duration_to_goal = $request->duration_to_goal;
                    $dealposition->duration_time_type = $request->duration_time_type;
                    $dealposition->save();
                    $firebase_positions[] = $dealposition;
              }

              $number_of_positions_of_deal = DealPosition::where('deal_id', $request->deal_id)->count();

              $for_this_deal = Deal::find($request->deal_id);

              $remaining_deals_positions = intval($for_this_deal->number_of_deals) - intval($number_of_positions_of_deal);

              return response()->json([
                           'message' => 'The locations has been markes successfully',
                           'number_of_positions' => $number_of_positions,
                           'store_id' => $request->store_id,
                           'store_address' => $store->location,
                           'discount_code' => $discount_code,
                           'positions' => $firebase_positions,
                           'remaining_deals_positions' => $remaining_deals_positions
                      ]);
         }

          return view('admin.deals.marking', compact(['deal', 'breadcrumbs', 'stores_params', 'id', 'max_number_deals']));
    }

    public function expire_deal($deal_id=null) {
          

          $deal = Deal::find($deal_id);
          $deal->status = 'expired';
          $deal->save();

          $deal_position_count = DealPosition::where('deal_id', '=', $deal_id)->where('status', '<>', 'catched')->count();
          $deal_positions = DealPosition::where('deal_id', '=', $deal_id)->get();
          
          if($deal_position_count > 0) {
              foreach($deal_positions as $deal_position) {
                 $deal_position->status = 'expired';
                 $deal_position->save();
              }
          } 

          return response()->json([
                           'message' => 'Deal got expired'
                      ]);

    }


    public function show_results($deal_id=null) {
         
         //$this->expire_the_deal($deal_id);

         $deal = Deal::where('id', '=', $deal_id)->with('company')->first();


         $deal_positions_number = DealPosition::where('deal_id', '=', $deal_id)->count();

         $ongoing_deals = DealPosition::where('deal_id', '=', $deal_id)->where('status', '=', 'ongoing')->count();
         $taken_deals = DealPosition::where('deal_id', '=', $deal_id)->where('status', '=', 'catched')->count();
         $consumed_deals = DealPosition::where('deal_id', '=', $deal_id)->where('status', '=', 'used')->count();
         $expired_deals = DealPosition::where('deal_id', '=', $deal_id)->where('status', '=', 'expired')->count();

         $company = Company::where('id', '=', $deal->company_id)->with('stores')->first();
         $stores = $company->stores;

         $stores_params = [];


          foreach($stores as $store) {
              $stores_params[$store->id] = $store->location;
          }

          $stores_params = ['' => ' ----- Select a Location ----- ']+$stores_params;

          $breadcrumbs = [
              [
                  'title' => 'Dashboard',
                  'url' => ''.route('admin.dashboard'),
                  'active' => false
              ],
              [
                  'title' => 'Deals',
                  'url' => ''.route('admin.deals'),
                  'active' => false
              ],
              [
                  'title' => 'DEAL CODE ('.$deal->discount_code.') RESULTS',
                  'url' => ''.route('admin.deal.results', ['id' => $deal_id]),
                  'active' => true
              ]
         ];

        return view('admin.deals.deal_results', compact(['deal', 'stores_params', 'breadcrumbs', 'deal_id', 'deal_positions_number', 'ongoing_deals', 'taken_deals', 'consumed_deals', 'expired_deals']));
    }

    public function show_info($deal_id=null, $position_id = null, $validated = null) {
          
          $deal_position = new DealPosition;

          if($position_id != -1) {
              $deal_position = DealPosition::where('id', '=', $position_id)->first();
          }

          $deal = Deal::where('id', '=', $deal_id)->with('company')->first();

          return view('admin.deals.show_info', compact(['deal_position', 'deal', 'validated']));
    }

    public function change_time_to_goal($position_id=null, $time_to_goal=null) {
          
          $deal_position_count = DealPosition::where('id', '=', $position_id)->count();

          $deal_position = DealPosition::where('id', '=', $position_id)->first();
          $deal_position->duration_to_goal = $time_to_goal;
          $deal_position->save();

          return response()->json([
                 'time_to_goal' => $deal_position->duration_to_goal,
                 'position_id' => $position_id
            ]);
    }
	

}