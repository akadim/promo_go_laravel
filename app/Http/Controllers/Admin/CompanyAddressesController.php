<?php
namespace App\Http\Controllers\Admin;

use App\CompanyAddress;
use App\Company;
use App\Country;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CompanyAddressesController extends Controller {

    public $breadcrumbs = [];
    

	public function index($company_id = null){
         
         $company = Company::find($company_id);

         if($company == null) {
              return $this->redirect()->back();
         }

		 $breadcrumbs = [
		      [
		          'title' => t('Dashboard'),
			        'url' => ''.route('admin.dashboard'),
              'active' => false
		      ],
          [
              'title' => t('Companies'),
              'url' => ''.route('admin.companies'),
              'active' => false
          ],
		      [
		          'title' => $company->name.' Addresses',
			        'url' => ''.route('admin.companies.addresses', ['company_id' => $company_id]),
			        'active' => true
		      ]
		 ];



		 $companyAddresses = CompanyAddress::with('company')->where('company_id', '=', $company_id)->get();
		 //die(print_r($companies));
		 $companies = Company::lists('name', 'id');

		 return view('admin.companies.addresses.index', compact(['company', 'companies', 'breadcrumbs', 'companyAddresses']));
	}

	public function edit($company_id = null, $id = null) {
        
		if($id == -1) {
			$companyAddress = new CompanyAddress;
            $companyAddress->company_id = $company_id;
			return json_encode($companyAddress);
		}

        return CompanyAddress::where('id', '=', $id)->with('company')->first();
	}
    
    public function store(Request $request) {
         

        $companyAddress = new CompanyAddress();

        if($request->id != '' || $request->id != null) {
             $companyAddress = CompanyAddress::where('id', '=', $request->id)->with('company')->first();
        }

        //die($request->title);

        $companyAddress->title = $request->title;
        $companyAddress->address = $request->address;
        $companyAddress->phone = $request->phone;
        $companyAddress->email = $request->email;
        $companyAddress->google_map_link = $request->google_map_link;
        $companyAddress->show_banner_header = $request->show_banner_header;
        $companyAddress->id = $request->id;

        if($request->file('image_banner_path') != null &&  $request->file('image_banner_path')->getClientOriginalExtension() != null) {
           $image_banner_path = $companyAddress->title . '.' . $request->file('image_banner_path')->getClientOriginalExtension();
           $companyAddress->image_banner_path = $image_banner_path;
           $request->file('image_banner_path')->move(base_path() . '/public/images/companies/', $image_banner_path);
        }



        $companyAddress->company_id = $request->company_id;

        $companyAddress->save();

        $request->session()->flash('status', t('Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect('admin/companies/addresses/'.$request->company_id);
    } 

    public function delete(Request $request, $id = null)  {

         
         $companyAddress = CompanyAddress::where('id', '=', $request->id)->first();

         $company_id = $companyAddress->company_id;

         if(file_exists(base_path() . '/public/images/companies/'.$companyAddress->image_banner_path)) {
           unlink(base_path() . '/public/images/companies/'.$companyAddress->image_banner_path);
         }
         
         $companyAddress->delete();

         $request->session()->flash('status', t('Successfully Deleted') );

         return redirect('admin/companies/addresses/'.$company_id);
    }
	

}