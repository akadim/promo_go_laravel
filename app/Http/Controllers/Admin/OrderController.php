<?php
namespace App\Http\Controllers\Admin;

use App\Order;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class OrderController extends Controller {

    public $breadcrumbs = [];
    

	public function index(){

		 $breadcrumbs = [
		      [
		          'title' => t('Dashboard'),
			      'url' => ''.route('admin.dashboard'),
			      'active' => true
		      ],
		      [
		          'title' => 'Order',
			      'url' => ''.route('admin.orders'),
			      'active' => false
		      ]
		 ];

		 $orders = Order::with('client', 'order_lines')->get();

		 return view('admin.orders.index', compact(['orders', 'breadcrumbs']));
	}

    public function order_lines_by_order($order_id = null) {

              $order = Order::where('id', '=', $order_id)->with(['client', 'order_lines' => function($q) { return $q->with('deal', 'deal_option'); }])->first();

              $breadcrumbs = [
                  [
                      'title' => t('Dashboard'),
                      'url' => ''.route('admin.dashboard'),
                      'active' => true
                  ],
                  [
                      'title' => 'Order',
                      'url' => ''.route('admin.orders'),
                      'active' => true
                  ],
                  [
                      'title' =>  $order->client->fullname.' orders',
                      'url' => ''.route('admin.order.order_lines', ['order_id' => $order->id]),
                      'active' => false
                  ]
             ];

             //die(print_r($order));

             return view('admin.orders.order_lines.index', compact(['order', 'breadcrumbs']));
    }

    public function delete(Request $request, $id = null)  {
         
         $order = Order::where('id', '=', $id)->with('order_lines')->first();

         //die(print_r($order));

         foreach($order->order_lines as $order_line) {
             $order->order_lines()->delete($order_line);
         }

         $order->delete();

         $request->session()->flash('status', t('Order Successfully Deleted') );

         return redirect('admin/deals/orders');
    }
	

}