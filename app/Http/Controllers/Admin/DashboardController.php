<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

    public $breadcrumbs = [];


	public function index(){

		 $breadcrumbs[] = [
		      'title' => 'Dashboard',
		      'url' => ''.route('admin.dashboard'),
		      'active' => true
		 ];

		 return view('admin.dashboard', compact('breadcrumbs'));
	}

	

}