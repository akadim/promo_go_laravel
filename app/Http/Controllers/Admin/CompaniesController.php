<?php
namespace App\Http\Controllers\Admin;

use App\Company;
use App\Sector;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CompaniesController extends Controller {

    public $breadcrumbs = [];
    

	public function index(){

		 $breadcrumbs = [
		      [
		          'title' => 'Dashboard',
			      'url' => ''.route('admin.dashboard'),
			      'active' => false
		      ],
		      [
		          'title' => 'Companies',
			      'url' => ''.route('admin.companies'),
			      'active' => true
		      ]
		 ];



		 $companies = Company::where('deleted', '=', 0)->with('sector')->get();

         foreach($companies as $key => $company) {
            $parent_company_count = Company::where('deleted', '=', 0)->where('id', '=', $company->parent_id)->count();

            if($parent_company_count > 0) {
                $parent_company = Company::where('deleted', '=', 0)->where('id', '=', $company->parent_id)->first();
                $companies[$key]->parent_company = $parent_company;
            }
            else {
                $companies[$key]->parent_company = new Company;
            }
         }

         //die(print_r($companies));

         $companies_list = ['' => ' ---- Choose a Company ---- '] + Company::where('deleted', '=', 0)->lists('title', 'id')->all();
         $sectors_list = ['' => ' ---- Choose a Sector ---- '] + Sector::where('deleted', '=', 0)->lists('title', 'id')->all();

		 return view('admin.companies.index', compact(['companies', 'breadcrumbs', 'companies_list', 'sectors_list']));
	}

	public function edit($id = null) {
        
		if($id == -1) {
			$company = new Company;
			return json_encode($company);
		}

        return Company::where('id', '=', $id)->first();
	}
    
    public function store(Request $request) {
         

        $company = new Company();

        if($request->id != '' || $request->id != null) {
             $company = Company::where('id', '=', $request->id)->first();

             $request->session()->flash('updated_company', $company->id);
        }

        //die($request->title);

        $company->title = $request->title;
        $company->website = $request->website;
        $company->sector_id = $request->sector_id;
        $company->id = $request->id;

        if(!is_null($request->parent_id)) {
           $company->parent_id = $request->parent_id;
        }

        $company->deleted = 0;

        if(!is_null($request->file('imagePath'))) {

            $iconName  = $company->title . '_icon.' . $request->file('imagePath')->getClientOriginalExtension();

            $company->imagePath = $iconName;

            $request->file('imagePath')->move(base_path() . '/public/images/companies/', $iconName);
        }

        $company->save();

        $request->session()->flash('status', t('Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        $request->session()->flash('api_company', $company->id);
        
        return redirect($request->route()->getPrefix().'/companies');
    }

    public function get_company(Request $request) {
         $id = $request->id;

         $company = Company::where('id', '=', $id)->first();

         return response()->json(['company' => $company], 200);
    }

    public function delete(Request $request, $id = null)  {
         
         $company = Company::where('id', '=', $id)->first();
                 
         /*
         if(file_exists(base_path() . '/public/images/companies/'.$company->image_path)) {
           unlink(base_path() . '/public/images/companies/'.$company->image_path);
         }
         */
         
         $company->deleted = 1;

         $company->save();

         $request->session()->flash('status', t('Successfully Deleted') );
         $request->session()->flash('cancel_status', $id );
         $request->session()->flash('api_company', $company->id);
         $request->session()->flash('updated_company', $company->id);

         return redirect($request->route()->getPrefix().'/companies');
    }

    public function undo_delete(Request $request, $id=null) {
         $company = Company::where('id', '=', $id)->first();
         
         $company->deleted = 0;

         $company->save();

         $request->session()->flash('status', t('Successfully Restored') );
         $request->session()->flash('api_company', $company->id);
         $request->session()->flash('updated_company', $company->id);

         return redirect($request->route()->getPrefix().'/companies');
    }
	

}