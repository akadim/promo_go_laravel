<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\DealPosition;
use App\Badge;
use App\CatcherStatus;
use App\CatcherGadget;
use Mail;
use Hash;
use Crypt;
use DB;
use Carbon\Carbon;
use App\Profile;
use App\Gadget;

use Illuminate\Http\Request;

class UsersController extends Controller {
     
     public function loginmobile(Request $request){


         $credentials = array('email' => $request->get("email"), 'password' => $request->get("password"), 'active' => 1, 'deleted' => 0 );

          if(Auth::attempt($credentials)){

            $user= User::where('id', Auth::user()->id)->first();

            $catched_deals = DealPosition::where('user_id', '=', $user->id)->with('deal')->get();

            $catched_deals_count = DealPosition::where('user_id', '=', $user->id)->with('deal')->count();

            $catcher_badges = CatcherStatus::where('user_id', '=', $user->id)->with('badge')->get();

            $catcher_gadgets = CatcherGadget::where('user_id', '=', $user->id)->with('gadget')->get();

            $user_infos = [
                    'id' => $user->id,
                    'fullname' => $user->fullname,
                    'email' => $user->email,
                    'password' => $user->password,
                    'phone' => $user->phone,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                    'rank' => $user->rank,
                    'level' => $user->level,
                    'experience' => $user->experience,
                    'image_path' => (($user->image_path != null) ? asset('images/catchers/'.$user->image_path) : '') ,
                    'coin_value' => $user->coin_value,
            ];
            
            $catched_deals_infos = [];
            foreach($catched_deals as $catched_deal) {
                $catched_deals_infos[] = $catched_deal->id;
            }


            $user_infos['catched_deals'] = $catched_deals_count;
            

            $catcher_gadgets_infos = [];

            foreach($catcher_gadgets as $catcher_gadget) {
                $catcher_gadgets_infos[] = [
                       'gadget_id' => $catcher_gadget->gadget->id,
                       'quantity'  => $catcher_gadget->qte_bought
                ];
            }
            
            $user_infos['gadgets'] = $catcher_gadgets_infos;

            $user_badges_infos = [];
            foreach($catcher_badges as $catcher_badge) {
                $user_badges_infos[] = $catcher_badge->badge->id;
            }

            $user_infos['badges'] = $user_badges_infos;

            return response()->json([
                      'status' => 200,
                      'user' => $user_infos,
                      'token' => csrf_token()
              ], 200);
          }
          else{

             return \Response::json(array('status'=>500,'error_code'=>201,'description'=>'User or password not found'));
          }

      }


      public function use_gadget($user_id=null, $gadget_id=null) {
          
            $catcher_gadget = CatcherGadget::where('user_id', '=', $user_id)->where('gadget_id', '=', $gadget_id)->first();

            if(intval($catcher_gadget->qte_bought) > 0) {
                 $catcher_gadget->qte_bought = intval($catcher_gadget->qte_bought) - 1;
            }

            if(intval($catcher_gadget->qte_bought) <= 0) {
                 $catcher_gadget->deleted = 1;
            }

            $catcher_gadget->save();

            $catcher = User::where('id', '=', $user_id)->with('catcher_gadgets.gadget')->first();

            $all_gadgets = Gadget::where('deleted', '=', 0)->get();

            $gadgets = [];

            foreach($all_gadgets as $one_gadget) {

               $gadget = [
                       'id' => $one_gadget->id,
                       'title' => $one_gadget->title,
                       'image_path' => $one_gadget->image_path,
                       'has' => ($catcher_gadget->deleted == 1) ? true : false,
                       'price' => $one_gadget->price,
                       'quantity' => 0,
                       'type' => $one_gadget->type,
                       'value' => $one_gadget->value, 
                       'value_unit' => $one_gadget->value_unit,
                       'has' => false
                ];

               foreach($catcher->catcher_gadgets as $catcher_gadget) {
                  if($catcher_gadget->gadget->id == $one_gadget->id && $catcher_gadget->deleted == 0) {
                     $gadget['has'] = true;
                     $gadget['quantity'] = $catcher_gadget->qte_bought;
                  }
               }

               $gadgets[] = $gadget;
            }

            return response()->json(['gadgets' => $gadgets ], 200);
      }





     public function signupmobile(Request $request){ 
       $rules = [
                  'fullname' => 'required',
                  'email' => 'required|email|unique:users',
                  'password' => 'required'
              ];

              $input = \Request::only(
                  'fullname',
                  'email',
                  'password'

              );
        \Log::info('Showing user profile for user: '.json_encode(\Request::all()));
              $validator = \Validator::make($input, $rules);
      if($validator->fails())
              {
             return \Response::json(array('status'=>500,'error_code'=>202,'description'=>'User already exist'));
              }

               $newUser =new User();
         
            $newUser->fullname = $request->get("fullname");

            $newUser->email = $request->get("email");

            $newUser->deleted = 0;

            $profile = Profile::where('title', '=', 'Catcher')->first();

            $newUser->profile_id = $profile->id;

            $newUser->active = 1;

            $newUser->rank = 'Noob';

            $newUser->level = 1;

            $newUser->experience = 0;

            $newUser->coin_value = 500;
   
            $newUser->password = bcrypt($request->get("password"));

            $newUser->save();

            if (!$newUser) {
               return \Response::json(array('status'=>500,'error_code'=>201,'description'=>'User is not created'));
            }

            $credentials = array('email' => $request->get("email"), 'password' => $request->get("password") );

            if(Auth::attempt($credentials)){
                 $user= \App\User::where('id',\Auth::user()->id)->first();
                 return \Response::json(array('status'=>200,'token'=>csrf_token(),'user_id'=>\Auth::user()->id,'name'=>\Auth::user()->name,'users'=>$user));
            }
      }

      public function catch_deal($deal_position_id=null, $user_id=null) {
        
            $deal_position = DealPosition::where('id', '=', $deal_position_id)->with('deal')->first();

            $catcher = User::where('id', '=', $user_id)->where('active', '=', 1)->first();

            $next_level = false;

            $next_rank = false;

            $catcher_results = [];

            $ranks = [
               5 => 'Beginner',
               30 => 'Enthusiastic',
               60 => 'Crazy',
               100 => 'Onimoniac'
            ];

            $deal_position->status = "catched";

            $deal_position->user_id = $user_id;

            $deal_position->catched_time = Carbon::now()->timestamp;

            $deal_position->save();


            $catcher->experience = intval($catcher->experience) + 2;

            $catcher_results['experience'] = $catcher->experience;

            if(intval($catcher->experience) >= 100) {
                $catcher->level = intval($catcher->level) + 1;
                $catcher_results['level'] = $catcher->experience;
                $next_level = true;
                foreach($ranks as $key=> $rank) {
                    if($catcher->level == intval($key)) {
                       $catcher->rank = $rank;
                       $next_rank = true;
                       $catcher_results['rank'] = $catcher->rank;
                       break;
                    }
                }
            }
            
            $catcher_results['next_level'] = $next_level;
            $catcher_results['next_rank'] = $next_rank;

            $catcher->save();

            return response()->json([
              'deal_position' => $deal_position,
              'catcher_results' => $catcher_results,
              ], 200);
       }

       public function leaderboard() {
            $catcher_profile = Profile::where('title', '=', 'Catcher')->first();
            $catchers = User::where('profile_id', '=', $catcher_profile->id)->with('deal_positions')->get();

            $catchers_stats = [];
            $catched_values = [];
            $sorted_catchers_stats = [];

            foreach($catchers as $catcher) {

                $deal_catched_count = 0;
                foreach($catcher->deal_positions as $deal_position) {
                     if($deal_position->status == "catched") {
                         $deal_catched_count += 1;
                     }
                }
                
                $catcher_stat['fullname'] =  $catcher->fullname;
                $catcher_stat['profile_image'] = $catcher->image_path;
                $catcher_stat['catched_deals'] = $deal_catched_count;
                $catched_values [] = $deal_catched_count;

                $catcher_stats[] = $catcher_stat;
            }

            return response()->json([
                  'leaderboard' => $catcher_stats
            ]);

       }

       public function expire_deal($deal_position_id=null) {
            
            $deal_position = DealPosition::where('id', '=', $deal_position_id)->first();

            $deal_position->status = "expired";

            $deal_position->save();

            return response()->json(['deal_position' => $deal_position], 200);
       }

       public function buy_coin($id=null, $coin_value=null) {

            $current_user = User::find($id);

            $current_user->coin_value = intval($current_user->coin_value) + intval($coin_value);

            $current_user->save();

            return response()->json(['message' => 'Coins added Successfully'], 200);
       }

       public function buy_gadget($user_id=null, $gadget_id=null) {

           $catcher_gadget_count = CatcherGadget::where('user_id', '=', $user_id)->where('gadget_id', '=', $gadget_id)->count();

           $catcher_gadget = [];

           $current_gadget = Gadget::where('id', '=', $gadget_id)->first();

           if($catcher_gadget_count <= 0) {
               $catcher_gadget = new CatcherGadget;
               $catcher_gadget->user_id = $user_id;
               $catcher_gadget->gadget_id = $gadget_id;
               $catcher_gadget->qte_bought = 0;
               $catcher_gadget->save();
           }
           else {
               $catcher_gadget = CatcherGadget::where('user_id', '=', $user_id)->where('gadget_id', '=', $gadget_id)->first();
           }


            $catcher_gadget->qte_bought = intval($catcher_gadget->qte_bought) + 1;
            $catcher_gadget->deleted = 0;

            $catcher_gadget->save();

            $current_user = User::where('id', '=', $user_id)->first();

            $current_user->coin_value = intval($current_user->coin_value) - intval($current_gadget->price);

            $current_user->save();

            $catcher = User::where('id', '=', $user_id)->with('catcher_gadgets.gadget')->first();

            $all_gadgets = Gadget::where('deleted', '=', 0)->get();

            $gadgets = [];

            foreach($all_gadgets as $one_gadget) {

               $gadget = [
                       'id' => $one_gadget->id,
                       'title' => $one_gadget->title,
                       'image_path' => $one_gadget->image_path,
                       'has' => ($catcher_gadget->deleted == 1) ? true : false,
                       'price' => $one_gadget->price,
                       'quantity' => 0,
                       'type' => $one_gadget->type,
                       'value' => $one_gadget->value, 
                       'value_unit' => $one_gadget->value_unit,
                       'has' => false
                ];

               foreach($catcher->catcher_gadgets as $catcher_gadget) {
                  if($catcher_gadget->gadget->id == $one_gadget->id && $catcher_gadget->deleted == 0) {
                    $gadget['has'] = true;
                    $gadget['quantity'] = $catcher_gadget->qte_bought;
                  }
               }

               $gadgets[] = $gadget;
            }

            return response()->json(['code' => 200, 'gadgets' => $gadgets]);
       }

       public function upload_profile_image($user_id=null) {
             
             $catcher = User::where('id', '=', $user_id)->first();

             $image_file = $_FILES['file'];

             $image_name = time().'_'.$_FILES['file']['name'];

             $catcher->image_path = $image_name;

             $catcher->save();

             move_uploaded_file($_FILES['file']['tmp_name'], base_path().'/public/images/catchers/'.$image_name);

             $options = app('request')->header('accept-charset') == 'utf-8' ? JSON_UNESCAPED_UNICODE : null;

             return response()->json(['status' => 200, 'image_path' => $image_name], 200);
       }

       public function use_deal($deal_position_id=null) {

           $dealPosition = DealPosition::find($deal_position);
           
           $dealPosition->status = "used";

           $dealPosition->save();

           return  response()->json([ 'code' => 200, 'deal_position' => $dealPosition ], 200);
       }
}