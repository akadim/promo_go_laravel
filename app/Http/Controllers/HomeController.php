<?php
namespace App\Http\Controllers;

use App\Category;
use App\Country;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class HomeController extends Controller {

    public $breadcrumbs = [];
    

	public function index(Request $request){
        
         $current_country = Country::where('id', '=', $request->session()->get('current_country'))->first();
		 return redirect('deals/'.$current_country->title_en);
	}

	public function edit($id = null) {

		if($id == -1) {
			$category = new Category;
			return json_encode($category);
		}

		return Category::where('id', '=', $id)->first();
	}

    public function change_country(Request $request, $country_id = null) {

           $_SESSION['current_country'] = $country_id;

           $current_country = Country::where('id', '=', $country_id)->first();
           return redirect('deals/'.$current_country->title_en);
    }
    
    public function store(Request $request) {

         $this->validate($request, [
	        'title' => 'required|max:255'
	     ]);

        $category = new Category();

        if($request->id != '' || $request->id != null) {
             $category = Category::where('id', '=', $request->id)->first();
        }

        //die($request->title);

        $category->title = $request->title;
        $category->description = $request->description;
        $category->id = $request->id;

        $category->save();

        $request->session()->flash('status', t('Category Successfully '.(($request->id == null || $request->id == '') ? 'Added' : 'Updated')) );
        
        return redirect('admin/categories');
    }

    public function delete(Request $request, $id = null)  {
         
         $category = Category::where('id', '=', $id)->first();
         
         $category->delete();

         $request->session()->flash('status', t('Category Successfully Deleted') );

         return redirect('admin/categories');
    }
	

}