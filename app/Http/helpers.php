<?php
use App\Jobshunt\Models\Category;
use App\Jobshunt\Models\PostType;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use App\Permission;
use App\User;
use App\Store;
use App\Company;
use App\CompanyAssignment;
use App\StoreAssignment;

/**
 * @param $string
 * @return string
 */
function t($string)
{
    return trans('messages.' . $string, [], null, session('my.locale'));
}

/**
 * @return int
 */
function perPage()
{
    return 20;
}

/**
 * @param $request
 * @return mixed
 */
function siteSettings($request)
{

    return Cache::rememberForever($request, function () use ($request) {
        $request = DB::table('sitesettings')->whereName($request)->first();

        return $request->value;
    });
}

/**
 * @param        $email
 * @param int $s
 * @param string $d
 * @param string $r
 * @return string
 */
function get_gravatar($email, $s = 80, $d = 'mm', $r = 'g')
{
    $url = '//www.gravatar.com/avatar/';
    $url .= md5(strtolower(trim($email)));
    $url .= "?s=$s&d=$d&r=$r";

    return $url;
}

function is_user_allowed($user_id, $permission) {

     $current_user = User::where('id', '=', $user_id)->first();

     $profile = Profile::where('id', '=', $current_user->profile_id)->with('permissions')->first();
     
     if(!$current_user->is_super_admin) {
         foreach($profile->permissions as $permit) {
             if($permit->title === $permission) {
                return true;
             }
         }
     }
     else {
        return true;
     }

     return false;
}

function current_company($user_id) {
    
    $company_assignments_count = CompanyAssignment::where('user_id', '=', $user_id)->count();
    $store_assignments_count = StoreAssignment::where('user_id', '=', $user_id)->count();

    $company_id = 0;

    if($company_assignments_count > 0) {
          $company_assignment = CompanyAssignment::where('user_id', '=', $user_id)->first();
          $company_id = $company_assignment->company_id;
    }
    else if($store_assignments_count > 0) {
          $store_assignment = StoreAssignment::where('user_id', '=', $user_id)->first();
          $store = Store::where('id', '=', $store_assignment->store_id)->first();
          $company_id = $store->company_id;
    }

    return Company::find($company_id);

}
