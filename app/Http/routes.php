<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api'], function(){
        Route::any('/login', ['as' => 'client.login', 'uses' => 'UsersController@loginmobile']);
        Route::any('/logout', ['as' => 'client.login', 'uses' => 'UsersController@logoutmobile']);
        Route::any('/deal/catch/{deal_position_id}/{user_id}', ['as' => 'client.deal.catch', 'uses' => 'UsersController@catch_deal']);
        Route::any('/deal/expire/{deal_position_id}', ['as' => 'client.deal.expire', 'uses' => 'UsersController@expire_deal']);
        Route::any('/catcher/{id}/{coin_value}/buy-coin', ['as' => 'client.catcher.coin', 'uses' => 'UsersController@buy_coin']);
        Route::any('/catcher/{id}/{gadget_id}/buy-gadget', ['as' => 'client.catcher.gadget', 'uses' => 'UsersController@buy_gadget']);
        Route::any('/catcher/{user_id}/{gadget_id}/use-gadget', ['as' => 'client.catcher.use.gadget', 'uses' => 'UsersController@use_gadget']);
        Route::any('/leaderboard', ['as' => 'leaderboard', 'uses' => 'UsersController@leaderboard']);
        Route::any('/catcher/{user_id}/upload-image', ['as' => 'catcher.upload.image', 'uses' => 'UsersController@upload_profile_image']);
        Route::any('/deal/{deal_position_id}/use-deal', ['as' => 'use.deal.position', 'uses' => 'UsersController@use_deal']);
});


  Route::any('api/signup', ['as' => 'client.login', 'uses' => 'UsersController@signupmobile']);

Route::get('/',  ['as' => 'landing', 'uses' => 'LandingController@index']);
Route::post('/signup',  ['as' => 'join.us', 'uses' => 'LandingController@joinus']);


Route::group(['prefix' => '/adminocatch', 'namespace' => 'Admin'], function(){

	Route::get('/login', ['as' => 'admin.login', 'uses' => 'UserController@login']);

	Route::post('/login', ['as' => 'admin.login', 'uses' => 'UserController@login']);

    Route::get('/reset-password', ['as' => 'admin.reset.password', 'uses' => 'UserController@reset_password']);

    Route::post('/reset-password', ['as' => 'admin.reset.password', 'uses' => 'UserController@reset_password']);

	Route::get('/logout', ['as' => 'admin.logout', 'uses' => 'UserController@logout']);

});

Route::group(['prefix' => '/adminocatch', 'namespace' => 'Admin', 'middleware' => ['auth'/*, 'checkPermission'*/] ], function(){

    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);

    Route::get('/profile', ['as' => 'admin.profile', 'uses' => 'UserController@profile']);

    Route::post('/profile', ['as' => 'admin.profile', 'uses' => 'UserController@profile']);

    Route::post('/password', ['as' => 'admin.password', 'uses' => 'UserController@password']);

    Route::get('/settings', ['as' => 'admin.settings', 'uses' => 'SettingsController@index']);

    Route::post('/settings', ['as' => 'admin.settings', 'uses' => 'SettingsController@index']);

    Route::get('/companies', ['as' => 'admin.companies', 'uses' => 'CompaniesController@index']);
    Route::get('/companies/{id}/edit', ['as' => 'admin.companies.edit', 'uses' => 'CompaniesController@edit']);
    Route::post('/company', ['as' => 'admin.company', 'uses' => 'CompaniesController@get_company']);
    Route::post('/companies/store', ['as' => 'admin.companies.store', 'uses' => 'CompaniesController@store']);
    Route::get('/companies/{id}/delete', ['as' => 'admin.companies.delete', 'uses' => 'CompaniesController@delete']);
    Route::get('/companies/{id}/delete/undo', ['as' => 'admin.companies.delete.undo', 'uses' => 'CompaniesController@undo_delete']);

    Route::get('/sectors', ['as' => 'admin.sectors', 'uses' => 'SectorsController@index']);
    Route::get('/sectors/{id}', ['as' => 'admin.sector', 'uses' => 'SectorsController@show']);
    Route::get('/sectors/{id}/edit', ['as' => 'admin.sectors.edit', 'uses' => 'SectorsController@edit']);
    Route::post('/sectors/store', ['as' => 'admin.sectors.store', 'uses' => 'SectorsController@store']);
    Route::get('/sectors/{id}/delete', ['as' => 'admin.sectors.delete', 'uses' => 'SectorsController@delete']);
    Route::get('/sectors/{id}/delete/undo', ['as' => 'admin.sectors.delete.undo', 'uses' => 'SectorsController@undo_delete']);


    Route::get('/companies-managers', ['as' => 'admin.companies.managers', 'uses' => 'CompaniesManagersController@index']);
    Route::get('/companies-managers/{id}/edit', ['as' => 'admin.companies.managers.edit', 'uses' => 'CompaniesManagersController@edit']);
    Route::post('/companies-managers/store', ['as' => 'admin.companies.managers.store', 'uses' => 'CompaniesManagersController@store']);
    Route::get('/companies-managers/{id}/delete', ['as' => 'admin.companies.managers.delete', 'uses' => 'CompaniesManagersController@delete']);
    Route::get('/companies-managers/{id}/delete/undo', ['as' => 'admin.companies.managers.delete.undo', 'uses' => 'CompaniesManagersController@undo_delete']);
    Route::post('/companies-managers/change-password', ['as' => 'admin.companies.managers.change.password', 'uses' => 'CompaniesManagersController@change_password']);
    Route::post('/companies-managers/verify-email', ['as' => 'admin.companies.managers.verify.email', 'uses' => 'CompaniesManagersController@verify_email']);
    Route::post('/companies-managers/activate', ['as' => 'admin.companies.managers.activate', 'uses' => 'CompaniesManagersController@activate_account']);

	Route::get('/stores', ['as' => 'admin.stores', 'uses' => 'StoresController@index']);
	Route::get('/stores/{id}/edit', ['as' => 'admin.stores.edit', 'uses' => 'StoresController@edit']);
	Route::post('/stores/store', ['as' => 'admin.stores.store', 'uses' => 'StoresController@store']);
	Route::get('/stores/{id}/delete', ['as' => 'admin.stores.delete', 'uses' => 'StoresController@delete']);
    Route::get('/stores/{id}/delete/undo', ['as' => 'admin.stores.delete.undo', 'uses' => 'StoresController@undo_delete']);

    Route::get('/stores-managers', ['as' => 'admin.stores.managers', 'uses' => 'StoresManagersController@index']);
    Route::get('/stores-managers/{id}/edit', ['as' => 'admin.stores.managers.edit', 'uses' => 'StoresManagersController@edit']);
    Route::post('/stores-managers/store', ['as' => 'admin.stores.managers.store', 'uses' => 'StoresManagersController@store']);
    Route::get('/stores-managers/{id}/delete', ['as' => 'admin.stores.managers.delete', 'uses' => 'StoresManagersController@delete']);
    Route::get('/stores-managers/{id}/delete/undo', ['as' => 'admin.stores.managers.delete.undo', 'uses' => 'StoresManagersController@undo_delete']);
    Route::post('/stores-managers/change-password', ['as' => 'admin.stores.managers.change.password', 'uses' => 'StoresManagersController@change_password']);
    Route::post('/stores-managers/verify-email', ['as' => 'admin.stores.managers.verify.email', 'uses' => 'StoresManagersController@verify_email']);
    Route::get('/stores-managers/store_section/{company_id}/{store_id}', ['as' => 'admin.stores.managers.store.section', 'uses' => 'StoresManagersController@store_section']);

	Route::get('/deals', ['as' => 'admin.deals', 'uses' => 'DealsController@index']);
    Route::post('/sector/deals', ['as' => 'admin.sector.deals', 'uses' => 'DealsController@sector_deals']);
    Route::post('/company/deals', ['as' => 'admin.company.deals', 'uses' => 'DealsController@company_deals']);
    Route::post('/deals/more', ['as' => 'admin.deals.more', 'uses' => 'DealsController@show_more']);
	Route::get('/deals/{id}/edit', ['as' => 'admin.deals.edit', 'uses' => 'DealsController@edit']);
    Route::post('/deal', ['as' => 'admin.deals.show', 'uses' => 'DealsController@get_deal']);
	Route::post('/deals/store', ['as' => 'admin.deals.store', 'uses' => 'DealsController@store']);
	Route::get('/deals/{id}/delete', ['as' => 'admin.deals.delete', 'uses' => 'DealsController@delete']);
    Route::get('/deals/{id}/marking', ['as' => 'admin.deals.marking', 'uses' => 'DealsController@marking']);
    Route::post('/deals/{id}/marking', ['as' => 'admin.deals.marking', 'uses' => 'DealsController@marking']);
    Route::get('/deals/{id}/{store_id}/markers', ['as' => 'admin.deals.markers', 'uses' => 'DealsController@markers']);
    Route::get('/deals/{id}/delete/undo', ['as' => 'admin.deals.delete.undo', 'uses' => 'DealsController@undo_delete']);
    Route::get('/deals/expire_deal/{deal_id}', ['as' => 'admin.expire.deal', 'uses' => 'DealsController@expire_deal']);
    Route::get('/deals/results/{deal_id}', ['as' => 'admin.deal.results', 'uses' => 'DealsController@show_results']);
    Route::get('/deals/show_info/{deal_id}/{position_id}/{validated}', ['as' => 'admin.deal.show.info', 'uses' => 'DealsController@show_info']);
    Route::get('/deals/time_to_goal/{position_id}/{time_to_goal}', ['as' => 'admin.deal.time_to_goal', 'uses' => 'DealsController@change_time_to_goal']);
    
    Route::get('/profiles', ['as' => 'admin.profiles', 'uses' => 'ProfilesController@index']);
    Route::get('/profiles/{id}/edit', ['as' => 'admin.profiles.edit', 'uses' => 'ProfilesController@edit']);
    Route::post('/profiles/store', ['as' => 'admin.profiles.store', 'uses' => 'ProfilesController@store']);
    Route::get('/profiles/{id}/delete', ['as' => 'admin.profiles.delete', 'uses' => 'ProfilesController@delete']);
    Route::get('/profiles/{id}/undo/delete', ['as' => 'admin.profiles.delete.undo', 'uses' => 'ProfilesController@undo_delete']);

    Route::get('/users', ['as' => 'admin.users', 'uses' => 'UsersController@index']);
	Route::get('/users/{id}/edit', ['as' => 'admin.users.edit', 'uses' => 'UsersController@edit']);
	Route::post('/users/store', ['as' => 'admin.users.store', 'uses' => 'UsersController@store']);
	Route::get('/users/{id}/delete', ['as' => 'admin.users.delete', 'uses' => 'UsersController@delete']);

    Route::get('/gadgets', ['as' => 'admin.gadgets', 'uses' => 'GadgetsController@index']);
    Route::get('/gadgets/{id}', ['as' => 'admin.gadget', 'uses' => 'GadgetsController@show']);
	Route::get('/gadgets/{id}/edit', ['as' => 'admin.gadgets.edit', 'uses' => 'GadgetsController@edit']);
    Route::get('/gadgets/{id}/view', ['as' => 'admin.gadgets.view', 'uses' => 'GadgetsController@view']);
	Route::post('/gadgets/store', ['as' => 'admin.gadgets.store', 'uses' => 'GadgetsController@store']);
	Route::get('/gadgets/{id}/delete', ['as' => 'admin.gadgets.delete', 'uses' => 'GadgetsController@delete']);
    Route::get('/gadgets/{id}/undo/delete', ['as' => 'admin.gadgets.delete.undo', 'uses' => 'GadgetsController@undo_delete']);

    Route::get('/catchers', ['as' => 'admin.catchers', 'uses' => 'CatchersController@index']);
	Route::get('/catchers/{id}/delete', ['as' => 'admin.catchers.delete', 'uses' => 'CatchersController@delete']);
    Route::get('/catchers/toggle-active/{id}', ['as' => 'admin.catchers.toggle.active', 'uses' => 'CatchersController@toggle_active']);
    Route::post('/catchers/deals/expire', ['as' => 'admin.catchers.expire.deal', 'uses' => 'CatchersController@expire_catched_deal']);
    Route::get('/catchers/{id}', ['as' => 'admin.catcher.show', 'uses' => 'CatchersController@show_profile']);
    Route::get('/catchers/{id}/undo/delete', ['as' => 'admin.catchers.delete.undo', 'uses' => 'CatchersController@undo_delete']);

    Route::get('/badges', ['as' => 'admin.badges', 'uses' => 'BadgesController@index']);
    Route::get('/badges/{id}', ['as' => 'admin.badge', 'uses' => 'BadgesController@show']);
	Route::get('/badges/{id}/edit', ['as' => 'admin.badges.edit', 'uses' => 'BadgesController@edit']);
	Route::post('/badges/store', ['as' => 'admin.badges.store', 'uses' => 'BadgesController@store']);
	Route::get('/badges/{id}/delete', ['as' => 'admin.badges.delete', 'uses' => 'BadgesController@delete']);
    Route::get('/badges/{id}/undo/delete', ['as' => 'admin.badges.delete.undo', 'uses' => 'BadgesController@undo_delete']);

    Route::get('/coins', ['as' => 'admin.coins', 'uses' => 'CoinsController@index']);
    Route::get('/coins/{id}', ['as' => 'admin.coin', 'uses' => 'CoinsController@show']);
	Route::get('/coins/{id}/edit', ['as' => 'admin.coins.edit', 'uses' => 'CoinsController@edit']);
	Route::post('/coins/store', ['as' => 'admin.coins.store', 'uses' => 'CoinsController@store']);
	Route::get('/coins/{id}/delete', ['as' => 'admin.coins.delete', 'uses' => 'CoinsController@delete']);
    Route::get('/coins/{id}/undo/delete', ['as' => 'admin.coins.delete.undo', 'uses' => 'CoinsController@undo_delete']);

	Route::get('/companies/addresses/{company_id}', ['as' => 'admin.companies.addresses', 'uses' => 'CompanyAddressesController@index']);
	Route::get('/companies/addresses/{company_id}/{id}/edit', ['as' => 'admin.companies.addresses.edit', 'uses' => 'CompanyAddressesController@edit']);
	Route::post('/companies/addresses/{company_id}/store', ['as' => 'admin.companies.addresses.store', 'uses' => 'CompanyAddressesController@store']);
	Route::get('/companies/addresses/{company_id}/{id}/delete', ['as' => 'admin.companies.addresses.delete', 'uses' => 'CompanyAddressesController@delete']);


	Route::get('/deals/options/{deal_id}', ['as' => 'admin.deals.options', 'uses' => 'DealOptionsController@index']);
	Route::get('/deals/options/{deal_id}/{id}/edit', ['as' => 'admin.deals.options.edit', 'uses' => 'DealOptionsController@edit']);
	Route::post('/deals/options/{deal_id}/store', ['as' => 'admin.deals.options.store', 'uses' => 'DealOptionsController@store']);
	Route::get('/deals/options/{deal_id}/{id}/delete', ['as' => 'admin.deals.options.delete', 'uses' => 'DealOptionsController@delete']);

    Route::get('/deals/orders', ['as' => 'admin.orders', 'uses' => 'OrderController@index']);
    Route::get('/deals/orders/{order_id}', ['as' => 'admin.order.order_lines', 'uses' => 'OrderController@order_lines_by_order']);
    Route::get('/deals/order/{order_id}/delete', ['as' => 'admin.order.delete', 'uses' => 'OrderController@delete']);
});

