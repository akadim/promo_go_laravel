<?php

namespace App\Http\Middleware;

use Closure;
use App\Profile;
use App\User;
use App\Permission;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_user = Auth::user();
        $current_user = User::where('id','=', $current_user->id)->with('profile')->first();
        $current_profile = Profile::where('id', '=', $current_user->profile_id)->with('permissions')->first();
        $current_url = $_SERVER['REQUEST_URI'];
        $current_url = explode('/', $current_url);
       
        if(!$current_user->is_super_admin) {
            if(!empty($current_profile->permissions)) {
                foreach($current_profile->permissions as $current_permission) {

                     $current_permission_array = explode('/', $current_permission->path_link);

                     if( in_array($current_permission_array, $current_url) === false) {
                          return redirect()->route('admin.login');
                     }
                }
            }
            else {
                return redirect()->route('admin.login');
            }
        }

        return $next($request);
    }
}
