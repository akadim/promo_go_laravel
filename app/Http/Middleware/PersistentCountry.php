<?php

namespace App\Http\Middleware;

use Closure;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PersistentCountry
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        session_start();

        $current_country = 0;

        
        if(!isset($_SESSION['current_country'])) {
            $first_country = Country::first();
            $current_country = $first_country->id;
            $_SESSION['current_country'] =  $current_country;
        }

        $cart_country_ar = $request->session()->get('cart_'.$current_country.'_ar');
        $cart_country_en = $request->session()->get('cart_'.$current_country.'_en');

        if(!$cart_country_ar && !$cart_country_en) {
            $request->session()->set('cart_'.$current_country.'_ar', []);
            $request->session()->set('cart_'.$current_country.'_en', []);
        }

        return $next($request);
    }
}
