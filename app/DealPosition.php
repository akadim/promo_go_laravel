<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealPosition extends Model
{
    public function deal() {
    	return $this->belongsTo('App\Deal');
    }

    public function user() {
        return $this->hasMany('App\User');
    }
}
