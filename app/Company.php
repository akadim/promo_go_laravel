<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function stores() {
    	return $this->hasMany('App\Store');
    }

    public function deals() {
    	return $this->hasMany('App\Deal');
    }

    public function sector() {
    	return $this->belongsTo('App\Sector');
    }
}
