<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Country;
use App\Deal;
use App\OrderLine;
use App\CompanyAddress;
use App\User;
use App\Permission;
use App\Profile;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {


        view()->composer('layouts.default', function($view)
        {
            $countries = Country::all();

            $number_orderLines = OrderLine::count();

            $order_lines = OrderLine::all();
            
            $total_bee_deals = 0.0;

            foreach($order_lines as $order_line) {
                 $total_bee_deals += ((intval($order_line->ordered_quantity) * floatval($order_line->price)));
            }

            $banner_company_addresses = CompanyAddress::where('show_banner_header', '=', '1')->whereNotNull('image_banner_path')->get();

            $empty_deal = new Deal;
            $current_country = $_SESSION['current_country'];

            //die('current country = '.$_SESSION['current_country']);

            $current_country = Country::where('id', '=', $current_country)->with(['categories.subcategories', 'categories', 'categories.deals', 'categories.travel_locations'])->first();

            $view->with('current_country', $current_country);
            $view->with('number_orderLines', $number_orderLines);
            $view->with('total_bee_deals', $total_bee_deals);
            $view->with('empty_deal', $empty_deal);
            $view->with('banner_company_addresses', $banner_company_addresses);
            $view->with('countries', $countries);
            $view->with('current_user', Auth::user());
        });

        view()->composer('layouts.admin', function($view){
              
              $current_user = Auth::user();
              $current_user = User::where('id', '=', $current_user->id)->with('profile')->first();
              $current_permissions = [];

              $sidebar = [];

              $parent_ids = [];

              if(!$current_user->is_super_admin) {
                 $current_profile = Profile::where('id', '=', $current_user->profile_id)->with('permissions')->first();
                 //die(print_r($current_profile));
                 $current_permissions = $current_profile->permissions;
              }
              else {
                 $current_permissions = Permission::all();
              }

              $permission_ids = [];

              foreach($current_permissions as $currento) {
                  $permission_ids[] = $currento->id;
              }

              foreach($current_permissions as $current_permission) {
                
                if(!is_null($current_permission->parent_id) && !in_array($current_permission->parent_id, $parent_ids)) {
                      $parent_ids[] = $current_permission->parent_id;
                }
                
                if(is_null($current_permission->parent_id) && $current_permission->path_link != null) {
                      $parent_ids[] = $current_permission->id;
                }
             }

              if(!empty($parent_ids)) {
                  foreach($parent_ids as $parent_id) {
                      $permit = Permission::where('id', '=', $parent_id)->first();
                      $permit = ['title' => $permit->title, 'url' => $permit->path_link];
                      $child_permissions = Permission::where('parent_id', '=', $parent_id)->whereIn('id', $permission_ids)->where('in_sidebar', '=', 1)->get();
                      $child_permissions_count = Permission::where('parent_id', '=', $parent_id)->whereIn('id', $permission_ids)->where('in_sidebar', '=', 1)->count();
                      $permit['child_permissions'] = ( $child_permissions_count > 0 ) ? $child_permissions : [];
                      $sidebar[] = $permit;
                  }
              }

              $view->with('sidebar', $sidebar);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
